package Auth;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.turningcloud.controller.business.contract.EauthService;
import com.turningcloud.controller.utils.PacketDataUtils;
import com.turningcloud.view.beans.views.LoggedInCheck;
import com.turningcloud.view.beans.views.Usersession;
import com.turningcloud.view.pojo.GooglePojo;

@WebServlet("/Oauth2callback")
public class Oauth2Callback extends HttpServlet
{

	private static final long	serialVersionUID	= 1L;
	@Inject
	private Usersession			userSession;

	@Inject
	private LoggedInCheck		loginBean;

	@Inject
	private EauthService		eauthService;

	@Inject
	private PacketDataUtils		packetDataUtils;

	Logger						log					= LoggerFactory.getLogger(getClass());

	public Oauth2Callback()
	{
		super();

	}

	@SuppressWarnings("unused")
	private void createSession(HttpServletRequest request, HttpServletResponse response,
			String userMail, String userPicture)
	{
		HttpSession session = request.getSession(true);
		session.setAttribute("userID", userMail);
		session.setAttribute("userPicture", userPicture);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		log.debug("Entering doGet Method.{}", request.getParameterMap());

		try
		{
			// get code
			String code = request.getParameter("code");
			// format parameters to post
			String urlParameters = String.format(
					"code=%s&client_id=907773160369-hb1n7htno2em9luje084v299jke4a0br.apps.googleusercontent.com&client_secret=w7Eq8UgzYrmwtWqAOAARcdVR&redirect_uri=http://localhost:8080/Oauth2callback&grant_type=authorization_code",
					URLEncoder.encode(code, "UTF-8"));

			// post parameters
			log.debug("Sending Google Oauth Request");
			long startTime = System.currentTimeMillis();

			URL url = new URL("https://accounts.google.com/o/oauth2/token");
			URLConnection urlConn = url.openConnection();
			urlConn.setDoOutput(true);

			urlConn.setRequestProperty("Accept-Charset", "UTF-8");
			urlConn.setRequestProperty("USER-AGENT",
					"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
			urlConn.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded;charset=UTF-8");

			try (OutputStreamWriter writer = new OutputStreamWriter(urlConn.getOutputStream()))
			{
				writer.write(urlParameters);
				writer.flush();
			}

			log.debug("Response Time is:{}", System.currentTimeMillis() - startTime);

			// get output in outputString
			String line, outputString = "";

			try (BufferedReader reader = new BufferedReader(
					new InputStreamReader(urlConn.getInputStream())))
			{
				while ((line = reader.readLine()) != null)
				{
					outputString += line;
				}
			}

			log.debug(outputString);

			// get Access Token
			JsonObject json = (JsonObject) new JsonParser().parse(outputString);
			String access_token = json.get("access_token").getAsString();

			log.debug(access_token);

			// get User Info
			startTime = System.currentTimeMillis();
			log.debug("Sending Request for user Identity");
			url = new URL(
					"https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + access_token);
			urlConn = url.openConnection();

			urlConn.setRequestProperty("Accept-Charset", "UTF-8");
			urlConn.setRequestProperty("USER-AGENT",
					"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");

			outputString = "";

			try (BufferedReader reader = new BufferedReader(
					new InputStreamReader(urlConn.getInputStream())))
			{
				while ((line = reader.readLine()) != null)
				{
					outputString += line;
				}
			}

			log.debug("Response Time is {}", System.currentTimeMillis() - startTime);

			log.debug(outputString);

			GooglePojo data = new Gson().fromJson(outputString, GooglePojo.class);

			//identify user and manage session
			log.debug(data.toString());
			if (eauthService.isAuthenticated(data.getEmail()))
			{

				loginBean.setUpn(data.getEmail());

				packetDataUtils.updateImage(data.getPicture().getBytes(), "gAuthimg",
						data.getEmail().toString().toLowerCase());
				response.sendRedirect("index.jsp");
				createSession(request, response, data.getEmail(), data.getPicture());
				eauthService.userSetting(data.getEmail().toLowerCase(), "active");
			}

			else
				response.sendRedirect("login.jsp");
			return;

		}
		catch (ProtocolException | MalformedURLException e)
		{
			log.error(e.getMessage());
		}
		log.debug("Except the Get Functionality");
	}

}