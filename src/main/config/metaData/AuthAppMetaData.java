/**
 * 
 */
package metaData;

/**
 * @author manoj
 *
 */
public interface AuthAppMetaData
{

	public interface GoogleAuth
	{
		String	appUrl				= "https://accounts.google.com/o/oauth2/auth?client_id=907773160369-hb1n7htno2em9luje084v299jke4a0br.apps.googleusercontent.com&response_type=code&scope=email&redirect_uri=http://localhost:8080/Oauth2callback&access_type=online&approval_prompt=auto";
		String	OauthCallbackURL	= "/Oauth2callback";
	}
}
