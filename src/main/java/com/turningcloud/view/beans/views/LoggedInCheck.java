package com.turningcloud.view.beans.views;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.turningcloud.controller.utils.PacketDataUtils;

import metaData.AuthAppMetaData;

@Named("login")
@RequestScoped
public class LoggedInCheck implements Serializable
{

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 260787340918196655L;

	private String				upn;

	@Inject
	private PacketDataUtils		packetDataUtils;

	@Inject
	private Usersession			usersession;

	private String				googleAuth;

	public LoggedInCheck()
	{
	}

	@PostConstruct
	public void initialize()
	{
		googleAuth = AuthAppMetaData.GoogleAuth.appUrl;
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getkeyValue(String upn)
	{
		Map<String, String> entrySet = new HashMap<>();
		entrySet.put("key", upn);
		return entrySet;
	}

	public String getUpn()
	{
		return upn;
	}

	public void setUpn(String upn)
	{
		this.upn = upn;
		this.setusersession();
	}

	public void setusersession()
	{
		usersession.setEmployeedata(packetDataUtils.getEmpData("Employeedata.findByUpn",
				this.getkeyValue(this.getUpn())));
		usersession.setUserStatus("Online");
		usersession.setRoles(packetDataUtils.roles(this.getUpn()));
	}

	public String getGoogleAuth()
	{
		return googleAuth;
	}

	public void setGoogleAuth(String googleAuth)
	{
		this.googleAuth = googleAuth;
	}

}
