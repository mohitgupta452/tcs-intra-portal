/**
 * 
 */
package com.turningcloud.view.beans.views;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import org.mortbay.log.Log;

import com.google.gson.Gson;
import com.itextpdf.text.BaseColor;
import com.turningcloud.controller.utils.FilesUtils;
import com.turningcloud.controller.utils.PDFCreation;

/**
 * @author manoj
 *
 */
@Named("downloadPdf")
@RequestScoped
public class downloadpdf
{

	/**
	 * 
	 */

	private static final long	serialVersionUID	= 1L;

	PDFCreation					pdfCreation			= null;

	public downloadpdf()
	{
		// TODO Auto-generated constructor stub
	}

	public File generatePdf(Map<String, String> map)
	{
		File file = null;
		Log.debug("response map {}", map);
		//		if (map.get("service") == null)
		//			map.put("serviceList", "attachment");

		String path = FilesUtils.getPlatformBasedParentDir().getPath() + File.separator +"PDF";

		try
		{
			pdfCreation = new PDFCreation(path, map.get("service") + ".pdf");

			if ((map.get("service").equals("PaySlip")))
			{
//				pdfCreation.addHeader("PAY SLIP FOR THE MONTH "
//						+ map.get("salaryMonth").toUpperCase() + " " + map.get("salaryYear"));
//				pdfCreation.addSpace();
				pdfCreation.addImage();
				pdfCreation.addDataAtRight( "TurningCloud Solutions Pvt Ltd  \n C-73, First Floor, Sector 63, U.P \n Pincode-201301 ");;
				pdfCreation.addSpace();
				pdfCreation.addDataAtCenter("PAY SLIP FOR THE MONTH "
						+ map.get("salaryMonth").toUpperCase() + " " + map.get("salaryYear"));
				pdfCreation.addSpace();
				List<String> headList = new ArrayList<>();
				headList.add("");
				List<String> dataList = new ArrayList<>();
				List<String> cellData = new ArrayList<>();
				cellData.add("Employee Name");
				cellData.add(map.get("name"));
				cellData.add("Emp. Code");
				cellData.add(map.get("empCode"));
				cellData.add("Joining Date");
				cellData.add(map.get("joinDate"));
				cellData.add("Designation");
				cellData.add(map.get("designation"));
				cellData.add("Department");
				cellData.add(map.get("department"));
				pdfCreation.createFirstTable(cellData);
				dataList.add(map.get("basicSalary"));
				dataList.add(map.get("hra"));
				dataList.add(map.get("conveyance"));
				dataList.add(map.get("specialAllowance"));
				dataList.add(map.get("perPay"));
				dataList.add(map.get("deputation"));
				dataList.add(map.get("netPay"));
				dataList.add("0.00");// for Pf
				dataList.add("0.00");// professional Tax
				dataList.add("0.00");//Lunch Recovery
				dataList.add("0.00");//Transport Recovery
				dataList.add("0.00");//Income Tax
				pdfCreation.salaryAmount(dataList);
				BaseColor headingColor = new BaseColor(Color.GRAY);
				BaseColor textColor = new BaseColor(Color.BLACK);
				pdfCreation.createTable(1, "Pay Slip Information", headingColor, textColor);
				//				pdfCreation.createTableRow(generatedData, true);
//				pdfCreation.createTableRow();
				pdfCreation.addTable();
			}
			file = pdfCreation.closePDF();
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}

		return file;
	}

}
