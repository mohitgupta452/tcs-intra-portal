/**
 * 
 */
package com.turningcloud.view.beans.views;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.turningcloud.controller.business.contract.serviceRequestUtil;
import com.turningcloud.controller.service.AppMetaEnumeration;
import com.turningcloud.controller.utils.CrudService;
import com.turningcloud.controller.utils.PacketDataUtils;

/**
 * @author manoj
 *
 */
@Named("aplb")
@RequestScoped
public class AppliedLeave
{

	/**
	 * 
	 */

	@Inject
	private serviceRequestUtil			serviceRequests;

	@Inject
	private CrudService					crudService;

	private Map<String, String>			appliedLeave;

	private List<Map<String, Object>>	appliedleaveRequest;

	private List<Map<String, Object>>	managerResponse;

	private List<Map<String, Object>>	adminResponses;

	private List<Map<String, Object>>	hrResponses;

	private Map<String, Object>			leaveDetails;

	@Inject
	private PacketDataUtils				packetDataUtils;

	@Inject
	private Usersession					usersession;

	public AppliedLeave()
	{
	}

	@PostConstruct
	public void appliedLeaveRequest()
	{
		List<String> roles = usersession.getRoles();
		boolean role = packetDataUtils.isManager(
				usersession.getEmployeedata().getUserPrincipalName().toLowerCase().toString());
		if (!usersession.getEmployeedata().getManager().isEmpty() && !role
				&& usersession.getRoles().contains(AppMetaEnumeration.Roles.HRManager.toString()))
			hrResponses = serviceRequests.requestdata(
					AppMetaEnumeration.Services.LeaveApplication.toString(), "UserActionByHR",
					AppMetaEnumeration.Roles.HRManager.toString());
		else if (!usersession.getEmployeedata().getManager().isEmpty() && role)
		{
			if (usersession.getRoles()
					.contains(AppMetaEnumeration.Roles.ReportingManager.toString()))
			{
				managerResponse = serviceRequests.requestdata(
						AppMetaEnumeration.Services.LeaveApplication.toString(), "UserActionByRM",
						AppMetaEnumeration.Roles.ReportingManager.toString());
			}

			if (usersession.getRoles().contains(AppMetaEnumeration.Roles.HRManager.toString()))
			{
				hrResponses = serviceRequests.requestdata(
						AppMetaEnumeration.Services.LeaveApplication.toString(), "UserActionByHR",
						AppMetaEnumeration.Roles.HRManager.toString());
			}
			if (usersession.getRoles().contains(AppMetaEnumeration.Roles.Administrator.toString()))
			{
				adminResponses = serviceRequests.requestdata(
						AppMetaEnumeration.Services.LeaveApplication.toString(),
						"UserActionByRoles", AppMetaEnumeration.Roles.Administrator.toString());
			}

		}
		appliedleaveRequest = serviceRequests.requestdata(
				AppMetaEnumeration.Services.LeaveApplication.toString(),
				"UserAction.findAllByService", null);
		leaveDetails = serviceRequests.leaveDetails();
	}

	public Map<String, String> getAppliedLeave()
	{
		return appliedLeave;
	}

	public void setAppliedLeave(Map<String, String> appliedLeave)
	{
		this.appliedLeave = appliedLeave;
	}

	public List<Map<String, Object>> getAppliedleaveRequest()
	{
		return appliedleaveRequest;
	}

	public void setAppliedleaveRequest(List<Map<String, Object>> appliedleaveRequest)
	{
		this.appliedleaveRequest = appliedleaveRequest;
	}

	public List<Map<String, Object>> getHrResponses()
	{
		return hrResponses;
	}

	public void setHrResponses(List<Map<String, Object>> hrResponses)
	{
		this.hrResponses = hrResponses;
	}

	public List<Map<String, Object>> getManagerResponse()
	{
		return managerResponse;
	}

	public void setManagerResponse(List<Map<String, Object>> managerResponse)
	{
		this.managerResponse = managerResponse;
	}

	public List<Map<String, Object>> getAdminResponses()
	{
		return adminResponses;
	}

	public Map<String, Object> getLeaveDetails()
	{
		return leaveDetails;
	}

	public void setLeaveDetails(Map<String, Object> leaveDetails)
	{
		this.leaveDetails = leaveDetails;
	}

	public void setAdminResponses(List<Map<String, Object>> adminResponses)
	{
		this.adminResponses = adminResponses;
	}

}
