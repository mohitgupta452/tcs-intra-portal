/**
 * 
 */
package com.turningcloud.view.beans.views;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.turningcloud.controller.business.contract.ActionMasters;

/**
 * @author manoj
 *
 */
@Named("psGen")
@RequestScoped
public class PaySlipGeneration
{

	/**
	 * 
	 */
	@Inject
	private ActionMasters				actionMasters;

	private List<Map<String, Object>>	payGenList;

	@Inject
	private Usersession					usersession;

	public PaySlipGeneration()
	{
	}

	@PostConstruct
	public void payGen()
	{
		int empDataId = usersession.getEmployeedata().getEmpDataID();
		payGenList = actionMasters.generatedPaySlip(empDataId);
		System.out.println(payGenList);
	}

	public List<Map<String, Object>> getPayGenList()
	{
		return payGenList;
	}

	public void setPayGenList(List<Map<String, Object>> payGenList)
	{
		this.payGenList = payGenList;
	}

}
