/**
 * 
 */
package com.turningcloud.view.beans.views;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.turningcloud.controller.business.contract.ActionMasters;

/**
 * @author manoj
 *
 */
@Named("eData")
@RequestScoped
public class EmployeeData implements Serializable
{

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 3070353588671816919L;

	private Map<String, Object>	empData;

	private List<String>		empCode;

	@Inject
	private Usersession			userData;

	@Inject
	private ActionMasters		actionMasters;

	/**
	 * 
	 */

	public EmployeeData()
	{
	}

	@PostConstruct
	public void construct()
	{
		String upn = userData.getEmployeedata().getUserPrincipalName().toString();
		empData = actionMasters.empInformation(upn);
		empCode = actionMasters.getEmpCode();
		System.out.println(empData);
	}

	public Map<String, Object> getEmpData()
	{
		return empData;
	}

	public void setEmpData(Map<String, Object> empData)
	{
		this.empData = empData;
	}

	public List<String> getEmpCode()
	{
		return empCode;
	}

	public void setEmpCode(List<String> empCode)
	{
		this.empCode = empCode;
	}

}
