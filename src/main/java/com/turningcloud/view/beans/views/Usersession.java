/**
 * 
 */
package com.turningcloud.view.beans.views;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import com.turningcloud.model.entity.Employeedata;

/**
 * @author manoj
 *
 */
@Named("usersession")
@SessionScoped
public class Usersession implements Serializable
{

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -788342295713290244L;
	/**
	 * 
	 */
	private Employeedata		employeedata;

	private String				userStatus;

	private List<String>		roles;

	public Usersession()
	{
	}

	public Employeedata getEmployeedata()
	{
		return employeedata;
	}

	public void setEmployeedata(Employeedata employeedata)
	{
		this.employeedata = employeedata;
	}

	public String getUserStatus()
	{
		return userStatus;
	}

	public void setUserStatus(String userStatus)
	{
		this.userStatus = userStatus;
	}

	public List<String> getRoles()
	{
		return roles;
	}

	public void setRoles(List<String> roles)
	{
		this.roles = roles;
	}

}
