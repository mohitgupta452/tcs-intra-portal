/**
 * 
 */
package com.turningcloud.controller.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.DataFormatException;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NamedQuery;
import javax.transaction.Transactional;

import org.apache.poi.sl.draw.geom.IfElseExpression;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.turningcloud.controller.business.contract.ActivityListner;
import com.turningcloud.controller.service.AppMetaEnumeration;
import com.turningcloud.controller.service.RequestMetaData;
import com.turningcloud.model.entity.ActiveUser;
import com.turningcloud.model.entity.EmpDatainfo;
import com.turningcloud.model.entity.Employeedata;
import com.turningcloud.model.entity.LeaveBtable;
import com.turningcloud.model.entity.Payslip;
import com.turningcloud.model.entity.Requestmasterdata;
import com.turningcloud.model.entity.RoleManager;
import com.turningcloud.model.entity.UserAction;

/**
 * @author manoj
 *
 */
@Stateless
@Transactional
public class PacketDataUtils
{

	/**
	 * 
	 */

	private Boolean			authUser;

	@Inject
	CrudService				crudService;

	@Inject
	private ActivityListner	activityListner;

	@SuppressWarnings("unused")
	private DateFormat		df			= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private Date			reqDt_time	= Calendar.getInstance().getTime();

	public PacketDataUtils()
	{
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("unchecked")
	public Employeedata getEmpData(String namedQuery, Map<String, String> pairValue)
	{
		Employeedata employeedata = null;
		if (!pairValue.isEmpty())
		{
			@SuppressWarnings("unused")
			List<Employeedata> empList = crudService.findWithNamedQuery(namedQuery,
					QueryParameter.with("key", pairValue.get("key")).parameters());
			if (!empList.isEmpty())
			{
				this.setAuthUser(true);
				employeedata = empList.get(0);
			}
			return employeedata;
		}
		else
		{
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public ActiveUser getActiveUser(String namedQuery, Map<String, String> pairValue)
	{
		//ActiveUser.findAllByUpn
		Employeedata empDts = getEmpData("Employeedata.findByUpn", pairValue);
		if (!Integer.toString(empDts.getEmpDataID()).isEmpty())
		{
			@SuppressWarnings("unused")
			List<ActiveUser> userList = crudService.findWithNamedQuery(namedQuery,
					QueryParameter.with("key", empDts.getEmpDataID()).parameters());
			if (userList.size() > 0)
				return userList.get(0);
			else
			{
				return null;
			}
		}
		else
		{
			return null;
		}
	}

	public Boolean getAuthUser()
	{
		return true;
	}

	public void setAuthUser(Boolean authUser)
	{
		this.authUser = authUser;
	}

	public void updateImage(byte[] image, String imgtype, String Id)
	{
		@SuppressWarnings(
		{ "unused", "unchecked" })
		List<Employeedata> empData = crudService.findWithNamedQuery("Employeedata.findByUpn",
				QueryParameter.with("key", Id).parameters());

		Employeedata employeedata = crudService.find(Employeedata.class,
				empData.get(0).getEmpDataID());
		if (!imgtype.isEmpty() && imgtype == "gauthimg")
			employeedata.setGoogleImage(image);
		else
		{
			employeedata.setStoreImage(image);
		}
		crudService.update(employeedata);
	}

	@SuppressWarnings("unchecked")
	public <T> T retrieveEntityDatas(String namedQuery)
	{
		T entity;
		try
		{
			List<String> namedQuerylist = Arrays.asList(namedQuery.split("."));
			entity = (T) namedQuerylist.get(0);
		}
		catch (Exception e)
		{
			return null;
		}
		return entity;
	}

	@SuppressWarnings("unchecked")
	public List<UserAction> getUserActionData(String namedQuery, Map<String, String> pairValue)
	{
		List<UserAction> userAction = null;
		if (!pairValue.isEmpty())
		{
			@SuppressWarnings("unused")
			List<UserAction> empList = crudService.findWithNamedQuery(namedQuery,
					QueryParameter.with("key", pairValue.get("key")).parameters());
			if (!empList.isEmpty())
			{
				userAction = empList;
			}
		}
		return userAction;
	}

	public boolean isManager(String upn)
	{
		boolean role = false;
		Map<String, String> pairValue = new HashMap<>();
		pairValue.put("key", upn);
		if (!upn.isEmpty())
		{
			Employeedata empData = getEmpData("Employeedata.findManager", pairValue);
			if (empData != null)
				return role = true;
		}

		return role;
	}

	public List<String> roles(String upn)
	{
		Map<String, String> pairValue = new HashMap<>();
		pairValue.put("key", upn);
		List<String> rolesList = new ArrayList<>();
		if (!upn.isEmpty())
		{
			if (isManager(upn))
				rolesList.add(AppMetaEnumeration.Roles.ReportingManager.toString());
			@SuppressWarnings("unchecked")
			List<RoleManager> roleManager = crudService.findWithNamedQuery(
					"RoleManager.findAllBYUpn", QueryParameter.with("key", upn).parameters());
			if (roleManager != null)
			{
				for (RoleManager roleManager2 : roleManager)
				{
					rolesList.add(roleManager2.getRoles().toString());
				}
			}
			return rolesList;
		}

		return rolesList;
	}

	@SuppressWarnings("unchecked")
	public UserAction UserActionData(String namedQuery, Map<String, String> pairValue)
	{
		UserAction userAction = null;
		if (!pairValue.isEmpty())
		{
			@SuppressWarnings("unused")
			List<UserAction> empList = crudService.findWithNamedQuery(namedQuery, QueryParameter
					.with("key", Integer.parseInt(pairValue.get("key"))).parameters());
			if (!empList.isEmpty())
			{
				userAction = empList.get(0);
			}
		}
		return userAction;
	}

	@SuppressWarnings("unchecked")
	public Employeedata employeeByRole(String namedQuery, Map<String, String> pairValue)
	{
		Employeedata employeedata = null;
		if (!pairValue.isEmpty())
		{
			@SuppressWarnings("unused")
			List<Employeedata> empList = crudService.findWithNamedQuery(namedQuery,
					QueryParameter.with("key", pairValue.get("key")).parameters());
			if (!empList.isEmpty())
			{
				employeedata = empList.get(0);
			}
			return employeedata;
		}
		else
		{
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public LeaveBtable leaveDetails(String namedQuery, Map<String, String> pairValue)
	{
		LeaveBtable leaveBtable = null;
		if (!pairValue.isEmpty())
		{
			@SuppressWarnings("unused")
			List<LeaveBtable> leaveDetails = crudService.findWithNamedQuery(namedQuery,
					QueryParameter.with("key", Integer.parseInt(pairValue.get("key")))
							.parameters());
			if (!leaveDetails.isEmpty())
			{
				leaveBtable = leaveDetails.get(0);
			}
		}
		return leaveBtable;
	}

	@SuppressWarnings("unchecked")
	public EmpDatainfo empInformationDetails(String namedQuery, Map<String, String> pairValue)
	{
		EmpDatainfo employeedata = null;
		if (!pairValue.isEmpty())
		{
			@SuppressWarnings("unused")
			List<EmpDatainfo> empDataList = crudService.findWithNamedQuery(namedQuery,
					QueryParameter.with("key", pairValue.get("key")).parameters());
			if (!empDataList.isEmpty())
			{
				employeedata = empDataList.get(0);
			}
			return employeedata;
		}
		else
		{
			return null;
		}
	}

	public Map<String, String> requestData(Requestmasterdata requestmasterdata)
	{
		Map<String, String> attrPair = activityListner
				.accessServiceAttributes(requestmasterdata.getServiceRequest());
		Set<String> keys = attrPair.keySet();
		Map<String, String> responseDataMap = new HashMap<>();
		String jsonString = requestmasterdata.getRequestData().toString();
		JsonParser jsonParser = new JsonParser();
		JsonElement element = jsonParser.parse(jsonString);
		for (@SuppressWarnings("rawtypes")
		Iterator iterator = keys.iterator(); iterator.hasNext();)
		{
			String key = iterator.next().toString();
			String value = element.getAsJsonObject().get(key).toString().replace('"', ' ')
					.replace('[', ' ').replace(']', ' ').trim();
			String displayKey = attrPair.get(key);
			responseDataMap.put(displayKey, value);
		}
		return responseDataMap;
	}

	@SuppressWarnings("unchecked")
	public List<Requestmasterdata> requestMasterData(String namedQuery,
			Map<String, String> pairValue)
	{
		Date from = null;
		List<Requestmasterdata> requestmasterdata = null;
		try
		{
			from = this.returnStartDtf();
		}
		catch (DateTimeException e)
		{
			e.printStackTrace();
		}
		if (!pairValue.isEmpty())
		{
			@SuppressWarnings("unused")
			List<Requestmasterdata> requests = crudService.findWithNamedQuery(namedQuery,
					QueryParameter.with("key", from)
							.and("keys", Integer.parseInt(pairValue.get("key")))
							.and("service", pairValue.get("service").toString())
							.and("status", pairValue.get("status")).parameters());
			if (!requests.isEmpty())
			{
				requestmasterdata = requests;
			}
		}
		return requestmasterdata;
	}

	public Date returnStartDtf()
	{
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		List<String> list = new ArrayList<>();
		int index = 0;
		String dtftm = null;
		for (String string : dtf.format(now).split("/"))
		{
			if (index == 0)
				list.add(string);
			else if (index == 1)
				list.add(string);
			else if (index == 2)
			{
				string.split(" ");
				list.add(string.split(" ")[0]);
			}
			index++;
		}
		dtftm = list.get(0) + "-" + list.get(1) + "-" + "01" + " 00:01:02";
		Date stDate = null;
		SimpleDateFormat stmntDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try
		{
			stDate = stmntDate.parse(dtftm);
			System.out.println(stDate);
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}
		return stDate;
	}

	@SuppressWarnings("unchecked")
	public Payslip GeneratedPaySlipData(String namedQuery, Map<String, String> pairValue)
	{
		Payslip employeePaySlip = null;
		if (!pairValue.isEmpty())
		{
			@SuppressWarnings("unused")
			List<Payslip> payDataList = crudService.findWithNamedQuery(namedQuery, QueryParameter
					.with("key", Integer.parseInt(pairValue.get("key"))).parameters());
			if (!payDataList.isEmpty())
			{
				employeePaySlip = payDataList.get(0);
			}
			return employeePaySlip;
		}
		else
		{
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Payslip> getGeneratedPaySlip(String namedQuery, Map<String, Integer> pairValue)
	{
		List<Payslip> paySlip = null;
		if (!pairValue.isEmpty())
		{
			@SuppressWarnings("unused")
			List<Payslip> paySlipList = crudService.findWithNamedQuery(namedQuery,
					QueryParameter.with("key", pairValue.get("key")).parameters());
			if (!paySlipList.isEmpty())
			{
				paySlip = paySlipList;
			}
		}
		return paySlip;
	}
}
