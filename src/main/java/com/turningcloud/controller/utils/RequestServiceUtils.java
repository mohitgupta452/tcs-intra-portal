/**
 * 
 */
package com.turningcloud.controller.utils;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.research.ws.wadl.Request;

/**
 * @author manoj
 *
 */
public abstract class RequestServiceUtils
{

	/**
	 * 
	 */
	private static final Logger log = LoggerFactory.getLogger(Request.class);

	public RequestServiceUtils()
	{
	}

	public abstract void setRequestMaster(Map<String, String> requestData);

	public <T> T createEntityFromParameters(Class<T> entityClass)
	{
		T entity;
		try
		{
			entity = entityClass.newInstance();
		}
		catch (Exception e)
		{
			log.error("Cannot create new instance of class '{}'", entityClass.getName(), e);
			return null;
		}

		//		updateEntityFromParameters(entity);

		return entity;
	}
	

}
