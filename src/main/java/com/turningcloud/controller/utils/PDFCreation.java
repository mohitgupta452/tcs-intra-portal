package com.turningcloud.controller.utils;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class PDFCreation
{

	protected static final Logger	log	= LoggerFactory.getLogger(PDFCreation.class);

	private File					file;
	private Document				pdfDoc;
	private PdfPTable				my_table;
	private PdfPCell				table_cell;
	private int						colNo;
	private int						rowNo;
	private Paragraph				paragraph;
	private static PdfWriter		pdfWriter;

	public PDFCreation(String path, String fileName) throws Exception
	{

		this.file = new File(path, fileName);
		if (!(this.file.exists()))
		{
			log.debug("folders not exists..");
			(new File(file.getParent())).mkdirs();
			log.debug("folder created...");
		}

		FileOutputStream baos = new FileOutputStream(file);
		this.createPdf(baos);
	}

	public void salaryAmount(List<String> salaryList) throws IOException, DocumentException
	{
		my_table = new PdfPTable(24);
		my_table.setTotalWidth(pdfDoc.getPageSize().getWidth() - 80);
		my_table.setLockedWidth(true);
		PdfPCell paySlipAmtHeading = new PdfPCell(new Phrase("Earnings"));
		paySlipAmtHeading.setBackgroundColor(new BaseColor(Color.LIGHT_GRAY));
		paySlipAmtHeading.setColspan(10);
		my_table.addCell(paySlipAmtHeading);
		PdfPCell paySlipHeadingAmt = new PdfPCell(new Phrase("Amount"));
		paySlipHeadingAmt.setBackgroundColor(new BaseColor(Color.LIGHT_GRAY));
		paySlipHeadingAmt.setColspan(2);
		my_table.addCell(paySlipHeadingAmt);
		PdfPCell paySlipHeadingAmts = new PdfPCell(new Phrase("Deductions"));
		paySlipHeadingAmts.setBackgroundColor(new BaseColor(Color.LIGHT_GRAY));
		paySlipHeadingAmts.setColspan(10);
		my_table.addCell(paySlipHeadingAmts);
		PdfPCell paySlipHeadingAmtEarning = new PdfPCell(new Phrase("Amount"));
		paySlipHeadingAmtEarning.setBackgroundColor(new BaseColor(Color.LIGHT_GRAY));
		paySlipHeadingAmtEarning.setColspan(2);
		my_table.addCell(paySlipHeadingAmtEarning);
		pdfDoc.add(my_table);
		my_table = new PdfPTable(24);
		my_table.setTotalWidth(pdfDoc.getPageSize().getWidth() - 80);
		my_table.setLockedWidth(true);
		PdfPCell paySlipEarning = new PdfPCell(
				new Phrase(" Basic Salary \n" + "\n House Rent Allowance\n" + "\n Coneyance\n"
						+ "\n Special Allowance\n" + "\n Performance Pay\n" + "\n Deputation\n"+"\n"));
		paySlipEarning.setColspan(10);
		my_table.addCell(paySlipEarning);
		PdfPCell earningAmt = new PdfPCell(new Phrase(" " + salaryList.get(0) + ".00" + "\n\n" + " "
				+ salaryList.get(1) + ".00" + "\n\n" + " " + salaryList.get(2) + ".00" + "\n\n"
				+ " " + salaryList.get(3) + ".00" + "\n\n" + " " + salaryList.get(4) + ".00"
				+ "\n\n" + " " + salaryList.get(5)+"\n"));
		earningAmt.setColspan(2);
		my_table.addCell(earningAmt);
		PdfPCell deductionEarning = new PdfPCell(
				new Phrase(" Provident Fund \n" + "\n Professinal Tax \n" + "\n Lunch Recovery\n"
						+ "\n Transport Recovery\n" + "\n Income Tax\n"));
		deductionEarning.setColspan(10);
		my_table.addCell(deductionEarning);
		PdfPCell paySlipDeductionAmount = new PdfPCell(new Phrase(" " + salaryList.get(7) + "\n\n"
				+ " " + salaryList.get(8) + "\n\n" + " " + salaryList.get(9) + "\n\n" + " "
				+ salaryList.get(10) + "\n\n" + " " + salaryList.get(11)));
		paySlipDeductionAmount.setColspan(2);
		my_table.addCell(paySlipDeductionAmount);
		pdfDoc.add(my_table);
		my_table = new PdfPTable(24);
		my_table.setTotalWidth(pdfDoc.getPageSize().getWidth() - 80);
		my_table.setLockedWidth(true);
		Font f = new Font(FontFamily.COURIER, 10.0f, Font.BOLD, BaseColor.BLACK);
		Paragraph paragraphNetPayment = new Paragraph("Net Payment", f);
		PdfPCell netPayment = new PdfPCell(new Phrase(paragraphNetPayment));
		netPayment.setBackgroundColor(new BaseColor(Color.WHITE));
		netPayment.setColspan(18);
		netPayment.setRight(10f);
		my_table.addCell(netPayment);
		Font fs = new Font(FontFamily.COURIER, 10.0f, Font.BOLD, BaseColor.BLACK);
		Paragraph paragraph = new Paragraph("    Rs.   " + salaryList.get(6)+ "\n", fs);
		paragraph.setAlignment(Paragraph.ALIGN_CENTER);
		PdfPCell netPaymentAmount = new PdfPCell(new Phrase(paragraph));
		netPaymentAmount.setBackgroundColor(new BaseColor(Color.WHITE));
		netPaymentAmount.setColspan(6);
		my_table.addCell(netPaymentAmount);
		pdfDoc.add(my_table);
		pdfDoc.close();
	}

	private void createPdf(FileOutputStream baos) throws Exception
	{
		pdfDoc = new Document(PageSize.A4.rotate());
		pdfWriter = PdfWriter.getInstance(pdfDoc, baos);
		pdfDoc.open();

	}

	public void createTable(int colNo, int rowNo, List<String> headingList, BaseColor color,
			BaseColor textColor) throws Exception
	{
		my_table = new PdfPTable(colNo);
		this.colNo = colNo;
		this.rowNo = rowNo;
		this.createTableHeading(headingList, color, textColor);
	}

	public void createTable(int colNo, String headingSeperatedByComma, BaseColor color,
			BaseColor textColor) throws Exception
	{
		my_table = new PdfPTable(colNo);
		my_table.setWidthPercentage(100f);
		this.colNo = colNo;
		this.createTableHeading(headingSeperatedByComma, color, textColor);
	}

	public void addData(String data) throws Exception
	{
		if (data.contains("["))
		{
			data = (data).replace('[', ' ');
			data = (data).replace(']', ' ');
		}

		Font f = new Font(FontFamily.COURIER, 10.0f, Font.NORMAL, BaseColor.BLACK);
		Paragraph paragraph = new Paragraph(data, f);
		pdfDoc.add(paragraph);
	}

	// function for heading	
	public void addHeader(String data) throws Exception
	{
		if (data.contains("["))
		{
			data = (data).replace('[', ' ');
			data = (data).replace(']', ' ');
		}
		Font f = new Font(FontFamily.COURIER, 17.0f, Font.BOLD, BaseColor.BLACK);
		Paragraph paragraph = new Paragraph(data, f);
		pdfDoc.add(paragraph);
	}

	public void addDataAtCenter(String data) throws Exception
	{
		if (data.contains("["))
		{
			data = (data).replace('[', ' ');
			data = (data).replace(']', ' ');
		}

		Font f = new Font(FontFamily.COURIER, 13.0f, Font.BOLD, BaseColor.BLACK);
		Chunk c = new Chunk(data, f);
		c.setBackground(BaseColor.WHITE);

		Paragraph paragraph = new Paragraph(c);
		paragraph.setAlignment(Paragraph.ALIGN_CENTER);
		pdfDoc.add(paragraph);

	}

	public void addDataAtRight(String data) throws Exception
	{
		if (data.contains("["))
		{
			data = (data).replace('[', ' ');
			data = (data).replace(']', ' ');
		}

		Font f = new Font(FontFamily.COURIER, 13.0f, Font.NORMAL, BaseColor.BLACK);
		Chunk c = new Chunk(data, f);
		c.setBackground(BaseColor.WHITE);

		Paragraph paragraph = new Paragraph(c);
		paragraph.setAlignment(Paragraph.ALIGN_RIGHT);
		pdfDoc.add(paragraph);

	}

	public void addImage() throws Exception
	{
		PdfContentByte canvas = pdfWriter.getDirectContentUnder();
		String url = "http://localhost:8080/assets/img/logob.png";
		Image image = Image.getInstance(url);
		image.scaleAbsolute(100f, 90f);
		image.setAbsolutePosition(60f, 500f);
		canvas.addImage(image);

	}

	private void createTableHeading(List<String> headingList, BaseColor color, BaseColor textColor)
			throws Exception, Exception
	{
		if (this.colNo == headingList.size())
		{
			for (Object data : headingList)
			{
				table_cell = new PdfPCell(new Phrase(data.toString(),
						new Font(FontFamily.COURIER, 11f, Font.NORMAL, textColor)));
				table_cell.setBackgroundColor(color);
				table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table_cell.setBorder(5);
				table_cell.setBorderColor(BaseColor.BLACK);
				my_table.addCell(table_cell);
			}
		}
	}

	private void createTableHeading(String heading, BaseColor color, BaseColor textColor)
			throws Exception, Exception
	{
		String[] headingArray = heading.split(",");
		if (this.colNo == headingArray.length)
		{
			for (int i = 0; i < headingArray.length; i++)
			{
				table_cell = new PdfPCell(new Phrase(headingArray[i].toString(),
						new Font(FontFamily.COURIER, 11f, Font.NORMAL, textColor)));
				table_cell.setBackgroundColor(color);
				table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table_cell.setBorder(5);
				table_cell.setBorderColor(BaseColor.BLACK);
				my_table.addCell(table_cell);
			}
		}
	}

	public void createTableRowWithPading(String data, int rowSpan, int colSpan,
			BaseColor backGroundColor, BaseColor textColor) throws DocumentException, IOException
	{
		table_cell = new PdfPCell(
				new Phrase(data, new Font(FontFamily.COURIER, 9f, Font.NORMAL, textColor)));
		table_cell.setRowspan(rowSpan);
		table_cell.setColspan(colSpan);
		table_cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table_cell.setBackgroundColor(backGroundColor);
		my_table.addCell(table_cell);
	}

	public void createTableRow(String dataSeperatedByColon, boolean generateExtraCol)
			throws DocumentException, IOException
	{
		String[] dataArray = dataSeperatedByColon.split(":");
		if (!generateExtraCol || this.colNo == dataArray.length)
		{
			for (int i = 0; i < dataArray.length; i++)
			{
				if (dataArray[i].contains(","))
				{
					String function = (dataArray[i]).substring(1, ((dataArray[i]).length() - 1));
					String[] tempString = function.split(",");
					String str = "";
					for (int j = 0; j < tempString.length; j++)
					{
						str += "-" + tempString[j].trim() + "" + Chunk.NEWLINE;
					}
					table_cell = new PdfPCell(new Phrase(str,
							new Font(FontFamily.COURIER, 9f, Font.NORMAL, BaseColor.BLACK)));
				}

				else if (dataArray[i].contains("["))
				{
					String function = (dataArray[i]).substring(1, ((dataArray[i]).length() - 1));
					table_cell = new PdfPCell(new Phrase("-" + function,
							new Font(FontFamily.COURIER, 9f, Font.NORMAL, BaseColor.BLACK)));
				}
				else
				{
					table_cell = new PdfPCell(new Phrase(dataArray[i].toString(),
							new Font(FontFamily.COURIER, 9f, Font.NORMAL, BaseColor.BLACK)));
				}

				if (dataArray[i].contains(",") || dataArray[i].contains("["))
				{
					table_cell.setVerticalAlignment(Element.ALIGN_LEFT);
				}
				else
				{
					table_cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				}

				my_table.addCell(table_cell);
			}
		}
		else if (generateExtraCol && this.colNo >= dataArray.length)
		{
			for (int i = 0; i < dataArray.length; i++)
			{
				if (dataArray[i].contains(","))
				{
					String function = (dataArray[i]).substring(1, ((dataArray[i]).length() - 1));
					String[] tempString = function.split(",");
					String str = "";
					for (int j = 0; j < tempString.length; j++)
					{
						str += tempString[j] + "\n";
					}
					table_cell = new PdfPCell(new Phrase(str,
							new Font(FontFamily.COURIER, 10f, Font.NORMAL, BaseColor.BLACK)));
				}
				else if (dataArray[i].contains("["))
				{
					String function = (dataArray[i]).substring(1, ((dataArray[i]).length() - 1));
					table_cell = new PdfPCell(new Phrase(function,
							new Font(FontFamily.COURIER, 10f, Font.NORMAL, BaseColor.BLACK)));
				}
				else
				{
					table_cell = new PdfPCell(new Phrase(dataArray[i].toString(),
							new Font(FontFamily.COURIER, 10f, Font.NORMAL, BaseColor.BLACK)));
				}
				table_cell.setVerticalAlignment(Element.ALIGN_CENTER);
				table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				my_table.addCell(table_cell);
			}
			for (int i = dataArray.length; i < colNo; i++)
			{
				table_cell = new PdfPCell(new Phrase(""));
				my_table.addCell(table_cell);
			}
		}
		else
		{
			log.error("data is larger than coloumn please correct the data..");
		}
	}

	public void createFirstTable(List<String> cellData)
	{
		// a table with three columns
		// the cell object
		PdfPCell cell;
		my_table = new PdfPTable(2);
		my_table.setTotalWidth(pdfDoc.getPageSize().getWidth() - 80);
		my_table.setLockedWidth(true);
		// we add a cell with colspan 3
		//	        cell = new PdfPCell(new Phrase("Pay Slip Information"));
		//	        cell.setColspan(2);
		//	        table.addCell(cell);
		// now we add a cell with rowspan 2
		//	        cell = new PdfPCell(new Phrase("Cell with rowspan 2"));
		//	        cell.setRowspan(2);
		//	        table.addCell(cell);
		// we add the four remaining cells with addCell()
		if (cellData.size() != 0)
		{
			for (String string : cellData)
			{
				my_table.addCell(string);
			}
			try
			{
				pdfDoc.add(my_table);
			}
			catch (DocumentException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public void createTableRow(List<Object> dataList)
	{
		if (dataList.size() != 0)
		{
			for (Object data : dataList)
			{
				table_cell = new PdfPCell(new Phrase(data.toString()));
				my_table.addCell(table_cell);
			}
			for (int i = dataList.size(); i < colNo; i++)
			{
				table_cell = new PdfPCell(new Phrase(""));
				my_table.addCell(table_cell);
			}
		}
		else if (this.colNo >= dataList.size())
		{
			for (Object data : dataList)
			{
				table_cell = new PdfPCell(new Phrase(data.toString()));
				my_table.addCell(table_cell);
			}
			for (int i = dataList.size(); i < colNo; i++)
			{
				table_cell = new PdfPCell(new Phrase(""));
				my_table.addCell(table_cell);
			}
		}
		else if (this.rowNo <= dataList.size())
		{
			for (Object data : dataList)
			{
				table_cell = new PdfPCell(new Phrase(data.toString()));
				my_table.addCell(table_cell);
			}
			for (int i = dataList.size(); i < colNo; i++)
			{
				table_cell = new PdfPCell(new Phrase(""));
				my_table.addCell(table_cell);
			}
		}
		else if (this.colNo == dataList.size())
		{
			for (Object data : dataList)
			{
				table_cell = new PdfPCell(new Phrase(data.toString()));
				my_table.addCell(table_cell);
			}
		}
		else
		{
			log.error("data is larger than coloumn please correct the data..");
		}
	}

	public void addTable(int[] colWidths)
	{
		try
		{
			my_table.setWidths(colWidths);
			pdfDoc.add(Chunk.NEWLINE);
			pdfDoc.add(my_table);
		}
		catch (DocumentException e)
		{
			log.error(e.getMessage());
		}
	}

	public void addTable()
	{
		try
		{
			pdfDoc.add(Chunk.NEWLINE);
			pdfDoc.add(my_table);
		}
		catch (DocumentException e)
		{
			log.error(e.getMessage());
		}
	}

	public void addSpace()
	{
		try
		{
			pdfDoc.add(Chunk.NEWLINE);
		}
		catch (DocumentException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public File closePDF()
	{
		pdfDoc.close();
		return this.file;
	}

	/*
	 * public static void main(String[] args) throws Exception { List ss = new
	 * LinkedList(); ss.add("dfdsvds"); ss.add(155); ss.add("aaaa");
	 * ss.add("ccccccccccc"); ss.add(454); ss.add(5.154); ss.add("aaaa");
	 * ss.add("ccccccccccc"); ss.add("dfdsvds"); ss.add("eeeeeee"); PDFCreation
	 * pdfCreation = new PDFCreation("e:", "hxDSADSDSsh.pdf");
	 * pdfCreation.addData("sadsadsdss"); pdfCreation.addData("sadsadsdss");
	 * pdfCreation.addData("sadsadsdss"); pdfCreation.createTable(5,
	 * "fdf,fdsf,fsdf,fsdf,dfsef");
	 * pdfCreation.createTableRow("21,212,12,12,121");
	 * pdfCreation.createTableRow("re,ere,er,er,as");
	 * pdfCreation.createTableRow(
	 * "dsfsdfadasdasdads,dsadadsdasdeWSQWQ,WEWEWEWREWTREY,RYTUYTIUIYTHGSDFDDSA,5"
	 * ); pdfCreation.addTable(); pdfCreation.closePDF();
	 * 
	 * }
	 */

}
