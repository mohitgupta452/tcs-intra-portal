/**
 * 
 */
package com.turningcloud.controller.utils;

/**
 * @author manoj
 *
 */

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.enterprise.context.RequestScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.turningcloud.controller.business.contract.ActivityListner;
import com.turningcloud.controller.service.AppMetaEnumeration;
import com.turningcloud.controller.service.AppMetaEnumeration.ProcessType;

@RequestScoped
public abstract class ProcessHelper implements ActivityListner
{
	private Logger			log	= LoggerFactory.getLogger(ProcessHelper.class);

	//	protected ProcessType	processType;
	private List<String>	processType;

	protected Properties	processProp;

	private List<String>	serviceList;

	private List<String>	ownerList;

	protected String		service;

	InputStream				inputStream;

	protected Properties	serviceAttr;

	public ProcessHelper()
	{

		processProp = new Properties();
		serviceAttr = new Properties();

		try
		{
			String propFileName = "properties/config.properties";
			String serviceAttributes = "properties/requestMetaData.properties";

			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
			processProp.load(this.getClass().getClassLoader().getResourceAsStream(propFileName));
			serviceAttr
					.load(this.getClass().getClassLoader().getResourceAsStream(serviceAttributes));
			serviceList = Arrays.asList(processProp.getProperty("Servicelist").split("\\."));
			String processKey = AppMetaEnumeration.ProcessType.ApprovalProcess.getProcessType()
					.toString();
			processType = Arrays.asList(processProp.getProperty(processKey).split("\\."));
			ownerList = Arrays
					.asList(processProp.getProperty("NextOwner." + processKey).split("\\."));

		}
		catch (IOException e)
		{
			log.debug("file not found", e.getMessage());
			e.printStackTrace();
		}
		System.out.println("Executed the process helper");

	}

	public List<String> getOwnerList()
	{
		return ownerList;
	}

	public void setOwnerList(List<String> ownerList)
	{
		this.ownerList = ownerList;
	}

	public List<String> getProcessType()
	{
		return processType;
	}

	public String getService()
	{
		return service;
	}

	public void setService(String service)
	{
		this.service = service;
	}

	public void setProcessType(List<String> processType)
	{
		this.processType = processType;
	}

	public Properties getProcessProp()
	{
		return processProp;
	}

	public void setProcessProp(Properties processProp)
	{
		this.processProp = processProp;
	}

	public List<String> getServiceList()
	{
		return serviceList;
	}

	public void setServiceList(List<String> serviceList)
	{
		this.serviceList = serviceList;
	}

}
