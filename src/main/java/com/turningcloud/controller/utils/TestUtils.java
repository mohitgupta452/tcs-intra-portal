/**
 * 
 */
package com.turningcloud.controller.utils;

import java.io.File;

/**
 * @author manoj
 *
 */

import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
 
public class TestUtils {
 
	public static final String DEST = "/home/manoj/tcsportal/slip.pdf";
	 public static void main(String[] args) throws IOException,
     DocumentException {
 File file = new File(DEST);
 file.getParentFile().mkdirs();
 new TestUtils().createPdf(DEST);
}
public void createPdf(String dest) throws IOException, DocumentException {
 Document document = new Document(PageSize.A3.rotate());
 PdfWriter.getInstance(document, new FileOutputStream(dest));
 document.open();
 PdfPTable table = new PdfPTable(24);
 table.setTotalWidth(document.getPageSize().getWidth()-80);
 table.setLockedWidth(true);
 PdfPCell contractor = new PdfPCell(new Phrase(" Basic Salary \n"+"\n House Rent Allowance\n"+"\n Special Allowance\n"+"\n Coneyance\n"+"\n Performance Pay\n"+"\n Deputation\n"));
 contractor.setColspan(10);
 table.addCell(contractor);
 PdfPCell workType = new PdfPCell(new Phrase());
 workType.setColspan(2);
 table.addCell(workType);
 PdfPCell supervisor = new PdfPCell(new Phrase(" Provident Fund \n"+"\n Professinal Tax \n"+"\n Lunch Recovery\n"+"\n Transport Recovery\n"+"\n Income Tax\n"));
 supervisor.setColspan(10);
 table.addCell(supervisor);
 PdfPCell paySlipHead = new PdfPCell(new Phrase(""));
 paySlipHead.setColspan(2);
 table.addCell(paySlipHead);
// PdfPCell paySlipMonth = new PdfPCell(new Phrase("XXXXXXX"));
// paySlipMonth.setColspan(2);
// table.addCell(paySlipMonth);
// PdfPCell blank = new PdfPCell(new Phrase(""));
// blank.setColspan(9);
// table.addCell(blank);
 document.add(table);
 document.close();
}
}