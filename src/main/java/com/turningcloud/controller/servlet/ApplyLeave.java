package com.turningcloud.controller.servlet;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.formula.functions.Days360;
import org.joda.time.DateMidnight;
import org.joda.time.Days;

import com.turningcloud.controller.business.contract.RequestManager;
import com.turningcloud.controller.service.AlertMessages;

/**
 * Servlet implementation class ApplyLeave
 */
@WebServlet("/apleave")
public class ApplyLeave extends HttpServlet
{
	@Inject
	private RequestManager		requestManager;

	private static final long	serialVersionUID	= 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ApplyLeave()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		@SuppressWarnings("unused")
		Map<String, String[]> aplparam = request.getParameterMap();
		String apl = request.getParameter("apls");
		String st = request.getParameter("leaveType");
		String fromDate = request.getParameter("fromdate");
		DateMidnight start = new DateMidnight(request.getParameter("fromdate"));
		DateMidnight end = new DateMidnight(request.getParameter("todate"));
		Days d = Days.daysBetween(start, end);
		int days = d.getDays();
		String[] str = new String[]
		{ st };
		String[] dtr = new String[]
		{ Integer.toString(days) };
		aplparam.put("ServiceType", str);
		aplparam.put("TotalDays", dtr);
		if (apl.equalsIgnoreCase("apl") && !apl.isEmpty())
		{
			String status = requestManager.setRequestMaster(aplparam);
			HttpSession session = request.getSession(false);
			session.setAttribute("isPRG", "true");
			if (!status.isEmpty() & status != null)
				response.sendRedirect(
						"PRGServlet?status=success&url=leavemanagement.jsp&alertMessage="
								+ AlertMessages.requestSent);
			else
				response.sendRedirect(
						"PRGServlet?status=success&url=leavemanagement.jsp&alertMessage="
								+ AlertMessages.requestFailed);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
