package com.turningcloud.controller.servlet;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;
import javax.jws.soap.InitParam;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.turningcloud.controller.business.contract.ActionMasters;
import com.turningcloud.view.beans.views.downloadpdf;

/**
 * Servlet implementation class DownloadResponse
 */
@WebServlet("/DownloadResponse")
public class DownloadResponse extends HttpServlet
{
	private static final long	serialVersionUID	= 1L;

	Logger						log					= LoggerFactory.getLogger(getClass());

	@Inject
	private ActionMasters		actionMasters;

	@Inject
	private downloadpdf			downloadpdf;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DownloadResponse()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		log.debug("------Download PDF------");

		String requestId = request.getParameter("requestId");
		String service = request.getParameter("service");
		String fileName = request.getParameter("fileName");

		Map jsonObject = new Gson().fromJson(actionMasters.getResonseData(requestId, service),
				Map.class);

		File filePath = downloadpdf.generatePdf(jsonObject);

		String queryStr = filePath.getPath()
				+ (filePath != null ? "|" + fileName : "");

		request.getRequestDispatcher("DownloadController?filename=" + queryStr).forward(request,
				response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
