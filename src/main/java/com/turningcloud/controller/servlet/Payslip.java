package com.turningcloud.controller.servlet;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.turningcloud.controller.business.contract.RequestManager;
import com.turningcloud.controller.service.AlertMessages;

/**
 * Servlet implementation class Payslip
 */
@WebServlet("/Payslip")
public class Payslip extends HttpServlet
{
	private static final long	serialVersionUID	= 1L;

	@Inject
	private RequestManager		requestManager;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Payslip()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		String status = null;
		response.getWriter().append("Served at: ").append(request.getContextPath());
		Map<String, String[]> requestMap = request.getParameterMap();
		//		Set<String> keySet = requestMap.keySet();
		String requestAction = request.getParameter("edetailsSub");
		String requestActions = request.getParameter("edetailsGen");
		if (requestAction != null && requestAction.equalsIgnoreCase("eDetailSub"))
			status = requestManager.generatePaySlips(requestMap, requestAction);
		else if (requestActions != null && requestActions.equalsIgnoreCase("ePayGen"))
			status = requestManager.generatePaySlips(requestMap, requestActions);
		HttpSession session = request.getSession(false);
		session.setAttribute("isPRG", "true");
		if (status.equalsIgnoreCase("Generated")||status.equalsIgnoreCase("Pending"))
			response.sendRedirect("PRGServlet?status=success&url=Payslip.jsp&alertMessage="
					+ AlertMessages.requestSent);
		
		else
			response.sendRedirect("PRGServlet?status=success&url=Payslip.jsp&alertMessage="
					+ AlertMessages.requestFailed);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
