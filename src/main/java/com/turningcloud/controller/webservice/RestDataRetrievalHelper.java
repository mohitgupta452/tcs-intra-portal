package com.turningcloud.controller.webservice;

import java.io.Serializable;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.turningcloud.controller.business.contract.ActionMasters;
import com.turningcloud.controller.service.RequestMetaData;

@Local
@Stateless
@Path("/data")
public class RestDataRetrievalHelper implements Serializable
{

	private static final long	serialVersionUID	= 5406295124886195991L;

	private Logger				log					= LoggerFactory.getLogger(getClass());

	@Inject
	private ActionMasters		actionMasters;

	private Response okEmptyJsonResponse()
	{
		return Response.ok("{}", MediaType.APPLICATION_JSON).build();
	}

	@GET
	@Path("/showEmpdata/{empCode}")
	@javax.ws.rs.Produces("application/json")
	public Response showEmployeeData(@PathParam("empCode") String empCode)
	{
		Map<String, String> responseData = actionMasters.getEmpList(empCode);
		log.debug("Response Data : {}", responseData);
		if (!responseData.isEmpty())
		{
			responseData.put("responseCode", RequestMetaData.successCode);
			responseData.put("responseStatus", RequestMetaData.successMsg);
		}
		else
		{
			responseData.put("responseCode", RequestMetaData.failureCode);
			responseData.put("responseStatus", RequestMetaData.failureStatus);
		}
		return Response.ok(responseData).build();
	}

}
