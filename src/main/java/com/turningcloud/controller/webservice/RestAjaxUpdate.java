package com.turningcloud.controller.webservice;

import java.io.Serializable;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.turningcloud.controller.business.contract.ActionMasters;

@Local
@Stateless
@Path("update")
public class RestAjaxUpdate implements Serializable
{
	private static final long	serialVersionUID	= 5975416992295063271L;

	private Logger				log					= LoggerFactory.getLogger(getClass());

	@Inject
	private ActionMasters		actionMasters;

	private Response okEmptyJsonResponse()
	{
		return Response.ok("{}", MediaType.APPLICATION_JSON).build();
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/cancelrequest/{requestID}")
	public Response cancelrequest(@PathParam("requestID") int requestID)
	{
		String responseJson = "{\"status\" :\"" + actionMasters.cancelRequest(requestID) + "\"}";
		log.debug("response data : {}", responseJson);
		return Response.ok(responseJson.toString(), MediaType.APPLICATION_JSON).build();
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/requestAction/{requestId}/{action}")
	public Response actionOnRequest(@PathParam("requestId") int requestId,
			@PathParam("action") String action)
	{
		Map<String, String> responseData = actionMasters.actionOnRequest(requestId, action);
		String responseJson = "{\"status\" :\"" + responseData.get("status") + ","
				+ "\"leaveStatus\" :\"" + responseData.get("leaveStatus") + "\"}";
		log.debug("response data : {}", responseJson);
		return Response.ok(responseJson.toString(), MediaType.APPLICATION_JSON).build();

	}

}
