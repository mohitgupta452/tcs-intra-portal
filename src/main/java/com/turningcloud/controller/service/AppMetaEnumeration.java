/**
 * 
 */
package com.turningcloud.controller.service;

import javax.ejb.Local;

/**
 * @author manoj
 *
 */
public interface AppMetaEnumeration
{

	public enum Services
	{
		LeaveApplication("LeaveApplication"), PaySlip("Pay Slip");

		private Services(String service)
		{
			this.Services = service;
		}

		private String Services;

		public String getService()
		{
			return Services;
		}

	}

	public enum ProcessType
	{
		ApprovalProcess("Approvalprocess"), NonApprovalProcess(
				"NonApprovalprocess"), BiFuricationProcess("Bifuricationprocess"), IndirectProcess(
						"Indirectprocess"), DirectProcess("Directprocess");

		private ProcessType(String processType)
		{
			this.ProcessType = processType;
		}

		private String ProcessType;

		public String getProcessType()
		{
			return ProcessType;
		}
	}

	public enum Roles
	{
		ReportingManager("Reporting Manager"), Administrator("Administrator"), HRManager(
				"Human Resources");

		private Roles(String roles)
		{
			this.roles = roles;
		}

		private String roles;

		public String getRoles()
		{
			return roles;
		}
	}

}
