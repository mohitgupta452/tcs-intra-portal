/**
 * 
 */
package com.turningcloud.controller.service;

/**
 * @author manoj
 *
 */
public interface AlertMessages
{
	String requestSent = " Your request has been submitted successfully ";
	String requestDelete = "Successfully deleted";
	String requestFailed = "Your request can't be completed !";
}
