/**
 * 
 */
package com.turningcloud.controller.service;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;

/**
 * @author manoj
 *
 */
public interface RequestMetaData
{

	String	Id				= "ID";
	String	Name			= "Name";
	String	RequestStatus	= "Status";
	String	Action			= "Action";
	String	CreationDate	= "Create Date";
	String	UpdationDate	= "Update Date";
	String	CreatedBy		= "CreatedBy";
	String	Ip				= "IP Address";
	String  successCode     = "1000";
	String  successMsg      = "Data Successfully retrieved";
	String  failureCode		= "1001";
	String  failureStatus	= "Request Cannot proceed ! Please try after some time ";

	public enum Status
	{
		Pending, Cancelled, Completed, Hold, Processing, Rejected, Accepted, InProgress, UnderProcess, Active, OnHold, Removed, Deleted, LeaveTaken,Generated, Invalid;
	}

	public enum UserAction
	{
		Pending, Cancelled, Completed, Hold, Processing, Reject, Accept, InProgress, UnderProcess, Accepted, Rejected, Active, OnHold, Removed, Deleted;
	}

	public enum LeaveType
	{
		casual, paid, sick;
	}

	public enum ActionType
	{
		generateSlip, savePaySlipData;
	}
	public enum Actions
	{
		NoAction("No Action"), ApprovedByRM("Approved By RM"), ApprovedByHR(
				"Approved By HR"), SentToRequestor("Redirect to Applicant"), PendingByRM(
						"Pending By RM"), PendingByHR("Pending By HR"), RejectedByHR(
								"Rejected By HR"), RejectedByRM(
										"Rejected By RM"), Completed("Actioned By HR");
		
		

		private String Actions;

		Actions(String actions)
		{
			this.Actions = actions;
		}

		public String getActions()
		{
			return Actions;
		}

	}
	
}
