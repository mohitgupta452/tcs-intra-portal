/**
 * 
 */
package com.turningcloud.controller.business.contract;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.enterprise.context.RequestScoped;

import com.turningcloud.model.entity.EmpDatainfo;
import com.turningcloud.model.entity.Employeedata;
import com.turningcloud.model.entity.Requestmasterdata;

/**
 * @author manoj
 *
 */
@Local
public interface ActionMasters
{
	public String createUserAction(Map<String, String> requestData, Requestmasterdata request);

	public String cancelRequest(int requestID);

	public Map<String, String> actionOnRequest(int requestId, String action);

	public Map<String, Object> empInformation(String upn);

	public Map<String, String> getEmpList(String empId);
	
	public EmpDatainfo getEmployeeDateByID(String upn);
	
	public List<String> getEmpCode ();
	
	public String getResonseData(String requestId , String service);
	
	public List<Map<String, Object>> generatedPaySlip(int empDataId);
}
