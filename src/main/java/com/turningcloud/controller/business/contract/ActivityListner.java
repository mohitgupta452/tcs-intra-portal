/**
 * 
 */
package com.turningcloud.controller.business.contract;

import java.util.Map;

import javax.ejb.Local;
import javax.enterprise.context.RequestScoped;

import com.turningcloud.controller.service.AppMetaEnumeration.ProcessType;

/**
 * @author manoj
 *
 */
@RequestScoped
public interface ActivityListner
{
	public ProcessType processType(String activeServiceRequest);

	public String requestNextOwner();

	public Map<String, String> accessServiceAttributes(String serviceRequest);
}
