/**
 * 
 */
package com.turningcloud.controller.business.contract;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.enterprise.context.RequestScoped;

import com.turningcloud.model.entity.EmpDatainfo;
import com.turningcloud.model.entity.Payslip;

/**
 * @author manoj
 *
 */
@RequestScoped
public interface serviceRequestUtil
{

	public List<Map<String, Object>> requestdata(String serviceRequest, String namedQuery,
			String responseKey);

	public Map<String, Object> leaveDetails();

	public Map<String, Object> employeeDetails(EmpDatainfo empDatainfo);

	public String paySlipsParam(Map<String, String[]> requestParam, String actionType);

	public Map<String, String> generatePaySlipMap(Payslip payslip);

	public Map<String, Object> mapGeneratedPaySlip(Payslip payslip);

}
