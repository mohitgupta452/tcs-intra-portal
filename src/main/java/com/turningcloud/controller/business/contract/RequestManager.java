/**
 * 
 */
package com.turningcloud.controller.business.contract;

import java.util.Map;

import javax.ejb.Local;

/**
 * @author manoj
 *
 */
@Local
public interface RequestManager
{
	public String setRequestMaster(Map<String, String[]> requestParam);

	public String generatePaySlips(Map<String, String[]> requestParam, String Action);

}
