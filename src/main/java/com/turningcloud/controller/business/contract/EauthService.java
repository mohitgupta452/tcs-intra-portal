/**
 * 
 */
package com.turningcloud.controller.business.contract;

import javax.ejb.Local;

/**
 * @author manoj
 *
 */
@Local
public interface EauthService
{

	public boolean isAuthenticated(String accessUser);

	public void userSetting(String lguser, String actionType);

}