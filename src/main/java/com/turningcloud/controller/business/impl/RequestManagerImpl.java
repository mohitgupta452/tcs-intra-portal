package com.turningcloud.controller.business.impl;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;

import com.google.gson.Gson;
import com.turningcloud.controller.business.contract.ActionMasters;
import com.turningcloud.controller.business.contract.ActivityListner;
import com.turningcloud.controller.business.contract.RequestManager;
import com.turningcloud.controller.business.contract.serviceRequestUtil;
import com.turningcloud.controller.service.AppMetaEnumeration;
import com.turningcloud.controller.service.AppMetaEnumeration.ProcessType;
import com.turningcloud.controller.service.RequestMetaData;
import com.turningcloud.controller.utils.CrudService;
import com.turningcloud.controller.utils.PacketDataUtils;
import com.turningcloud.controller.utils.ProcessHelper;
import com.turningcloud.model.entity.Requestmasterdata;
import com.turningcloud.view.beans.views.Usersession;

/**
 * @author manoj
 *
 */
@Stateless
@Transactional
public class RequestManagerImpl implements RequestManager
{

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	@Inject
	private PacketDataUtils		packetDataUtils;

	@Inject
	private Usersession			usData;

	@Inject
	private ActivityListner		processListner;

	@Inject
	private CrudService			serveRequest;

	@Inject
	private ActionMasters		actionMasters;

	private String				requestStatus;

	@Inject
	private serviceRequestUtil	serviceRequestUtils;

	public RequestManagerImpl()
	{
	}

	@Override
	public String setRequestMaster(Map<String, String[]> requestParam)
	{
		Requestmasterdata serviceRequest = new Requestmasterdata();
		Gson gson = new Gson();
		String requestData = gson.toJson(requestParam);
		serviceRequest.setRequestData(requestData);
		/*Map<String, String> entrySet = new HashMap<>();
		entrySet.put("key", Integer.toString(usData.getEmployeedata().getEmpDataID()));
		packetDataUtils.getEmpData("Employeedata.findByUpn", entrySet);*/
		String service = gson.toJson(requestParam.get("service")).toString().replace('"', ' ')
				.replace('[', ' ').replace(']', ' ').trim();
		serviceRequest.setRequestedBy(usData.getEmployeedata().getEmpDataID());
		DateFormat df = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		Date reqDt_time = Calendar.getInstance().getTime();
		String loggedDate = df.format(reqDt_time);
		serviceRequest.setRequestDate_Time(reqDt_time);
		serviceRequest.setStatus(RequestMetaData.Status.Pending.toString());
		serviceRequest.setServiceRequest(service);
		String servicetype = gson.toJson(requestParam.get("ServiceType")).toString();
		int startindex = servicetype.indexOf('"') + 1;
		int lastindex = servicetype.length() - startindex;
		serviceRequest.setServiceType(servicetype.substring(startindex, lastindex));
		ProcessType processflow = processListner.processType(service);
		String nextRequestOwner = processListner.requestNextOwner();
		Map<String, String> userActionOnRequest = new HashMap<>();
		serviceRequest.setUpdatedOn(reqDt_time);
		serviceRequest.setAction(RequestMetaData.Actions.NoAction.toString());
		serviceRequest.setIsComplete((byte) 0);
		try
		{
			serviceRequest.setIpAddress(InetAddress.getLocalHost().toString());
		}
		catch (UnknownHostException e)
		{
			e.printStackTrace();
		}
		if (processflow.toString().equalsIgnoreCase(ProcessType.ApprovalProcess.toString()))
		{
			userActionOnRequest.put("nextOwner", nextRequestOwner);
			userActionOnRequest.put("action", RequestMetaData.Actions.PendingByRM.toString());
			userActionOnRequest.put("actionOn", loggedDate);
			userActionOnRequest.put("lactionOn", loggedDate);
			userActionOnRequest.put("status",
					RequestMetaData.Actions.PendingByRM.getActions().toString());
			userActionOnRequest.put("ownerUpn",
					usData.getEmployeedata().getManager().toString().toLowerCase());
		}
		if (!actionMasters.createUserAction(userActionOnRequest, serviceRequest).isEmpty())
			requestStatus = actionMasters.createUserAction(userActionOnRequest, serviceRequest);
		return requestStatus;

	}

	@Override
	public String generatePaySlips(Map<String, String[]> requestParam, String action)
	{
		String status = null;
		if (action.equalsIgnoreCase("eDetailSub") && !action.isEmpty())
			status = serviceRequestUtils.paySlipsParam(requestParam,
					RequestMetaData.ActionType.savePaySlipData.toString());
		else if (action.equalsIgnoreCase("ePayGen"))
			status = serviceRequestUtils.paySlipsParam(requestParam,
					RequestMetaData.ActionType.generateSlip.toString());

		return status;
	}

}
