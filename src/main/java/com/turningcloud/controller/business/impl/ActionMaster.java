/**
 * 
 */
package com.turningcloud.controller.business.impl;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.turningcloud.controller.business.contract.ActionMasters;
import com.turningcloud.controller.business.contract.ActivityListner;
import com.turningcloud.controller.business.contract.serviceRequestUtil;
import com.turningcloud.controller.service.AppMetaEnumeration;
import com.turningcloud.controller.service.RequestMetaData;
import com.turningcloud.controller.utils.CrudService;
import com.turningcloud.controller.utils.PacketDataUtils;
import com.turningcloud.controller.utils.QueryParameter;
import com.turningcloud.model.entity.EmpDatainfo;
import com.turningcloud.model.entity.Employeedata;
import com.turningcloud.model.entity.LeaveBtable;
import com.turningcloud.model.entity.Payslip;
import com.turningcloud.model.entity.Requestmasterdata;
import com.turningcloud.model.entity.UserAction;
import com.turningcloud.view.beans.views.Usersession;

/**
 * @author manoj
 *
 */
@Stateless
@Transactional
public class ActionMaster implements ActionMasters
{

	/**
	 * 
	 */
	@Inject
	private CrudService			serviceAction;

	@Inject
	private Usersession			usersession;

	@Inject
	private PacketDataUtils		packetDataUtils;

	@Inject
	private ActivityListner		activityListner;

	@Inject
	private serviceRequestUtil	utilService;

	@SuppressWarnings("unused")
	private DateFormat			df			= new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
	private Date				reqDt_time	= Calendar.getInstance().getTime();

	private Byte				isComplete	= 1;

	private int					el, cl, sl;

	private Date				fromDate;

	public ActionMaster()
	{
	}

	@Override
	public String createUserAction(Map<String, String> requestData, Requestmasterdata request)
	{
		String status = serviceAction.create(request).getStatus();
		UserAction actionOnrequest = new UserAction();
		actionOnrequest.setRequestmasterdata(request);
		actionOnrequest.setAction(requestData.get("action"));
		actionOnrequest.setNextOwnerRole(requestData.get("nextOwner"));
		actionOnrequest.setNextOwnerUpn(requestData.get("ownerUpn"));
		actionOnrequest.setStatus(requestData.get("status"));
		actionOnrequest.setLactionOn(reqDt_time);
		actionOnrequest.setActionOn(reqDt_time);
		serviceAction.create(actionOnrequest);
		return status;
	}

	@Override
	public String cancelRequest(int requestID)
	{
		Byte status = 0;
		Requestmasterdata requestMaster = serviceAction.find(Requestmasterdata.class, requestID);
		requestMaster.setStatus(RequestMetaData.Status.Cancelled.toString());
		if (requestMaster.getIsComplete() == status)
		{
			requestMaster.setIsComplete(isComplete);
			UserAction userAction = updateActions(requestMaster,
					RequestMetaData.Status.Cancelled.toString());
			userAction.setStatus(RequestMetaData.Status.Cancelled.toString());
			userAction.setNextOwnerRole(RequestMetaData.Actions.SentToRequestor.toString());
			userAction.setNextOwnerUpn(
					usersession.getEmployeedata().getUserPrincipalName().toLowerCase().toString());
			userAction.setActionOn(reqDt_time);
			userAction.setRequestmasterdata(requestMaster);
			//			serviceAction.update(userAction);
			return serviceAction.update(userAction).getStatus();
		}
		else
		{
			return "You Can't Cancel the request";
		}
	}

	public UserAction updateActions(Requestmasterdata requestData, String status)
	{
		UserAction userAction = serviceAction.find(UserAction.class, requestData.getReqId());
		return userAction;
	}

	@Override
	public Map<String, String> actionOnRequest(int requestId, String action)
	{
		Map<String, String> pairValue = new HashMap<>();
		Requestmasterdata requestMaster = serviceAction.find(Requestmasterdata.class, requestId);
		pairValue.put("key", Integer.toString(requestId));
		UserAction actionMaster = packetDataUtils.UserActionData("UserActionData", pairValue);
		actionMaster.setActionOn(reqDt_time);
		actionMaster.setStatus(RequestMetaData.Status.Accepted.toString());
		pairValue.clear();
		pairValue.put("key", AppMetaEnumeration.Roles.HRManager.toString());
		Employeedata employeedata = packetDataUtils.employeeByRole("EmpployeeDataByRole",
				pairValue);
		Map<String, String> responseMessage = new HashMap<>();
		String status = actionMaster.getRequestmasterdata().getStatus();
		if (usersession.getRoles().contains(AppMetaEnumeration.Roles.ReportingManager.toString()))
		{
			if (action.equalsIgnoreCase(RequestMetaData.UserAction.Accept.toString())
					&& usersession.getEmployeedata().getUserPrincipalName()
							.equalsIgnoreCase(actionMaster.getNextOwnerUpn()))
			{
				actionMaster.setAction(RequestMetaData.Actions.ApprovedByRM.toString());
				actionMaster.setNextOwnerRole(RequestMetaData.Actions.PendingByHR.toString());
				actionMaster.setNextOwnerUpn(employeedata.getUserPrincipalName().toString());
				actionMaster.setStatus(RequestMetaData.Actions.ApprovedByRM.getActions());
				requestMaster.setStatus(RequestMetaData.Actions.ApprovedByRM.getActions());
				actionMaster.setRequestmasterdata(requestMaster);

			}
			else if (action.equalsIgnoreCase(RequestMetaData.UserAction.Reject.toString())
					&& usersession.getEmployeedata().getUserPrincipalName()
							.equalsIgnoreCase(actionMaster.getNextOwnerUpn()))
			{
				actionMaster.setAction(RequestMetaData.Actions.RejectedByRM.toString());
				actionMaster.setStatus(RequestMetaData.Actions.RejectedByRM.getActions());
				actionMaster.setNextOwnerRole(RequestMetaData.Actions.SentToRequestor.toString());
				actionMaster.setNextOwnerUpn(usersession.getEmployeedata().getUserPrincipalName());
				requestMaster.setStatus(RequestMetaData.Actions.SentToRequestor.getActions());
				actionMaster.setRequestmasterdata(requestMaster);
			}

		}
		else if (usersession.getRoles().contains(AppMetaEnumeration.Roles.HRManager.toString()))
		{
			if (action.equalsIgnoreCase(RequestMetaData.UserAction.Accept.toString())
					&& usersession.getEmployeedata().getUserPrincipalName()
							.equalsIgnoreCase(actionMaster.getNextOwnerUpn()))
			{
				actionMaster.setAction(RequestMetaData.Actions.ApprovedByHR.toString());
				actionMaster.setStatus("Approved");
				requestMaster.setIsComplete(isComplete);
				if (actionMaster.getRequestmasterdata().getServiceRequest()
						.equalsIgnoreCase(AppMetaEnumeration.Services.LeaveApplication.toString())
						&& leaveManagement(
								Integer.parseInt(renderRequestMetaData(actionMaster).get("Days")),
								actionMaster, renderRequestMetaData(actionMaster).get("Service")))
					responseMessage.put("leaveStatus",
							RequestMetaData.Status.LeaveTaken.toString());
				else
					responseMessage.put("leaveStatus", "Please contact to the Administrator");

			}
			else if (action.equalsIgnoreCase(RequestMetaData.UserAction.Reject.toString())
					&& usersession.getEmployeedata().getUserPrincipalName()
							.equalsIgnoreCase(actionMaster.getNextOwnerUpn()))
			{
				actionMaster.setAction(RequestMetaData.Actions.RejectedByHR.toString());
				actionMaster.setStatus(RequestMetaData.Actions.RejectedByHR.getActions());
			}
			actionMaster.setNextOwnerRole(RequestMetaData.Actions.SentToRequestor.toString());
			actionMaster.setNextOwnerUpn(usersession.getEmployeedata().getUserPrincipalName());
			requestMaster.setStatus(RequestMetaData.Actions.Completed.getActions());
			actionMaster.setRequestmasterdata(requestMaster);
		}
		actionMaster.setLactionOn(reqDt_time);
		actionMaster.setActionOn(reqDt_time);
		actionMaster.setRequestmasterdata(requestMaster);
		if (status.equalsIgnoreCase(serviceAction.update(actionMaster).getStatus()))
			responseMessage.put("status", "You are not authenticated for this action !!");
		else
			responseMessage.put("status", serviceAction.update(actionMaster).getStatus());
		return responseMessage;
	}

	public Map<String, String> renderRequestMetaData(UserAction userAction)
	{
		Map<String, String> attrPair = activityListner
				.accessServiceAttributes(userAction.getRequestmasterdata().getServiceRequest());
		Set<String> keys = attrPair.keySet();
		Map<String, String> responseDataMap = new HashMap<>();
		String jsonString = userAction.getRequestmasterdata().getRequestData().toString();
		JsonParser jsonParser = new JsonParser();
		JsonElement element = jsonParser.parse(jsonString);
		for (@SuppressWarnings("rawtypes")
		Iterator iterator = keys.iterator(); iterator.hasNext();)
		{
			String key = iterator.next().toString();
			String value = element.getAsJsonObject().get(key).toString().replace('"', ' ')
					.replace('[', ' ').replace(']', ' ').trim();
			String displayKey = attrPair.get(key);
			responseDataMap.put(displayKey, value);
		}
		return responseDataMap;
	}

	public boolean leaveManagement(int al, UserAction userAction, String service)
	{
		boolean leaveStatus = false;
		Map<String, String> pairValue = new HashMap<>();
		pairValue.put("key", Integer.toString(userAction.getRequestmasterdata().getRequestedBy()));
		LeaveBtable leaveDetails = packetDataUtils.leaveDetails("BalanceLeave", pairValue);
		leaveDetails.setStatus(RequestMetaData.Status.LeaveTaken.toString());
		leaveDetails.setUpdatedAt(reqDt_time);
		try
		{
			leaveDetails.setUpdatedIp(InetAddress.getLocalHost().toString());
		}
		catch (UnknownHostException e)
		{
			e.printStackTrace();
		}
		if (leaveDetails != null)
		{
			if (leaveDetails.getCasualLeave() > 0 && leaveDetails.getCasualLeave() - al >= 0
					&& service.equalsIgnoreCase(RequestMetaData.LeaveType.casual.toString()))
			{
				leaveDetails.setCasualLeave(leaveDetails.getCasualLeave() - al);
				if (serviceAction.update(leaveDetails).getStatus()
						.equalsIgnoreCase(RequestMetaData.Status.LeaveTaken.toString()))
					leaveStatus = true;
			}
			else if (leaveDetails.getEarnedLeave() > 0 && leaveDetails.getEarnedLeave() - al >= 0
					&& service.equalsIgnoreCase(RequestMetaData.LeaveType.paid.toString()))
			{
				leaveDetails.setEarnedLeave(leaveDetails.getEarnedLeave() - al);
				if (serviceAction.update(leaveDetails).getStatus()
						.equalsIgnoreCase(RequestMetaData.Status.LeaveTaken.toString()))
					leaveStatus = true;
			}
			else if (leaveDetails.getSickLeave() > 0 && leaveDetails.getSickLeave() - al >= 0
					&& service.equalsIgnoreCase(RequestMetaData.LeaveType.sick.toString()))
			{
				leaveDetails.setSickLeave(leaveDetails.getSickLeave() - al);
				if (serviceAction.update(leaveDetails).getStatus()
						.equalsIgnoreCase(RequestMetaData.Status.LeaveTaken.toString()))
					leaveStatus = true;
			}

		}
		return leaveStatus;
	}

	@Override
	public Map<String, Object> empInformation(String upn)
	{
		EmpDatainfo empAllDetails = null;
		Map<String, String> pairValue = new HashMap<>();
		@SuppressWarnings("unused")
		Map<String, Object> employeeDetails = new HashMap<>();
		String activeUPN = usersession.getEmployeedata().getUserPrincipalName().toString();
		if (upn.equalsIgnoreCase(activeUPN))
		{
			pairValue.put("key", activeUPN);
			empAllDetails = packetDataUtils.empInformationDetails("EmpAllDetails", pairValue);
		}
		return utilService.employeeDetails(empAllDetails);
	}

	public Map<String, String> getEmpList(String empCode)
	{
		@SuppressWarnings("unchecked")
		List<Employeedata> empDataList = serviceAction.findWithNamedQuery("EmployeeID",
				QueryParameter.with("key", empCode).parameters());
		Map<String, String> eml = new HashMap<>();
		Employeedata employeedata = null;
		if (empDataList != null)
			employeedata = empDataList.get(0);
		EmpDatainfo emdinf = null;
		eml.put("key", Integer.toString(employeedata.getEmpDataID()));
		eml.put("service", AppMetaEnumeration.Services.LeaveApplication.toString());
		eml.put("status", RequestMetaData.Actions.Completed.getActions().toString());
		List<Requestmasterdata> requestMetaData = packetDataUtils
				.requestMasterData("RequestMasterData", eml);
		Map<String, String> requestData = null;
		if (requestMetaData.size() != 0 || requestMetaData != null)
		{
			for (Requestmasterdata requestmasterdata : requestMetaData)
			{
				requestData = packetDataUtils.requestData(requestmasterdata);
				if (requestData.get("Service")
						.equalsIgnoreCase(RequestMetaData.LeaveType.casual.toString()))
					cl = cl + Integer.parseInt(requestData.get("Days"));
				else if (requestData.get("Service")
						.equalsIgnoreCase(RequestMetaData.LeaveType.paid.toString()))
					el = el + Integer.parseInt(requestData.get("Days"));
				else if (requestData.get("Service")
						.equalsIgnoreCase(RequestMetaData.LeaveType.sick.toString()))
					sl = sl + Integer.parseInt(requestData.get("Days"));
			}
		}
		eml.clear();
		eml.putAll(requestData);
		eml.put("fname", employeedata.getDisplayName());
		eml.put("mNumber", employeedata.getMobile());
		eml.put("emailId", employeedata.getMail());
		eml.put("empId", employeedata.getEmployeeID());
		eml.put("empDataId", Integer.toString(employeedata.getEmpDataID()));
		emdinf = getEmployeeDateByID(employeedata.getUserPrincipalName());
		float daySalary = 0;
		if (emdinf != null)
		{
			eml.put("el", Integer.toString(el));
			eml.put("sl", Integer.toString(sl));
			eml.put("cl", Integer.toString(cl));
			eml.put("perAdd", emdinf.getEmpcontinfo().getPerAddress());
			eml.put("curAdd", emdinf.getEmpcontinfo().getCurrAddress());
			eml.put("city", emdinf.getEmpcontinfo().getCity());
			eml.put("state", emdinf.getEmpcontinfo().getState());
			eml.put("country", emdinf.getEmpcontinfo().getCountry());
			eml.put("pincode", emdinf.getEmpcontinfo().getPincode());
		}
		if (!emdinf.getSalaryDetail().getSalary_Details().isEmpty() && emdinf != null)
			daySalary = Integer.parseInt(emdinf.getSalaryDetail().getSalary_Details()) / 30;
		eml.put("daySalary", Integer.toString((int) daySalary));
		eml.put("monthSalary", emdinf.getSalaryDetail().getSalary_Details());
		eml.put("pf", Integer.toString(
				(int) (Integer.parseInt(emdinf.getSalaryDetail().getSalary_Details()) * 0.125)));
		eml.put("epf", "0.00");
		eml.put("eps", "0.00");
		eml.put("esi", "0.00");
		eml.put("esic", "0.00");
		eml.put("glwf", "0.00");
		int grossSalary, mgrossSalary, bmonthSalary, hra, convenyance, performancePay,
				sepcialAllowance;
		grossSalary = (int) (Integer.parseInt(emdinf.getSalaryDetail().getGrossSalary()));
		mgrossSalary = (int) (grossSalary * .08333);
		eml.put("gMonthlySalary", Integer.toString(mgrossSalary));
		bmonthSalary = (int) (grossSalary * .03333);
		hra = (int) (bmonthSalary * .50);
		convenyance = 1600;
		eml.put("monthBasicSalary", Integer.toString(bmonthSalary));
		eml.put("conveyance", Integer.toString(convenyance));
		performancePay = (int) (bmonthSalary * .10);
		sepcialAllowance = mgrossSalary - hra - convenyance - performancePay;
		eml.put("performancePay", Integer.toString(sepcialAllowance));
		eml.put("specialAllowance", Integer.toString(sepcialAllowance));
		eml.put("netPay", Integer.toString(mgrossSalary));
		return eml;
	}

	@Override
	public EmpDatainfo getEmployeeDateByID(String upn)
	{
		Map<String, String> pairValue = new HashMap<>();
		pairValue.put("key", upn);
		EmpDatainfo empDataInfo = packetDataUtils.empInformationDetails("EmpAllDetails", pairValue);
		return empDataInfo;
	}

	@Override
	public List<String> getEmpCode()
	{
		@SuppressWarnings("unchecked")
		List<Employeedata> employeedatas = serviceAction.findWithNamedQuery("Employeedata.findAll");
		List<String> emc = new ArrayList<>();
		for (Employeedata employeedata : employeedatas)
		{
			emc.add(employeedata.getEmployeeID());
		}
		return emc;
	}

	@Override
	public String getResonseData(String requestId, String service)
	{
		Map<String, String> pairValue = new HashMap<>();
		JsonObject responseJson = null;
		if (service.equalsIgnoreCase(AppMetaEnumeration.Services.PaySlip.toString()))
		{
			pairValue.put("key", requestId);
			Payslip payslip = packetDataUtils.GeneratedPaySlipData("GeneratePaySlip", pairValue);
			Map<String, String> respData = utilService.generatePaySlipMap(payslip);
			responseJson = new Gson().fromJson(
					respData.size() > 0 ? new Gson().toJson(respData) : null, JsonObject.class);
			responseJson.addProperty("Msg", respData.size() > 0 ? "Generated Successfully" : null);
			responseJson.addProperty("service", respData.size() > 0 ? service : null);
		}
		return responseJson.toString();
	}

	@Override
	public List<Map<String, Object>> generatedPaySlip(int empDataId)
	{
		Map<String, Integer> pairValue = new HashMap<>();
		List<Payslip> payslip = packetDataUtils.getGeneratedPaySlip("PaySlipByUser", pairValue);
		List<Map<String, Object>> genPaylist = new ArrayList<>();
		for (Payslip payMap : payslip)
		{
			genPaylist.add(utilService.mapGeneratedPaySlip(payMap));
		}
		return genPaylist;
	}

}
