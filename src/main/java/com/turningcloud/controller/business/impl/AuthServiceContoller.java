/**
 * 
 */
package com.turningcloud.controller.business.impl;

import com.turningcloud.controller.business.contract.EauthService;
import com.turningcloud.controller.utils.CrudService;
import com.turningcloud.controller.utils.PacketDataUtils;
import com.turningcloud.model.entity.ActiveUser;
import com.turningcloud.model.entity.Employeedata;
import com.turningcloud.view.beans.views.Usersession;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author manoj
 *
 */
@Transactional
@Stateless
public class AuthServiceContoller implements EauthService
{

	/**
	 * 
	 */
	@Inject
	private PacketDataUtils	packetDataUtils;

	@Inject
	private CrudService		crudService;

	private Logger			log	= LoggerFactory.getLogger(getClass());

	@Inject
	private Usersession		usersession;

	public AuthServiceContoller()
	{
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean isAuthenticated(String accessUser)
	{
		log.debug("mail id for authentication : {}", accessUser);

		if (accessUser != null)
		{
			Employeedata employeeData = this.getEmployeeDataByUPN(accessUser);

			if (employeeData != null && employeeData.getLocation() != null
					&& employeeData.getDisplayName() != null
					&& employeeData.getUserPrincipalName() != null)
			{
				log.debug("Employee {} with upn {} has been authenticated for TCS-Intra-Portal",
						employeeData.getDisplayName(), employeeData.getUserPrincipalName());
				//				if (this.isAllowedApplicationAccess())
				//				{
				//					log.debug(
				//							"Employee {} with upn {} is restricted to use this tool by the administrator",
				//							employeeData.getDisplayName(), employeeData.getUserPrincipalName());
				//					
				//				}
				return true;
			}
		}
		log.debug("Authentication to the portal failed due not valid accessId: {} ", accessUser);
		return false;
	}

	public Employeedata getEmployeeDataByUPN(String authupn)
	{
		Map<String, String> entrySet = new HashMap<>();
		entrySet.put("key", authupn);
		return packetDataUtils.getEmpData("Employeedata.findByUpn", entrySet);
	}

	public boolean isAllowedApplicationAccess()
	{
		return packetDataUtils.getAuthUser();
	}

	@Override
	public void userSetting(String upn, String actiontype)
	{
		ActiveUser generateActiveUser = new ActiveUser();
		Employeedata activUser = getEmployeeDataByUPN(upn);
		Map<String, String> entrySet = new HashMap<>();
		entrySet.put("key", upn);
		DateFormat df = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		Date today = Calendar.getInstance().getTime();
		String loggedDate = df.format(today);
		ActiveUser currentUser = packetDataUtils.getActiveUser("ActiveUser.findAllByUpn", entrySet);
		if (currentUser == null && actiontype.equalsIgnoreCase("active"))
		{
			generateActiveUser.setCurrentstatus(usersession.getUserStatus());
			generateActiveUser.setEmployeedata(activUser);
			generateActiveUser.setUsername(upn);
			generateActiveUser.setLoggedIntime(loggedDate);
			try
			{
				generateActiveUser.setIpaddress(InetAddress.getLocalHost().toString());

			}
			catch (UnknownHostException e)
			{
				e.printStackTrace();
			}
			crudService.create(generateActiveUser);
		}
		else if(actiontype.equalsIgnoreCase("active") && currentUser !=null)
		{
			currentUser.setLoggedIntime(loggedDate);
			try
			{
				currentUser.setIpaddress(InetAddress.getLocalHost().toString());
			}
			catch (UnknownHostException e)
			{
				e.printStackTrace();
			}
			crudService.update(currentUser);
		}else if (actiontype.equalsIgnoreCase("inactive")){
			currentUser.setLactive(today);
			currentUser.setCurrentstatus("Offline");
			currentUser.setStatus("Logout");
			crudService.update(currentUser);
		}

	}

}
