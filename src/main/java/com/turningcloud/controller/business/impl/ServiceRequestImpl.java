/**
 * 
 */
package com.turningcloud.controller.business.impl;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.constraints.Null;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.turningcloud.controller.business.contract.ActivityListner;
import com.turningcloud.controller.business.contract.serviceRequestUtil;
import com.turningcloud.controller.service.AppMetaEnumeration;
import com.turningcloud.controller.service.RequestMetaData;
import com.turningcloud.controller.utils.CrudService;
import com.turningcloud.controller.utils.PacketDataUtils;
import com.turningcloud.controller.utils.QueryParameter;
import com.turningcloud.model.entity.EmpDatainfo;
import com.turningcloud.model.entity.Employeedata;
import com.turningcloud.model.entity.LeaveBtable;
import com.turningcloud.model.entity.Payslip;
import com.turningcloud.model.entity.UserAction;
import com.turningcloud.view.beans.views.Usersession;

import metaData.AuthAppMetaData;

/**
 * @author manoj
 *
 */
@Stateless
@Transactional
public class ServiceRequestImpl implements serviceRequestUtil
{

	/**
	 * 
	 */
	@Inject
	private CrudService		serviceQuery;

	@Inject
	private ActivityListner	activityListner;

	@Inject
	private PacketDataUtils	packetDataUtils;

	@Inject
	private Usersession		userdata;

	@SuppressWarnings("unused")
	private DateFormat		df			= new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
	private Date			reqDt_time	= Calendar.getInstance().getTime();

	public ServiceRequestImpl()
	{
	}

	@SuppressWarnings("unused")
	@Override
	public List<Map<String, Object>> requestdata(String serviceRequest, String namedQuery,
			String responseKey)
	{
		List<Map<String, Object>> reqDataList = new ArrayList<>();
		if (serviceRequest.equalsIgnoreCase(AppMetaEnumeration.Services.LeaveApplication.toString())
				&& responseKey == null)
		{
			@SuppressWarnings("unchecked")
			List<UserAction> userRequests = serviceQuery.findWithNamedQuery("UserActionByOwner",
					QueryParameter.with("serviceRequest", serviceRequest)
							.and("activeUser", userdata.getEmployeedata().getEmpDataID())
							.parameters());

			if (userRequests != null)
			{
				for (UserAction userAction : userRequests)
				{
					reqDataList.add(cmtomap(userAction, serviceRequest));
				}
			}
			else
				reqDataList.add(null);

		}
		else if (serviceRequest
				.equalsIgnoreCase(AppMetaEnumeration.Services.LeaveApplication.toString())
				&& !responseKey.isEmpty()
				&& responseKey
						.equalsIgnoreCase(AppMetaEnumeration.Roles.ReportingManager.toString())
				|| serviceRequest
						.equalsIgnoreCase(AppMetaEnumeration.Services.LeaveApplication.toString())
						&& !responseKey.isEmpty() && responseKey
								.equalsIgnoreCase(AppMetaEnumeration.Roles.HRManager.toString()))
		{
			Map<String, String> pairValue = new HashMap<>();
			pairValue.put("key",
					userdata.getEmployeedata().getUserPrincipalName().toLowerCase().toString());
			@SuppressWarnings("unchecked")
			List<UserAction> userReqResponse = packetDataUtils.getUserActionData(namedQuery,
					pairValue);
			if (userReqResponse != null)
			{
				for (UserAction userAction : userReqResponse)
				{
					reqDataList.add(cmtomap(userAction, serviceRequest));
				}
			}
			else
				reqDataList.add(null);

		}
		return reqDataList;
	}

	public Map<String, Object> cmtomap(UserAction requestData, String serviceRequest)
	{
		Map<String, String> attrPair = activityListner.accessServiceAttributes(serviceRequest);
		Set<String> keys = attrPair.keySet();
		Map<String, Object> responseDataMap = new HashMap<>();
		String jsonString = requestData.getRequestmasterdata().getRequestData().toString();
		JsonParser jsonParser = new JsonParser();
		JsonElement element = jsonParser.parse(jsonString);
		responseDataMap.put("RequestID", requestData.getRequestmasterdata().getReqId());
		responseDataMap.put("ActionID", requestData.getActionId());
		responseDataMap.put("ActionStatus", requestData.getStatus());
		responseDataMap.put("NextOwnerRole", requestData.getNextOwnerRole());
		responseDataMap.put("NextOwnerUpn", requestData.getNextOwnerUpn());
		responseDataMap.put("Action", requestData.getAction());
		responseDataMap.put("LactionOn", requestData.getLactionOn());
		responseDataMap.put("ActionOn", requestData.getActionOn());
		responseDataMap.put("AIpAddress", requestData.getIpAddress());
		responseDataMap.put("RequestBy", requestData.getRequestmasterdata().getRequestedBy());
		responseDataMap.put("RequestOwner",
				serviceQuery
						.find(Employeedata.class,
								requestData.getRequestmasterdata().getRequestedBy())
						.getUserPrincipalName());
		responseDataMap.put("ReqDateTime",
				requestData.getRequestmasterdata().getRequestDate_Time());
		responseDataMap.put("ServiceRequest",
				requestData.getRequestmasterdata().getServiceRequest());
		responseDataMap.put("RequestStatus", requestData.getRequestmasterdata().getStatus());
		responseDataMap.put("UpdatedOn", requestData.getRequestmasterdata().getUpdatedOn());
		responseDataMap.put("LastUpdateOn", requestData.getRequestmasterdata().getLastUpdateOn());
		responseDataMap.put("RequestAction", requestData.getRequestmasterdata().getAction());
		responseDataMap.put("IsComplete", requestData.getRequestmasterdata().getIsComplete());
		responseDataMap.put("Active", requestData.getRequestmasterdata().getActive());
		responseDataMap.put("RequestIpAddess", requestData.getRequestmasterdata().getIpAddress());
		responseDataMap.put("ServiceType", requestData.getRequestmasterdata().getServiceType());
		//responseDataMap.put("", requestData.getRequestmasterdata());
		for (@SuppressWarnings("rawtypes")
		Iterator iterator = keys.iterator(); iterator.hasNext();)
		{
			String key = iterator.next().toString();
			String value = element.getAsJsonObject().get(key).toString().replace('"', ' ')
					.replace('[', ' ').replace(']', ' ').trim();
			String displayKey = attrPair.get(key);
			responseDataMap.put(displayKey, value);
		}
		/*	queryList.add(responseDataMap);
			System.out.println(queryList);*/

		return responseDataMap;

	}

	public Map<String, Object> leaveDetails()
	{
		Map<String, String> pairValue = new HashMap<>();
		pairValue.put("key", Integer.toString(userdata.getEmployeedata().getEmpDataID()));
		LeaveBtable leaveSummary = packetDataUtils.leaveDetails("BalanceLeave", pairValue);
		Map<String, Object> leaveSummaryDetails = new HashMap<>();
		if (leaveSummary != null)
		{
			leaveSummaryDetails.put("casual", leaveSummary.getCasualLeave());
			leaveSummaryDetails.put("paid", leaveSummary.getEarnedLeave());
			leaveSummaryDetails.put("sick", leaveSummary.getSickLeave());
			leaveSummaryDetails.put("total", leaveSummary.getTotalLeave());
		}
		return leaveSummaryDetails;
	}

	@Override
	public Map<String, Object> employeeDetails(EmpDatainfo empDatainfo)
	{
		Map<String, Object> emMapDetails = new HashMap<>();
		if (empDatainfo != null)
		{
			emMapDetails.put("EmployeeName", empDatainfo.getEmployeedata().getDisplayName());
			emMapDetails.put("EmployeeId", empDatainfo.getEmployeedata().getEmployeeID());
			emMapDetails.put("Initials", empDatainfo.getEmployeedata().getInitials());
			emMapDetails.put("Designation", empDatainfo.getEmployeedata().getDesignation());
			emMapDetails.put("Department", empDatainfo.getEmployeedata().getDepartment());
			emMapDetails.put("Manager", empDatainfo.getEmployeedata().getManager());
			emMapDetails.put("FileNo", empDatainfo.getFileNo());
			emMapDetails.put("HireDate", empDatainfo.getHireDate());
			emMapDetails.put("DiscDate", empDatainfo.getDiscontinueDate());
			emMapDetails.put("PPAvailbity", empDatainfo.getPpAvailablity());
			emMapDetails.put("PassportNo", empDatainfo.getPassportNo());
			emMapDetails.put("Remarks", empDatainfo.getRemarks());
			emMapDetails.put("FName", empDatainfo.getFatherName());
			emMapDetails.put("MName", empDatainfo.getMotherName());
			emMapDetails.put("SName", empDatainfo.getSpouseName());
			emMapDetails.put("Gender", empDatainfo.getGender());
			emMapDetails.put("BirthDate", empDatainfo.getBirthDate());
			emMapDetails.put("MaritalStatus", empDatainfo.getMaritalStatus());
			emMapDetails.put("Religion", empDatainfo.getReligion());
			emMapDetails.put("Telephone", empDatainfo.getEmpcontinfo().getTelephno());
			emMapDetails.put("Mobile", empDatainfo.getEmpcontinfo().getMobile());
			emMapDetails.put("City", empDatainfo.getEmpcontinfo().getCity());
			emMapDetails.put("State", empDatainfo.getEmpcontinfo().getState());
			emMapDetails.put("Country", empDatainfo.getEmpcontinfo().getCountry());
			emMapDetails.put("EAddress", empDatainfo.getEmpcontinfo().getEmailAddress());
			emMapDetails.put("CurrAddress", empDatainfo.getEmpcontinfo().getCurrAddress());
			emMapDetails.put("PerAddress", empDatainfo.getEmpcontinfo().getPerAddress());
			emMapDetails.put("SalaryDetails", empDatainfo.getSalaryDetail().getSalary_Details());
			emMapDetails.put("GrossSalary", empDatainfo.getSalaryDetail().getGrossSalary());
			emMapDetails.put("PatCurrency", empDatainfo.getSalaryDetail().getPatCurrency());
			emMapDetails.put("EmpBankAccount", empDatainfo.getSalaryDetail().getEmpBankAccount());
			emMapDetails.put("EmpAccountNo", empDatainfo.getSalaryDetail().getEmpAccountNo());
		}
		return emMapDetails;
	}

	@Override
	public String paySlipsParam(Map<String, String[]> requestParam, String actionType)
	{
		String status = null, key, monthly, conveyance, perkPay, specialAllowance;
		int payHr, absDeduction, grossMonthly;
		Payslip generatePaySlipData = new Payslip();
		System.err.println(new Gson().toJson(requestParam));
		if (requestParam.containsKey("edi"))
			key = requestParam.get("edi")[0];
		else
			key = requestParam.get("sedi")[0];
		System.out.println(requestParam.get("sedi")[0]);
		Employeedata employeedata = serviceQuery.find(Employeedata.class, Integer.parseInt(key));
		Map<String, String> pairValue = new HashMap<>();
		pairValue.put("key", employeedata.getUserPrincipalName().toString());
		EmpDatainfo empDatainfo = packetDataUtils.empInformationDetails("EmpAllDetails", pairValue);
		int Salary = Integer.parseInt(empDatainfo.getSalaryDetail().getGrossSalary());
		if (actionType.equalsIgnoreCase(RequestMetaData.ActionType.savePaySlipData.toString()))
			generatePaySlipData.setStatus(RequestMetaData.Status.Pending.toString());
		else if (actionType.equalsIgnoreCase(RequestMetaData.ActionType.generateSlip.toString()))
		{
			generatePaySlipData.setYear(requestParam.get("years")[0]);
			generatePaySlipData.setMonth(requestParam.get("month")[0]);
			generatePaySlipData.setCl(requestParam.get("casualLeave")[0]);
			generatePaySlipData.setEl(requestParam.get("earnedleave")[0]);
			generatePaySlipData.setSl(requestParam.get("sickleave")[0]);
			generatePaySlipData.setWday(requestParam.get("workingDays")[0]);
			generatePaySlipData.setHday(requestParam.get("holidays")[0]);
			generatePaySlipData.setAbscent(requestParam.get("absent")[0]);
			generatePaySlipData.setPhour(requestParam.get("paidHours")[0]);
			generatePaySlipData.setMday(requestParam.get("monthDays")[0]);
			generatePaySlipData.setPf(requestParam.get("pf")[0]);
			generatePaySlipData.setEsi(requestParam.get("esi")[0]);
			generatePaySlipData.setDaily(requestParam.get("daily")[0]);
			monthly = requestParam.get("monthly")[0];
			generatePaySlipData.setBasicMonth(monthly);
			int hra = (int) (Integer.parseInt(monthly) * .50);
			generatePaySlipData.setPfdeduction(requestParam.get("pfd")[0]);
			generatePaySlipData.setEpsdeduction(requestParam.get("epsd")[0]);
			generatePaySlipData.setEsideduction(requestParam.get("esid")[0]);
			generatePaySlipData.setEsicdeduction(requestParam.get("esicd")[0]);
			//			generatePaySlipData.setReimbursement(requestParam.get("allowanceAmt1")[0]);
			//			generatePaySlipData.setDeductionAllowance(requestParam.get("Dallowance0")[0]);
			generatePaySlipData.setGlwf(requestParam.get("glwf")[0]);
			generatePaySlipData.setNetpay(requestParam.get("netSalary")[0]);
			generatePaySlipData.setGeneratedAt(reqDt_time);
			try
			{
				generatePaySlipData.setGeneatedBy(InetAddress.getLocalHost().toString());
			}
			catch (UnknownHostException e)
			{
				e.printStackTrace();
			}
			conveyance = "1600";
			grossMonthly = (int) (Integer.parseInt(empDatainfo.getSalaryDetail().getGrossSalary())
					* .083333);
			generatePaySlipData.setStatus(RequestMetaData.Status.Generated.toString());
			generatePaySlipData.setHra(Integer.toString(hra));
			generatePaySlipData.setConveyance(conveyance);
			perkPay = Integer.toString((int) (Integer.parseInt(monthly) * .10));
			specialAllowance = Integer.toString(grossMonthly - (int) (Integer.parseInt(conveyance))
					- hra - (int) (Integer.parseInt(monthly) * .10)
					- (int) (Integer.parseInt(monthly)));
			generatePaySlipData.setSpecialAllowance(specialAllowance);
			generatePaySlipData.setPerformancePay(perkPay);
			generatePaySlipData.setDeputation("0.00");
			generatePaySlipData.setIncomeTax("0.00");
			payHr = (int) Integer.parseInt(requestParam.get("paidHours")[0])
					* (int) (grossMonthly * 0.0037037);
			absDeduction = (int) Integer.parseInt(requestParam.get("absent")[0])
					* (int) (grossMonthly * 0.03333);
			generatePaySlipData.setPayHourAmount(Integer.toString(payHr));
			generatePaySlipData.setAbscentAmt(Integer.toString(absDeduction));
		}
		generatePaySlipData.setEmployeedata(employeedata);
		generatePaySlipData.setEmpDatainfo(empDatainfo);
		status = serviceQuery.create(generatePaySlipData).getStatus();

		return status;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, String> generatePaySlipMap(Payslip payslip)
	{
		@SuppressWarnings("rawtypes")
		Map paySlipMap = new HashMap<>();
		paySlipMap.put("name", payslip.getEmployeedata().getDisplayName());
		paySlipMap.put("empCode", payslip.getEmployeedata().getEmployeeID());
		paySlipMap.put("joinDate", payslip.getEmpDatainfo().getHireDate());
		paySlipMap.put("designation", payslip.getEmployeedata().getDesignation());
		paySlipMap.put("bankAccount", payslip.getEmpDatainfo().getSalaryDetail().getEmpAccountNo());
		paySlipMap.put("birthDate", payslip.getEmpDatainfo().getBirthDate());
		paySlipMap.put("department", payslip.getEmployeedata().getDepartment());
		paySlipMap.put("location", payslip.getEmployeedata().getLocation());
		paySlipMap.put("costCenter", "");
		paySlipMap.put("Gender", payslip.getEmpDatainfo().getGender());
		paySlipMap.put("salaryMonth", payslip.getMonth());
		paySlipMap.put("salaryYear", payslip.getYear());
		paySlipMap.put("basicSalary", payslip.getBasicMonth());
		paySlipMap.put("hra", payslip.getHra());
		paySlipMap.put("conveyance", payslip.getConveyance());
		paySlipMap.put("specialAllowance", payslip.getSpecialAllowance());
		paySlipMap.put("perPay", payslip.getPerformancePay());
		paySlipMap.put("deputation", payslip.getDeputation());
		paySlipMap.put("netPay", payslip.getNetpay());
		return paySlipMap;
	}

	public Map<String, Object> mapGeneratedPaySlip(Payslip payslip)
	{

		Map<String, Object> paySlipMap = new HashMap<>();
		if (payslip != null)
		{
			paySlipMap.put("psid", payslip.getPsid());
			paySlipMap.put("Month", payslip.getMonth());
			paySlipMap.put("Year", payslip.getYear());
			paySlipMap.put("GenDate", payslip.getGeneratedAt());
			paySlipMap.put("Status", payslip.getStatus());
		}
		return paySlipMap;
	}
}
