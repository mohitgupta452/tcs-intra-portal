/**
 * 
 */
package com.turningcloud.controller.business.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;

import com.turningcloud.controller.service.AppMetaEnumeration;
import com.turningcloud.controller.service.AppMetaEnumeration.ProcessType;
import com.turningcloud.controller.utils.ProcessHelper;

/**
 * @author manoj
 *
 */
@RequestScoped
public class ActivityLIstner extends ProcessHelper
{

	/**
	 * 
	 */

	public ActivityLIstner()
	{
	}

	@Override
	public ProcessType processType(String activeServiceRequest)
	{
		ProcessType processType = null;
		setService(activeServiceRequest);
		String prop = processProp.getProperty("Approvalprocess").toString();
		List<String> process;
		if (prop.contains("."))
		{
			process = Arrays.asList(prop.split("\\."));
			if (process.contains(activeServiceRequest))
				processType = ProcessType.ApprovalProcess;
			return processType;
		}

		return processType;
	}

	@Override
	public String requestNextOwner()
	{
		List<String> serviceLists = getOwnerList();
		return serviceLists.get(0);
	}

	@Override
	public Map<String, String> accessServiceAttributes(String serviceRequest)
	{
		@SuppressWarnings("unused")
		List<String> attKey;
		Map<String, String> attributeKeyPair = new HashMap<>();
		if (serviceRequest
				.equalsIgnoreCase(AppMetaEnumeration.Services.LeaveApplication.toString()))
		{
			@SuppressWarnings("unused")
			List<String> serviceAttributesKey = Arrays
					.asList(serviceAttr.getProperty(serviceRequest).split("\\."));
			for (String string : serviceAttributesKey)
			{
				attKey = Arrays.asList(
						serviceAttr.getProperty(serviceRequest + "." + string).split("\\."));
				attributeKeyPair.put(attKey.get(0), attKey.get(1));
			}
		}
		return attributeKeyPair;
	}

}
