package com.turningcloud.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the user_action database table.
 * 
 */
@Entity
@Table(name = "user_action")
@NamedQueries(
{ @NamedQuery(name = "UserAction.findAll", query = "SELECT u FROM UserAction u"),
		@NamedQuery(name = "UserAction.findAllByService",
				query = "SELECT u FROm UserAction u Where u.requestmasterdata.serviceRequest=:serviceRequest"),
		@NamedQuery(name = "UserActionByRM",
				query = "SELECT u FROm UserAction u Where u.nextOwnerUpn=:key AND u.action='PendingByRM' Or u.action='RejectedByRM' OR u.action='ApprovedByRM'"),
		@NamedQuery(name = "UserActionByHR",
		query = "SELECT u FROm UserAction u Where u.nextOwnerUpn=:key AND u.action='ApprovedByRM'"),
		@NamedQuery(name = "UserActionData",
				query = "SELECT u FROM UserAction u Where u.requestmasterdata.reqId=:key"),
		@NamedQuery(name="UserActionByOwner", query = "SELECT u FROM UserAction u WHERE u.requestmasterdata.serviceRequest=:serviceRequest AND u.requestmasterdata.requestedBy=:activeUser")})
public class UserAction implements Serializable
{
	private static final long	serialVersionUID	= 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int					actionId;

	private String				action;

	@Temporal(TemporalType.TIMESTAMP)
	private Date				actionOn;

	private String				ipAddress;

	@Temporal(TemporalType.TIMESTAMP)
	private Date				lactionOn;

	private String				nextOwnerRole;

	private String				nextOwnerUpn;

	@ManyToOne
	@JoinColumn(name = "requestId")
	private Requestmasterdata	requestmasterdata;

	private String				status;

	public UserAction()
	{
	}

	public int getActionId()
	{
		return this.actionId;
	}

	public void setActionId(int actionId)
	{
		this.actionId = actionId;
	}

	public String getAction()
	{
		return this.action;
	}

	public void setAction(String action)
	{
		this.action = action;
	}

	public Date getActionOn()
	{
		return this.actionOn;
	}

	public void setActionOn(Date actionOn)
	{
		this.actionOn = actionOn;
	}

	public String getIpAddress()
	{
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress)
	{
		this.ipAddress = ipAddress;
	}

	public Date getLactionOn()
	{
		return this.lactionOn;
	}

	public void setLactionOn(Date lactionOn)
	{
		this.lactionOn = lactionOn;
	}

	public String getNextOwnerRole()
	{
		return this.nextOwnerRole;
	}

	public void setNextOwnerRole(String nextOwnerRole)
	{
		this.nextOwnerRole = nextOwnerRole;
	}

	public String getNextOwnerUpn()
	{
		return this.nextOwnerUpn;
	}

	public void setNextOwnerUpn(String nextOwnerUpn)
	{
		this.nextOwnerUpn = nextOwnerUpn;
	}

	public Requestmasterdata getRequestmasterdata()
	{
		return requestmasterdata;
	}

	public void setRequestmasterdata(Requestmasterdata requestmasterdata)
	{
		this.requestmasterdata = requestmasterdata;
	}

	public String getStatus()
	{
		return this.status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

}