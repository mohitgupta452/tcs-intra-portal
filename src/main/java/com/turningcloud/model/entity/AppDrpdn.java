package com.turningcloud.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the app_drpdn database table.
 * 
 */
@Entity
@Table(name="app_drpdn")
@NamedQuery(name="AppDrpdn.findAll", query="SELECT a FROM AppDrpdn a")
public class AppDrpdn implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int apdnid;

	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	private String creationIp;

	@Lob
	private String data;

	@Lob
	private String description;

	private String fieldName;

	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedDate;

	private String modifiedIp;

	public AppDrpdn() {
	}

	public int getApdnid() {
		return this.apdnid;
	}

	public void setApdnid(int apdnid) {
		this.apdnid = apdnid;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreationIp() {
		return this.creationIp;
	}

	public void setCreationIp(String creationIp) {
		this.creationIp = creationIp;
	}

	public String getData() {
		return this.data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFieldName() {
		return this.fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedIp() {
		return this.modifiedIp;
	}

	public void setModifiedIp(String modifiedIp) {
		this.modifiedIp = modifiedIp;
	}

}