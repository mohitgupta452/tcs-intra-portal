package com.turningcloud.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the response_masterdata database table.
 * 
 */
@Entity
@Table(name="response_masterdata")
@NamedQuery(name="ResponseMasterdata.findAll", query="SELECT r FROM ResponseMasterdata r")
public class ResponseMasterdata implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int respId;

	private String activity;

	private String ipAddress;

	@Temporal(TemporalType.TIMESTAMP)
	private Date lupdatedOn;

	private String responseBy;

	@Temporal(TemporalType.TIMESTAMP)
	private Date responseDate;

	private String responseMsg;

	private String responseStatus;

	//bi-directional many-to-one association to UserAction
	@ManyToOne
	@JoinColumn(name="actionId")
	private UserAction userAction;

	public ResponseMasterdata() {
	}

	public int getRespId() {
		return this.respId;
	}

	public void setRespId(int respId) {
		this.respId = respId;
	}

	public String getActivity() {
		return this.activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Date getLupdatedOn() {
		return this.lupdatedOn;
	}

	public void setLupdatedOn(Date lupdatedOn) {
		this.lupdatedOn = lupdatedOn;
	}

	public String getResponseBy() {
		return this.responseBy;
	}

	public void setResponseBy(String responseBy) {
		this.responseBy = responseBy;
	}

	public Date getResponseDate() {
		return this.responseDate;
	}

	public void setResponseDate(Date responseDate) {
		this.responseDate = responseDate;
	}

	public String getResponseMsg() {
		return this.responseMsg;
	}

	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}

	public String getResponseStatus() {
		return this.responseStatus;
	}

	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}

	public UserAction getUserAction() {
		return this.userAction;
	}

	public void setUserAction(UserAction userAction) {
		this.userAction = userAction;
	}

}