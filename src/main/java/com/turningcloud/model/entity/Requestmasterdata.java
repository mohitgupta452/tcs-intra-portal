package com.turningcloud.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the requestmasterdata database table.
 * 
 */
@Entity
@Table(name = "requestmasterdata")
@NamedQueries(
{ @NamedQuery(name = "Requestmasterdata.findAll", query = "SELECT r FROM Requestmasterdata r"),
		@NamedQuery(name = "RequestMasterData",
				query = "SELECT r FROM Requestmasterdata r WHERE  r.requestDate_Time>=:key AND r.requestedBy=:keys AND r.serviceRequest=:service AND r.status=:status"), })

public class Requestmasterdata implements Serializable
{
	private static final long	serialVersionUID	= 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int					reqId;

	private String				action;

	private Byte				active;

	private String				ipAddress;

	private Byte				isComplete;

	@Temporal(TemporalType.TIMESTAMP)
	private Date				lastUpdateOn;

	private String				requestData;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "`requestDate&Time`")
	private Date				requestDate_Time;

	private int					requestedBy;

	private String				serviceRequest;

	private String				serviceType;

	private String				status;

	@Temporal(TemporalType.TIMESTAMP)
	private Date				updatedOn;

	public Requestmasterdata()
	{
	}

	public int getReqId()
	{
		return this.reqId;
	}

	public void setReqId(int reqId)
	{
		this.reqId = reqId;
	}

	public String getAction()
	{
		return this.action;
	}

	public void setAction(String action)
	{
		this.action = action;
	}

	public Byte getActive()
	{
		return this.active;
	}

	public void setActive(Byte active)
	{
		this.active = active;
	}

	public String getIpAddress()
	{
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress)
	{
		this.ipAddress = ipAddress;
	}

	public Object getIsComplete()
	{
		return this.isComplete;
	}

	public void setIsComplete(Byte isComplete)
	{
		this.isComplete = isComplete;
	}

	public Date getLastUpdateOn()
	{
		return this.lastUpdateOn;
	}

	public void setLastUpdateOn(Date lastUpdateOn)
	{
		this.lastUpdateOn = lastUpdateOn;
	}

	public String getRequestData()
	{
		return this.requestData;
	}

	public void setRequestData(String requestData)
	{
		this.requestData = requestData;
	}

	public Date getRequestDate_Time()
	{
		return this.requestDate_Time;
	}

	public void setRequestDate_Time(Date requestDate_Time)
	{
		this.requestDate_Time = requestDate_Time;
	}

	public int getRequestedBy()
	{
		return this.requestedBy;
	}

	public void setRequestedBy(int requestedBy)
	{
		this.requestedBy = requestedBy;
	}

	public String getServiceRequest()
	{
		return this.serviceRequest;
	}

	public void setServiceRequest(String serviceRequest)
	{
		this.serviceRequest = serviceRequest;
	}

	public String getServiceType()
	{
		return this.serviceType;
	}

	public void setServiceType(String serviceType)
	{
		this.serviceType = serviceType;
	}

	public String getStatus()
	{
		return this.status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public Date getUpdatedOn()
	{
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn)
	{
		this.updatedOn = updatedOn;
	}

}