package com.turningcloud.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the empcontinfo database table.
 * 
 */
@Entity
@Table(name = "empcontinfo")
@NamedQuery(name = "Empcontinfo.findAll", query = "SELECT e FROM Empcontinfo e")
public class Empcontinfo implements Serializable
{
	private static final long	serialVersionUID	= 1L;

	@Id
	private int					ecoId;

	private String				action;

	private String				city;

	private String				country;

	private String				createdBy;

	private String				currAddress;

	private String				emailAddress;

	private String				mobile;

	private String				perAddress;

	private String				state;

	private String				telephno;

	private String				pincode;

	@Temporal(TemporalType.TIMESTAMP)
	private Date				updatedAt;

	//bi-directional many-to-one association to EmpDatainfo
	@OneToMany(mappedBy = "empcontinfo")
	private List<EmpDatainfo>	empDatainfos;

	//bi-directional many-to-one association to EmpDatainfo
	@ManyToOne
	@JoinColumn(name = "empDataInfoId")
	private EmpDatainfo			empDatainfo;

	//bi-directional many-to-one association to SalaryDetail
	@OneToMany(mappedBy = "empcontinfo")
	private List<SalaryDetail>	salaryDetails;

	public Empcontinfo()
	{
	}

	public int getEcoId()
	{
		return this.ecoId;
	}

	public void setEcoId(int ecoId)
	{
		this.ecoId = ecoId;
	}

	public String getAction()
	{
		return this.action;
	}

	public void setAction(String action)
	{
		this.action = action;
	}

	public String getCity()
	{
		return this.city;
	}

	public String getPincode()
	{
		return pincode;
	}

	public void setPincode(String pincode)
	{
		this.pincode = pincode;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public String getCountry()
	{
		return this.country;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public String getCreatedBy()
	{
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy)
	{
		this.createdBy = createdBy;
	}

	public String getCurrAddress()
	{
		return this.currAddress;
	}

	public void setCurrAddress(String currAddress)
	{
		this.currAddress = currAddress;
	}

	public String getEmailAddress()
	{
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress)
	{
		this.emailAddress = emailAddress;
	}

	public String getMobile()
	{
		return this.mobile;
	}

	public void setMobile(String mobile)
	{
		this.mobile = mobile;
	}

	public String getPerAddress()
	{
		return this.perAddress;
	}

	public void setPerAddress(String perAddress)
	{
		this.perAddress = perAddress;
	}

	public String getState()
	{
		return this.state;
	}

	public void setState(String state)
	{
		this.state = state;
	}

	public String getTelephno()
	{
		return this.telephno;
	}

	public void setTelephno(String telephno)
	{
		this.telephno = telephno;
	}

	public Date getUpdatedAt()
	{
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt)
	{
		this.updatedAt = updatedAt;
	}

	public List<EmpDatainfo> getEmpDatainfos()
	{
		return this.empDatainfos;
	}

	public void setEmpDatainfos(List<EmpDatainfo> empDatainfos)
	{
		this.empDatainfos = empDatainfos;
	}

	public EmpDatainfo addEmpDatainfo(EmpDatainfo empDatainfo)
	{
		getEmpDatainfos().add(empDatainfo);
		empDatainfo.setEmpcontinfo(this);

		return empDatainfo;
	}

	public EmpDatainfo removeEmpDatainfo(EmpDatainfo empDatainfo)
	{
		getEmpDatainfos().remove(empDatainfo);
		empDatainfo.setEmpcontinfo(null);

		return empDatainfo;
	}

	public EmpDatainfo getEmpDatainfo()
	{
		return this.empDatainfo;
	}

	public void setEmpDatainfo(EmpDatainfo empDatainfo)
	{
		this.empDatainfo = empDatainfo;
	}

	public List<SalaryDetail> getSalaryDetails()
	{
		return this.salaryDetails;
	}

	public void setSalaryDetails(List<SalaryDetail> salaryDetails)
	{
		this.salaryDetails = salaryDetails;
	}

	public SalaryDetail addSalaryDetail(SalaryDetail salaryDetail)
	{
		getSalaryDetails().add(salaryDetail);
		salaryDetail.setEmpcontinfo(this);

		return salaryDetail;
	}

	public SalaryDetail removeSalaryDetail(SalaryDetail salaryDetail)
	{
		getSalaryDetails().remove(salaryDetail);
		salaryDetail.setEmpcontinfo(null);

		return salaryDetail;
	}

}