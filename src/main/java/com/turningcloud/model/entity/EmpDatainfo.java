package com.turningcloud.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the emp_datainfo database table.
 * 
 */
@Entity
@Table(name = "emp_datainfo")
@NamedQueries(
{ @NamedQuery(name = "EmpDatainfo.findAll", query = "SELECT e FROM EmpDatainfo e"),
		@NamedQuery(name = "EmpAllDetails",
				query = "SELECT e FROM EmpDatainfo e WHERE e.employeedata.userPrincipalName=:key"), })
public class EmpDatainfo implements Serializable
{
	private static final long	serialVersionUID	= 1L;

	@Id
	private int					emp_infoId;

	private String				action;

	private String				birthDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date				createdAt;

	private String				createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	private Date				discontinueDate;

	@ManyToOne
	@JoinColumn(name = "emp_DataId")
	private Employeedata		employeedata;

	private String				empType;

	private String				fatherName;

	private String				fileNo;

	private String				gender;

	@Temporal(TemporalType.TIMESTAMP)
	private Date				hireDate;

	private String				maritalStatus;

	private String				motherName;

	private String				passportNo;

	@Column(name = "pp_availablity")
	private String				ppAvailablity;

	private String				religion;

	private String				remarks;

	private String				spouseName;

	@Temporal(TemporalType.TIMESTAMP)
	private Date				updatedAt;

	//bi-directional many-to-one association to Empcontinfo
	@ManyToOne
	@JoinColumn(name = "empConInfoId")
	private Empcontinfo			empcontinfo;

	//bi-directional many-to-one association to SalaryDetail
	@ManyToOne
	@JoinColumn(name = "empSalDetId")
	private SalaryDetail		salaryDetail;

	//bi-directional many-to-one association to Empcontinfo
	@OneToMany(mappedBy = "empDatainfo")
	private List<Empcontinfo>	empcontinfos;

	public EmpDatainfo()
	{
	}

	public int getEmp_infoId()
	{
		return this.emp_infoId;
	}

	public void setEmp_infoId(int emp_infoId)
	{
		this.emp_infoId = emp_infoId;
	}

	public String getAction()
	{
		return this.action;
	}

	public void setAction(String action)
	{
		this.action = action;
	}

	public String getBirthDate()
	{
		return this.birthDate;
	}

	public void setBirthDate(String birthDate)
	{
		this.birthDate = birthDate;
	}

	public Date getCreatedAt()
	{
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt)
	{
		this.createdAt = createdAt;
	}

	public String getCreatedBy()
	{
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy)
	{
		this.createdBy = createdBy;
	}

	public Date getDiscontinueDate()
	{
		return this.discontinueDate;
	}

	public void setDiscontinueDate(Date discontinueDate)
	{
		this.discontinueDate = discontinueDate;
	}

	public Employeedata getEmployeedata()
	{
		return employeedata;
	}

	public void setEmployeedata(Employeedata employeedata)
	{
		this.employeedata = employeedata;
	}

	public String getEmpType()
	{
		return this.empType;
	}

	public void setEmpType(String empType)
	{
		this.empType = empType;
	}

	public String getFatherName()
	{
		return this.fatherName;
	}

	public void setFatherName(String fatherName)
	{
		this.fatherName = fatherName;
	}

	public String getFileNo()
	{
		return this.fileNo;
	}

	public void setFileNo(String fileNo)
	{
		this.fileNo = fileNo;
	}

	public String getGender()
	{
		return this.gender;
	}

	public void setGender(String gender)
	{
		this.gender = gender;
	}

	public Date getHireDate()
	{
		return this.hireDate;
	}

	public void setHireDate(Date hireDate)
	{
		this.hireDate = hireDate;
	}

	public String getMaritalStatus()
	{
		return this.maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus)
	{
		this.maritalStatus = maritalStatus;
	}

	public String getMotherName()
	{
		return this.motherName;
	}

	public void setMotherName(String motherName)
	{
		this.motherName = motherName;
	}

	public String getPassportNo()
	{
		return this.passportNo;
	}

	public void setPassportNo(String passportNo)
	{
		this.passportNo = passportNo;
	}

	public String getPpAvailablity()
	{
		return this.ppAvailablity;
	}

	public void setPpAvailablity(String ppAvailablity)
	{
		this.ppAvailablity = ppAvailablity;
	}

	public String getReligion()
	{
		return this.religion;
	}

	public void setReligion(String religion)
	{
		this.religion = religion;
	}

	public String getRemarks()
	{
		return this.remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public String getSpouseName()
	{
		return this.spouseName;
	}

	public void setSpouseName(String spouseName)
	{
		this.spouseName = spouseName;
	}

	public Date getUpdatedAt()
	{
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt)
	{
		this.updatedAt = updatedAt;
	}

	public Empcontinfo getEmpcontinfo()
	{
		return this.empcontinfo;
	}

	public void setEmpcontinfo(Empcontinfo empcontinfo)
	{
		this.empcontinfo = empcontinfo;
	}

	public SalaryDetail getSalaryDetail()
	{
		return this.salaryDetail;
	}

	public void setSalaryDetail(SalaryDetail salaryDetail)
	{
		this.salaryDetail = salaryDetail;
	}

	public List<Empcontinfo> getEmpcontinfos()
	{
		return this.empcontinfos;
	}

	public void setEmpcontinfos(List<Empcontinfo> empcontinfos)
	{
		this.empcontinfos = empcontinfos;
	}

	public Empcontinfo addEmpcontinfo(Empcontinfo empcontinfo)
	{
		getEmpcontinfos().add(empcontinfo);
		empcontinfo.setEmpDatainfo(this);

		return empcontinfo;
	}

	public Empcontinfo removeEmpcontinfo(Empcontinfo empcontinfo)
	{
		getEmpcontinfos().remove(empcontinfo);
		empcontinfo.setEmpDatainfo(null);

		return empcontinfo;
	}

}