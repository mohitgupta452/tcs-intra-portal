package com.turningcloud.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the active_users database table.
 * 
 */
@Entity
@Table(name = "active_users")
@NamedQueries({
	@NamedQuery(name = "ActiveUser.findAll", query = "SELECT a FROM ActiveUser a"),
		@NamedQuery(name = "ActiveUser.findAllByUpn", query = "SELECT a FROM ActiveUser a where a.employeedata.empDataID=:key"),
})
public class ActiveUser implements Serializable
{
	private static final long	serialVersionUID	= 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int					auid;

	private String				currentstatus;

	private String				ipaddress;

	private byte[]				isactive;

	@Temporal(TemporalType.TIMESTAMP)
	private Date				lactive;

	private String				loggedIntime;

	private String				status;

	@ManyToOne
	@JoinColumn(name = "userId")
	private Employeedata		employeedata;

	private String				username;

	public ActiveUser()
	{
	}

	public int getAuid()
	{
		return this.auid;
	}

	public void setAuid(int auid)
	{
		this.auid = auid;
	}

	public String getCurrentstatus()
	{
		return this.currentstatus;
	}

	public void setCurrentstatus(String currentstatus)
	{
		this.currentstatus = currentstatus;
	}

	public String getIpaddress()
	{
		return this.ipaddress;
	}

	public void setIpaddress(String ipaddress)
	{
		this.ipaddress = ipaddress;
	}

	public byte[] getIsactive()
	{
		return this.isactive;
	}

	public void setIsactive(byte[] isactive)
	{
		this.isactive = isactive;
	}

	public Date getLactive()
	{
		return this.lactive;
	}

	public void setLactive(Date lactive)
	{
		this.lactive = lactive;
	}

	public String getLoggedIntime()
	{
		return this.loggedIntime;
	}

	public void setLoggedIntime(String loggedIntime)
	{
		this.loggedIntime = loggedIntime;
	}

	public String getStatus()
	{
		return this.status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public Employeedata getEmployeedata()
	{
		return employeedata;
	}

	public void setEmployeedata(Employeedata employeedata)
	{
		this.employeedata = employeedata;
	}

	public String getUsername()
	{
		return this.username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

}