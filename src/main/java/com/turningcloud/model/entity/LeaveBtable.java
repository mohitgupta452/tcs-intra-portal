package com.turningcloud.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the leave_btable database table.
 * 
 */
@Entity
@Table(name = "leave_btable")
@NamedQueries(
{ @NamedQuery(name = "LeaveBtable.findAll", query = "SELECT l FROM LeaveBtable l"),
		@NamedQuery(name = "BalanceLeave",
				query = "SELECT l FROM LeaveBtable l WHERE l.employeedata.empDataID=:key"), })
public class LeaveBtable implements Serializable
{
	private static final long	serialVersionUID	= 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int					leaveblnId;

	private int					casualLeave;

	private int					earnedLeave;

	@OneToOne
	@JoinColumn(name = "requestUserId")
	private Employeedata		employeedata;

	private int					sickLeave;

	private String				status;

	private int					totalLeave;

	@Temporal(TemporalType.TIMESTAMP)
	private Date				updatedAt;

	private String				updatedIp;

	public LeaveBtable()
	{
	}

	public int getLeaveblnId()
	{
		return this.leaveblnId;
	}

	public void setLeaveblnId(int leaveblnId)
	{
		this.leaveblnId = leaveblnId;
	}

	public int getCasualLeave()
	{
		return this.casualLeave;
	}

	public void setCasualLeave(int casualLeave)
	{
		this.casualLeave = casualLeave;
	}

	public int getEarnedLeave()
	{
		return this.earnedLeave;
	}

	public void setEarnedLeave(int earnedLeave)
	{
		this.earnedLeave = earnedLeave;
	}

	public Employeedata getEmployeedata()
	{
		return employeedata;
	}

	public void setEmployeedata(Employeedata employeedata)
	{
		this.employeedata = employeedata;
	}

	public int getSickLeave()
	{
		return this.sickLeave;
	}

	public void setSickLeave(int sickLeave)
	{
		this.sickLeave = sickLeave;
	}

	public String getStatus()
	{
		return this.status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public int getTotalLeave()
	{
		return this.totalLeave;
	}

	public void setTotalLeave(int totalLeave)
	{
		this.totalLeave = totalLeave;
	}

	public Date getUpdatedAt()
	{
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt)
	{
		this.updatedAt = updatedAt;
	}

	public String getUpdatedIp()
	{
		return this.updatedIp;
	}

	public void setUpdatedIp(String updatedIp)
	{
		this.updatedIp = updatedIp;
	}

}