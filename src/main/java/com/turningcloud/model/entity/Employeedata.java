
package com.turningcloud.model.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the employeedata database table.
 * 
 */
@Entity
@Table(name = "employeedata")
@NamedQueries(
{ @NamedQuery(name = "Employeedata.findAll", query = "SELECT e FROM Employeedata e"),
		@NamedQuery(name = "Employeedata.findByUpn",
				query = "SELECT e FROM Employeedata e where e.userPrincipalName=:key"),
		@NamedQuery(name = "Employeedata.findManager",
				query = "SELECT e FROM Employeedata e where e.manager=:key"),
		@NamedQuery(name = "EmpployeeDataByRole",
				query = "SELECT e FROM Employeedata e WHERE e.designation=:key"),
		@NamedQuery(name = "EmployeeDataID",
		query = "SELECT e.employeeID FROM Employeedata e"),
		@NamedQuery(name = "EmployeeID",
		query = "SELECT e FROM Employeedata e WHERE e.employeeID=:key"),})

public class Employeedata implements Serializable
{
	private static final long	serialVersionUID	= 1L;

	@Id
	private int					empDataID;

	private byte				active;

	private String				company;

	private String				department;

	private String				designation;

	private String				displayName;

	private String				distinguishedName;

	private String				employeeID;

	@Lob
	private byte[]				googleImage;

	private String				initials;

	private String				location;

	private String				mail;

	private String				manager;

	private String				mobile;

	private String				state;

	@Lob
	private byte[]				storeImage;

	private String				telephoneNumber;

	private String				userPrincipalName;

	public Employeedata()
	{
	}

	public int getEmpDataID()
	{
		return this.empDataID;
	}

	public void setEmpDataID(int empDataID)
	{
		this.empDataID = empDataID;
	}

	public byte getActive()
	{
		return this.active;
	}

	public void setActive(byte active)
	{
		this.active = active;
	}

	public String getCompany()
	{
		return this.company;
	}

	public void setCompany(String company)
	{
		this.company = company;
	}

	public String getDepartment()
	{
		return this.department;
	}

	public void setDepartment(String department)
	{
		this.department = department;
	}

	public String getDesignation()
	{
		return this.designation;
	}

	public void setDesignation(String designation)
	{
		this.designation = designation;
	}

	public String getDisplayName()
	{
		return this.displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}

	public String getDistinguishedName()
	{
		return this.distinguishedName;
	}

	public void setDistinguishedName(String distinguishedName)
	{
		this.distinguishedName = distinguishedName;
	}

	public String getEmployeeID()
	{
		return this.employeeID;
	}

	public void setEmployeeID(String employeeID)
	{
		this.employeeID = employeeID;
	}

	public byte[] getGoogleImage()
	{
		return this.googleImage;
	}

	public void setGoogleImage(byte[] googleImage)
	{
		this.googleImage = googleImage;
	}

	public String getInitials()
	{
		return this.initials;
	}

	public void setInitials(String initials)
	{
		this.initials = initials;
	}

	public String getLocation()
	{
		return this.location;
	}

	public void setLocation(String location)
	{
		this.location = location;
	}

	public String getMail()
	{
		return this.mail;
	}

	public void setMail(String mail)
	{
		this.mail = mail;
	}

	public String getManager()
	{
		return this.manager;
	}

	public void setManager(String manager)
	{
		this.manager = manager;
	}

	public String getMobile()
	{
		return this.mobile;
	}

	public void setMobile(String mobile)
	{
		this.mobile = mobile;
	}

	public String getState()
	{
		return this.state;
	}

	public void setState(String state)
	{
		this.state = state;
	}

	public byte[] getStoreImage()
	{
		return this.storeImage;
	}

	public void setStoreImage(byte[] storeImage)
	{
		this.storeImage = storeImage;
	}

	public String getTelephoneNumber()
	{
		return this.telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber)
	{
		this.telephoneNumber = telephoneNumber;
	}

	public String getUserPrincipalName()
	{
		return this.userPrincipalName;
	}

	public void setUserPrincipalName(String userPrincipalName)
	{
		this.userPrincipalName = userPrincipalName;
	}

}