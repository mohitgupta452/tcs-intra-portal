package com.turningcloud.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the salary_details database table.
 * 
 */
@Entity
@Table(name="salary_details")
@NamedQuery(name="SalaryDetail.findAll", query="SELECT s FROM SalaryDetail s")
public class SalaryDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int sdId;

	private String createdBy;

	private String empAccountNo;

	private String empBankAccount;

	private String grossSalary;

	private String patCurrency;

	private String salary_Details;

	private String updatedAt;

	//bi-directional many-to-one association to EmpDatainfo
	@OneToMany(mappedBy="salaryDetail")
	private List<EmpDatainfo> empDatainfos;

	//bi-directional many-to-one association to Empcontinfo
	@ManyToOne
	@JoinColumn(name="empContDetailId")
	private Empcontinfo empcontinfo;

	public SalaryDetail() {
	}

	public int getSdId() {
		return this.sdId;
	}

	public void setSdId(int sdId) {
		this.sdId = sdId;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getEmpAccountNo() {
		return this.empAccountNo;
	}

	public void setEmpAccountNo(String empAccountNo) {
		this.empAccountNo = empAccountNo;
	}

	public String getEmpBankAccount() {
		return this.empBankAccount;
	}

	public void setEmpBankAccount(String empBankAccount) {
		this.empBankAccount = empBankAccount;
	}

	public String getGrossSalary() {
		return this.grossSalary;
	}

	public void setGrossSalary(String grossSalary) {
		this.grossSalary = grossSalary;
	}

	public String getPatCurrency() {
		return this.patCurrency;
	}

	public void setPatCurrency(String patCurrency) {
		this.patCurrency = patCurrency;
	}

	public String getSalary_Details() {
		return this.salary_Details;
	}

	public void setSalary_Details(String salary_Details) {
		this.salary_Details = salary_Details;
	}

	public String getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public List<EmpDatainfo> getEmpDatainfos() {
		return this.empDatainfos;
	}

	public void setEmpDatainfos(List<EmpDatainfo> empDatainfos) {
		this.empDatainfos = empDatainfos;
	}

	public EmpDatainfo addEmpDatainfo(EmpDatainfo empDatainfo) {
		getEmpDatainfos().add(empDatainfo);
		empDatainfo.setSalaryDetail(this);

		return empDatainfo;
	}

	public EmpDatainfo removeEmpDatainfo(EmpDatainfo empDatainfo) {
		getEmpDatainfos().remove(empDatainfo);
		empDatainfo.setSalaryDetail(null);

		return empDatainfo;
	}

	public Empcontinfo getEmpcontinfo() {
		return this.empcontinfo;
	}

	public void setEmpcontinfo(Empcontinfo empcontinfo) {
		this.empcontinfo = empcontinfo;
	}

}