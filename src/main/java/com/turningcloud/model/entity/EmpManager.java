package com.turningcloud.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the emp_manager database table.
 * 
 */
@Entity
@Table(name = "emp_manager")
@NamedQuery(name = "EmpManager.findAll", query = "SELECT e FROM EmpManager e")
public class EmpManager implements Serializable
{
	private static final long	serialVersionUID	= 1L;

	@Id
	private int					emId;

	private String				action;

	@Temporal(TemporalType.TIMESTAMP)
	private Date				creation;

	@ManyToOne
	@JoinColumn(name = "empId")
	private Employeedata		employeedata;

	private String				ipAddress;

	@ManyToOne
	@JoinColumn(name = "managerId")
	private Employeedata		employedata;

	private String				managerName;

	private String				managerRole;

	private String				status;

	@Temporal(TemporalType.TIMESTAMP)
	private Date				updation;

	public EmpManager()
	{
	}

	public int getEmId()
	{
		return this.emId;
	}

	public void setEmId(int emId)
	{
		this.emId = emId;
	}

	public String getAction()
	{
		return this.action;
	}

	public void setAction(String action)
	{
		this.action = action;
	}

	public Date getCreation()
	{
		return this.creation;
	}

	public void setCreation(Date creation)
	{
		this.creation = creation;
	}

	public String getIpAddress()
	{
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress)
	{
		this.ipAddress = ipAddress;
	}

	public Employeedata getEmployeedata()
	{
		return employeedata;
	}

	public void setEmployeedata(Employeedata employeedata)
	{
		this.employeedata = employeedata;
	}

	public Employeedata getEmployedata()
	{
		return employedata;
	}

	public void setEmployedata(Employeedata employedata)
	{
		this.employedata = employedata;
	}

	public String getManagerName()
	{
		return this.managerName;
	}

	public void setManagerName(String managerName)
	{
		this.managerName = managerName;
	}

	public String getManagerRole()
	{
		return this.managerRole;
	}

	public void setManagerRole(String managerRole)
	{
		this.managerRole = managerRole;
	}

	public String getStatus()
	{
		return this.status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public Date getUpdation()
	{
		return this.updation;
	}

	public void setUpdation(Date updation)
	{
		this.updation = updation;
	}

}