package com.turningcloud.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the payslips database table.
 * 
 */
@Entity
@Table(name = "payslips")
@NamedQueries(
{ @NamedQuery(name = "Payslip.findAll", query = "SELECT p FROM Payslip p"),
		@NamedQuery(name = "GeneratePaySlip", query = "SELECT p FROM Payslip p WHERE p.psid=:key"),
		@NamedQuery(name = "PaySlipByUser",
				query = "SELECT p FROM Payslip p WHERE p.employeedata.empDataID=:key") })
public class Payslip implements Serializable
{
	private static final long	serialVersionUID	= 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int					psid;

	private String				abscent;

	private String				abscentAmt;

	private String				basicMonth;

	private String				cl;

	private String				conveyance;

	private String				daily;

	private String				deductionAllowance;

	private String				deputation;

	private String				el;

	@ManyToOne
	@JoinColumn(name = "empDataId")
	private EmpDatainfo			EmpDatainfo;

	@ManyToOne
	@JoinColumn(name = "empId")
	private Employeedata		employeedata;

	private String				epsdeduction;

	private String				esi;

	private String				esicdeduction;

	private String				esideduction;

	private String				geneatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	private Date				generatedAt;

	private String				glwf;

	private String				hday;

	private String				hra;

	private String				incomeTax;

	private String				mday;

	private String				month;

	private String				netpay;

	private String				payHourAmount;

	private String				performancePay;

	private String				pf;

	private String				pfdeduction;

	private String				phour;

	private String				reimbursement;

	private String				sl;

	private String				specialAllowance;

	private String				status;

	private String				wday;

	private String				year;

	public Payslip()
	{
	}

	public int getPsid()
	{
		return this.psid;
	}

	public void setPsid(int psid)
	{
		this.psid = psid;
	}

	public String getAbscent()
	{
		return this.abscent;
	}

	public void setAbscent(String abscent)
	{
		this.abscent = abscent;
	}

	public String getAbscentAmt()
	{
		return this.abscentAmt;
	}

	public void setAbscentAmt(String abscentAmt)
	{
		this.abscentAmt = abscentAmt;
	}

	public String getBasicMonth()
	{
		return this.basicMonth;
	}

	public void setBasicMonth(String basicMonth)
	{
		this.basicMonth = basicMonth;
	}

	public String getCl()
	{
		return this.cl;
	}

	public void setCl(String cl)
	{
		this.cl = cl;
	}

	public String getConveyance()
	{
		return this.conveyance;
	}

	public void setConveyance(String conveyance)
	{
		this.conveyance = conveyance;
	}

	public String getDaily()
	{
		return this.daily;
	}

	public void setDaily(String daily)
	{
		this.daily = daily;
	}

	public String getDeductionAllowance()
	{
		return this.deductionAllowance;
	}

	public void setDeductionAllowance(String deductionAllowance)
	{
		this.deductionAllowance = deductionAllowance;
	}

	public String getDeputation()
	{
		return this.deputation;
	}

	public EmpDatainfo getEmpDatainfo()
	{
		return EmpDatainfo;
	}

	public void setEmpDatainfo(EmpDatainfo empDatainfo)
	{
		EmpDatainfo = empDatainfo;
	}

	public Employeedata getEmployeedata()
	{
		return employeedata;
	}

	public void setEmployeedata(Employeedata employeedata)
	{
		this.employeedata = employeedata;
	}

	public void setDeputation(String deputation)
	{
		this.deputation = deputation;
	}

	public String getEl()
	{
		return this.el;
	}

	public void setEl(String el)
	{
		this.el = el;
	}

	public String getEpsdeduction()
	{
		return this.epsdeduction;
	}

	public void setEpsdeduction(String epsdeduction)
	{
		this.epsdeduction = epsdeduction;
	}

	public String getEsi()
	{
		return this.esi;
	}

	public void setEsi(String esi)
	{
		this.esi = esi;
	}

	public String getEsicdeduction()
	{
		return this.esicdeduction;
	}

	public void setEsicdeduction(String esicdeduction)
	{
		this.esicdeduction = esicdeduction;
	}

	public String getEsideduction()
	{
		return this.esideduction;
	}

	public void setEsideduction(String esideduction)
	{
		this.esideduction = esideduction;
	}

	public String getGeneatedBy()
	{
		return this.geneatedBy;
	}

	public void setGeneatedBy(String geneatedBy)
	{
		this.geneatedBy = geneatedBy;
	}

	public Date getGeneratedAt()
	{
		return this.generatedAt;
	}

	public void setGeneratedAt(Date generatedAt)
	{
		this.generatedAt = generatedAt;
	}

	public String getGlwf()
	{
		return this.glwf;
	}

	public void setGlwf(String glwf)
	{
		this.glwf = glwf;
	}

	public String getHday()
	{
		return this.hday;
	}

	public void setHday(String hday)
	{
		this.hday = hday;
	}

	public String getHra()
	{
		return this.hra;
	}

	public void setHra(String hra)
	{
		this.hra = hra;
	}

	public String getIncomeTax()
	{
		return this.incomeTax;
	}

	public void setIncomeTax(String incomeTax)
	{
		this.incomeTax = incomeTax;
	}

	public String getMday()
	{
		return this.mday;
	}

	public void setMday(String mday)
	{
		this.mday = mday;
	}

	public String getMonth()
	{
		return this.month;
	}

	public void setMonth(String month)
	{
		this.month = month;
	}

	public String getNetpay()
	{
		return this.netpay;
	}

	public void setNetpay(String netpay)
	{
		this.netpay = netpay;
	}

	public String getPayHourAmount()
	{
		return this.payHourAmount;
	}

	public void setPayHourAmount(String payHourAmount)
	{
		this.payHourAmount = payHourAmount;
	}

	public String getPerformancePay()
	{
		return this.performancePay;
	}

	public void setPerformancePay(String performancePay)
	{
		this.performancePay = performancePay;
	}

	public String getPf()
	{
		return this.pf;
	}

	public void setPf(String pf)
	{
		this.pf = pf;
	}

	public String getPfdeduction()
	{
		return this.pfdeduction;
	}

	public void setPfdeduction(String pfdeduction)
	{
		this.pfdeduction = pfdeduction;
	}

	public String getPhour()
	{
		return this.phour;
	}

	public void setPhour(String phour)
	{
		this.phour = phour;
	}

	public String getReimbursement()
	{
		return this.reimbursement;
	}

	public void setReimbursement(String reimbursement)
	{
		this.reimbursement = reimbursement;
	}

	public String getSl()
	{
		return this.sl;
	}

	public void setSl(String sl)
	{
		this.sl = sl;
	}

	public String getSpecialAllowance()
	{
		return this.specialAllowance;
	}

	public void setSpecialAllowance(String specialAllowance)
	{
		this.specialAllowance = specialAllowance;
	}

	public String getStatus()
	{
		return this.status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getWday()
	{
		return this.wday;
	}

	public void setWday(String wday)
	{
		this.wday = wday;
	}

	public String getYear()
	{
		return this.year;
	}

	public void setYear(String year)
	{
		this.year = year;
	}

}