package com.turningcloud.model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the role_manager database table.
 * 
 */
@Entity
@Table(name = "role_manager")
@NamedQueries({
	@NamedQuery(name = "RoleManager.findAll", query = "SELECT r FROM RoleManager r"),
	@NamedQuery(name = "RoleManager.findAllBYUpn", query = "SELECT r FROM RoleManager r Where r.employeedata.userPrincipalName=:key")
})

public class RoleManager implements Serializable
{
	private static final long	serialVersionUID	= 1L;

	@Id
	private int					rid;

	private String				action;

	private String				createdby;

	@Temporal(TemporalType.TIMESTAMP)
	private Date				creationDate;

	@ManyToOne
	@JoinColumn(name = "empDataID")
	private Employeedata		employeedata;

	private String				roles;

	private String				status;

	public RoleManager()
	{
	}

	public int getRid()
	{
		return this.rid;
	}

	public void setRid(int rid)
	{
		this.rid = rid;
	}

	public String getAction()
	{
		return this.action;
	}

	public void setAction(String action)
	{
		this.action = action;
	}

	public String getCreatedby()
	{
		return this.createdby;
	}

	public void setCreatedby(String createdby)
	{
		this.createdby = createdby;
	}

	public Date getCreationDate()
	{
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate)
	{
		this.creationDate = creationDate;
	}

	public Employeedata getEmployeedata()
	{
		return employeedata;
	}

	public void setEmployeedata(Employeedata employeedata)
	{
		this.employeedata = employeedata;
	}

	public String getRoles()
	{
		return this.roles;
	}

	public void setRoles(String roles)
	{
		this.roles = roles;
	}

	public String getStatus()
	{
		return this.status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

}