<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="template" tagdir="/WEB-INF/tags"%>
<template:form>
	<jsp:attribute name="headContent">
	 <link
			href="https://cdn.datatables.net/1.10.14/css/jquery.dataTables.min.css"
			rel="stylesheet " type="text/css" />
    <link href="assets/css/custom/employeeData.css" rel="stylesheet"
			type="text/css" />
    <script src='assets/js/plugins/jquery/jquery.min.js'></script>
    <script type='text/javascript'
			src='assets/js/plugins/jquery/jquery.validate.js'></script>
    <script type='text/javascript'
			src='assets/js/plugins/bootstrap/bootstrap.min.js'></script>
    <script type='text/javascript'
			src='assets/js/custom/Employee_Data.js'></script>
    <script type='text/javascript' src='assets/js/custom/utility.js'></script>
    <script type='text/javascript'
			src='assets/js/plugins/jquery/jquery-ui.min.js'></script>
      <script type='text/javascript' src='assets/js/custom/jquery.dataTables.min.js'></script>
    <style>
.modal-content {
	background: #fff;
}

#directory {
	display: none;
}

#directory::-webkit-scrollbar-track {
	-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
	border-radius: 10px;
	background-color: #F5F5F5;
}
#myBtn {
  display: none;
  position: fixed;
  bottom: 40px;
  right: 30px;
  z-index: 99;
  border: none;
  outline: none;
 border:1px solid #00838f;
  color: white;
  cursor: pointer;
  padding: 15px;
  border-radius: 50%;
  background:transparent;
}

#myBtn:hover {
  background-color: transparent;
}
select {
	background: #E0E0E0 !important;
}

#directory::-webkit-scrollbar {
	width: 12px;
	background-color: #F5F5F5;
}

label {
	color: #000 !important;
}

#directory::-webkit-scrollbar-thumb {
	border-radius: 10px;
	-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
	background-color: #555;
}

.direc_con {
	overflow-y: scroll;
}

.data-table-header {
	background-color: whitesmoke !important;
	opacity: .5;
}

.data-table-body {
	background-color: whitesmoke !important;
}

.bg-color-e0e0e0 {
	background: #E0E0E0 !important;
}

.block .header {
	height: 16px;
}

input[type="search"] {
	background: #E0E0E0 !important;
}

.block .content {
	padding: 10px !important;
}

h2 {
	font-size: 16px;
	font-style: normal;
	font-family: roboto;
	color: #000 !important;
}

.bg-white {
	background: #fff !important;
}

table.dataTable.no-footer {
	border-bottom: none !important
}

.table-striped>tbody>tr:nth-child(odd) {
	background: #fff !important
}

.table-striped>tbody>tr:nth-child(even) {
	background: #ededed !important
}

.modal-footer {
	background: #00838f !important;
	margin-top: 20px !important;
}

.block .footer {
	height: 40px !important;
}

body.modal-open {
	overflow: hidden;
	position: fixed;
}
.btn-default{border-color: #F4F4F4;
    background: #F4F4F4;
    color: #00838f !important;}
      .loader {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('assets\img\turning_image\Loading_icon .gif') 50% 50% no-repeat rgb(249,249,249);
}
    
</style>



    </jsp:attribute>


	<jsp:body>
	<div class="loader"></div>
                    <div class="col-md-10 layout">
                        <div class="row">
                            <div class="block box ">
                                
                                <div class="content data-table-body">
                                 <h2>Employee Data</h2>
                                    <table cellpadding="0"
							cellspacing="0" width="100%"
							class="table table-bordered table-striped sortable dataTable"
							id="example">
                                        <thead>
                                            <tr>
                                                <th>Employee Code</th>
                                                <th>Emp. File no. </th>
                                                <th>FullName</th>
                                                <th>Emp. Status </th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr>
                                                <td>TCEMP1054</td>
                                                <td>1054</td>
                                                <td>Kavita Singh</td>
                                                <td>nvcxxhcj</td>
                                                <td><a
										href="DownloadResponse?fileName=PaySlip&requestId=1&service=PaySlip"><button
												type="button" class="btn edit">EDIT</button></a></td>
                                            </tr>
                                            <tr>
                                                <td>TCEMP1054</td>
                                                <td>1006</td>
                                                <td>Manoj Singh</td>
                                                <td>nvcxxhcj</td>
                                                <td><button
											type="button" class="btn edit" data-toggle="modal"
											data-target="#edit_modal">EDIT</button></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                        <div class="row ">
                            <div class="block box">
                                
                                <div class="content data-table-body">
                                 <h2>Approval Authorities</h2>
                                    <table cellpadding="0"
							cellspacing="0" width="100%"
							class="table table-bordered table-striped sortable dataTable"
							id="authority">

                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Data 1</th>
                                                <th>Data 2</th>
                                                <th>Data 2</th>
                                                <th>Status</th>

                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>abc</td>
                                                <td>abc</td>
                                                <td>Reporting Manager</td>
                                                <td>Active</td>

                                            </tr>

                                            <tr>
                                                <td>2</td>
                                                <td>efg</td>
                                                <td>abc</td>
                                                <td>Hr manager</td>
                                                <td>Active</td>

                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="block box ">
                                
                                <div class="content data-table-body">
                                 <h2>Project Manager</h2>
                                    <table cellpadding="0"
							cellspacing="0" width="100%"
							class="table table-bordered table-striped sortable dataTable"
							id="project_manager">

                                        <thead>
                                            <tr>
                                                <th>Project Name</th>
                                                <th>Project Manager</th>
                                                <th>Data set</th>
                                                <th>Data set</th>
                                                <th>status</th>

                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr>
                                                <td>abc</td>
                                                <td>abc</td>
                                                <td>12-April-2017</td>
                                                <td>12-April-2017</td>
                                                <td>Hold</td>

                                            </tr>

                                            <tr>
                                                <td>cde</td>
                                                <td>cde</td>
                                                <td>20-April-2017</td>
                                                <td>20-April-2017</td>
                                                <td>Hold</td>

                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                 
                    </div>
               
                   
                    <!-- mydirectory -->
                    <div class="direc_con" id="directory"
			style="display: none; border: 1px solid #000">
                        <i class="fa fa-caret-up" aria-hidden="true"
				style="position: relative; left: 748px; font-size: 25px;"></i>
                        <div
				class="block block-transparent block-drop-shadow  hovershow"
				style="border-radius: 8px;">

                            <div class="container">
                                <div class="col-md-12  mydir">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <a href="#"> <img
									src="assets\img\turning_image\TeamC1.png" width="70px"
									height="70px" />
                                                <p>Team Collaboration </p>
                                        
							
							
							
							</div>
                                        </a>
                                        <a href="ijp.jsp">
                                            <div class="col-md-2">
                                                <img
										src="assets\img\turning_image\ijp1.png" width="70px"
										height="70px" />
                                                <p>IJP(Internal Job Posting) </p>
                                            </div>
                                        </a>
                                        <a href="Ticketing.jsp">
                                            <div class="col-md-2">
                                                <img
										src="assets\img\turning_image\helpDesk1.png" width="70px"
										height="70px" />
                                                <p>Help Desk Ticketing </p>
                                            </div>
                                        </a>
                                        <a href="Recognition.jsp">
                                            <div class="col-md-2">
                                                <img
										src="assets\img\turning_image\EmpRecognition1.png"
										width="70px" height="70px" />
                                                <p>Employee Recognition </p>
                                            </div>
                                        </a>
                                        <a href="Download_payslip.jsp">
                                            <div class="col-md-2">
                                                <img
										src="assets\img\turning_image\downloads.png" width="70px"
										height="70px" />
                                                <p>Download PaySlip</p>
                                            </div>
                                        </a>
                                        <a href="Payslip.jsp">
                                            <div class="col-md-2">
                                                <img
										src="assets\img\turning_image\payslip1.png" width="70px"
										height="70px" />
                                                <p>Generate Payslips </p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="row">

                                        <a href="Reimbursement.jsp">
                                            <div class="col-md-2">
                                                <img
										src="assets\img\turning_image\Reimbursement1.png" width="70px"
										height="70px" />
                                                <p>Reimbursement </p>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="col-md-2">
                                                <img
										src="assets\img\turning_image\teamM1.png" width="70px"
										height="70px" />
                                                <p>Team Management </p>
                                            </div>
                                        </a>
                                        <a href="hr.jsp">
                                            <div class="col-md-2">
                                                <img
										src="assets\img\turning_image\hr1.png" width="70px"
										height="70px" />
                                                <p>HR Activities </p>
                                            </div>
                                        </a>
                                        <a href="Event_Site.jsp">
                                            <div class="col-md-2">
                                                <img
										src="assets\img\turning_image\EventSite1.png" width="70px"
										height="70px" />
                                                <p>Event Site </p>
                                            </div>
                                        </a>
                                        <a href="Meeting.jsp">
                                            <div class="col-md-2">
                                                <img
										src="assets\img\turning_image\Meeting1.png" width="70px"
										height="70px" />
                                                <p>Meeting / Briefings </p>
                                            </div>
                                        </a>
                                        <a href="Ass_tracking.jsp">
                                            <div class="col-md-2">
                                                <img
										src="assets\img\turning_image\Tracking1.png" width="70px"
										height="70px" />
                                                <p>Assets Tracking System</p>
                                            </div>
                                        </a>

                                    </div>
                                    <div class="row">

                                        <a href="kpis.jsp">
                                            <div class="col-md-2">
                                                <img
										src="assets\img\turning_image\kpis.png" width="70px"
										height="70px" />
                                                <p>KPI(For Manager/ Employee)</p>
                                            </div>
                                        </a>
                                               <a href="kpi.jsp">
                                            <div class="col-md-2">
                                                <img
										src="assets\img\turning_image\kpi1.png" width="70px"
										height="70px" />
                                                <p>KPI(Key Performance Indicator) for HR</p>
                                            </div>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


                    <!--Edit modal  -->
                    <div class="modal modal-wide" id="edit_modal"
			data-backdrop="static" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content  block">
                                <div class="modal-header header">
                                    <button type="button" class="close"
							data-dismiss="modal" aria-hidden="true" tabindex="-1">
                       x </button>
                                    <h4 tabindex="-1"
							class="modal-title font-color">
							<span class="text-color">Add Employee </span>
						</h4>
                                </div>
                                <div class="modal-body clearfix">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="block sim-block">
                                                <ul
										class="nav nav-tabs nav-justified">
                                                    <li
											class="active font-color"><a href="#basic_detail"
											data-toggle="tab" class="text-black">Basic Details</a></li>
                                                    <li
											class="font-color"><a href="#contact_info"
											data-toggle="tab" class="text-black">Contact Info</a></li>
                                                    <li
											class="font-color"><a href="#salary_details"
											data-toggle="tab" class="text-black">Salary Details</a></li>
                                                </ul>
                                                <div
										class="content tab-content bg-white">

                                                    <div
											class="tab-pane active scroll " id="basic_detail">
                                                        <form
												role="form" id="details" action="" method="">
                                                            <div
													class="controls col-md-7">
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-3">
                                                                        <label
																class="modal-label-style text-black">Emp. code<span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-3">
                                                                        <input
																type="text"
																class="form-control input-set m-pad-0 text-black"
																name="Emp_code" id="Emp_code" required />
                                                                    </div>



                                                                    <div
															class="col-md-3">
                                                                        <label
																class="modal-label-style text-black">File no.<span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-3">
                                                                        <input
																type="number"
																class="form-control input-set m-pad-0 text-black"
																name="File_num" id="File_num" />
                                                                    </div>
                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-3">
                                                                        <label
																class="modal-label-style text-black"> Employee Name<span
																style="color: red">*</span></label>

                                                                    </div>
                                                                    <div
															class="col-md-9">
                                                                        <input
																type="text"
																class="form-control input-set m-pad-0 text-black"
																onkeypress="return onlyAlphabets(event,this);"
																name="Emp_name" id="Emp_name" required />
                                                                    </div>
                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-3">
                                                                        <label
																class="modal-label-style text-black">Title <span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-3">
                                                                        <input
																type="text" class="form-control input-set text-black"
																onkeypress="return onlyAlphabets(event,this);"
																name="title" id="title" required />
                                                                    </div>
                                                                    <div
															class="col-md-3">
                                                                        <label
																class="modal-label-style text-black">Type<span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-3">
                                                                        <select
																class="form-control modal_select m-pad-0 text-black"
																name="type" id="type" required>                                    
                                    <option value="">select</option>
                                    <option value="permanent">permanent</option>
                                    <option value="Contract">Contract</option>
                               
                                </select>
                                                                    </div>


                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-3">
                                                                        <label
																class="modal-label-style text-black"> Department <span
																style="color: red">*</span></label>

                                                                    </div>
                                                                    <div
															class="col-md-9">
                                                                        <select
																class="form-control modal_select m-pad-0 text-black"
																name="department" id="department" required>   
                                                        <option value="">Select</option>                                 
                                    <option value="Lab/Research">Lab/Research</option>
                                    <option value=" Finance"> Finance</option>
                                     <option value="Human Resource"> Human Resource</option>
                                      <option value="Marketing">Marketing</option>
                               
                                </select>
                                                                    </div>
                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-3">
                                                                        <label
																class="modal-label-style text-black"> Designation<span
																style="color: red">*</span></label>

                                                                    </div>
                                                                    <div
															class="col-md-9">
                                                                        <select
																class="form-control modal_select m-pad-0 text-black"
																name="designation" id="designation" required> 
                                                        <option value="">select</option>                                   
                                    <option value="Accountant">Accountant</option>
                                    <option value="HR"> HR</option>
                                     <option value="Software Developer"> Software Developer</option>
                                     
                               
                                </select>
                                                                    </div>
                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-3">
                                                                        <label
																class="modal-label-style text-black">Hire Date <span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-3">
                                                                        <input
																type="date"
																class="form-control input-set m-pad-0 text-black"
																name="hire_date" id="hire_date" required />
                                                                    </div>
                                                                    <div
															class="col-md-3">
                                                                        <label
																class="modal-label-style text-black">Status<span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-3">
                                                                        <select
																class="form-control modal_select m-pad-0 text-black"
																name="status" id="status" required>  
                                                            <option
																	value="">Select</option>                                  
                                    <option value="On Service">On Service </option>
                                    <option value="Suspend">Suspend</option>
                                <option value="Retired">Retired</option>
                                 <option value="Discontinued">Discontinued</option>
                                </select>
                                                                    </div>


                                                                </div>

                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-3">
                                                                        <label
																class="modal-label-style text-black"> Discontinued On<span
																style="color: red">*</span></label>

                                                                    </div>
                                                                    <div
															class="col-md-9">
                                                                        <input
																type="date"
																class="form-control input-set m-pad-0 text-black"
																name="discontinued" id="discontinued" required />
                                                                    </div>
                                                                </div>

                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-3">
                                                                        <label
																class="modal-label-style text-black">Passport status<span
																style="color: red">*</span></label>

                                                                    </div>
                                                                    <div
															class="col-md-9">

                                                                        <div
																class="checkbox">
                                                                            <div
																	style="padding-left: 15px; margin-top: 6px;"
																	class="checkbox col-md-6">
																	
																		<label
																		class="control control--checkbox modal-label-style text-black"><span
																		style="margin-left: 30px; font-size: 15px !important;">Yes</span> 
		                                                                   <input
																		type="checkbox" class="check-box" 
																		id="yes" value="yes" />
		                                                                     <div
																			class="control__indicator"></div>
	                                                       </label>                      
																</div>
                                                                            <div
																	style="padding-left: 0px; margin-top: 6px;"
																	class="checkbox col-md-6">
																		<label
																		class="control control--checkbox modal-label-style text-black"><span
																		style="margin-left: 30px; font-size: 15px !important;">No</span> 
		                                                                   <input
																		type="checkbox" class="check-box" 
																		id="No" value="No" />
		                                                                     <div
																			class="control__indicator"></div>
	                                                       </label> 
																</div>

                                                                            <!--<label class="modal-label-style">Yes</label>
                                                        <input type="checkbox" name="status" value="yes">
                                                        <label class="modal-label-style">No</label>
                                                        <input type="checkbox" name="status" value="no"></label>-->

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-3">
                                                                        <label
																class="modal-label-style text-black">Passport No.

                                                                    </div>
                                                                    <div
															class="col-md-9">
                                                                        <input
																type="text"
																class="form-control input-set m-pad-0 text-black"
																name="passportNum" id="passportNum" disabled="disabled" required />
                                                                    </div>
                                                                </div>

                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-3">
                                                                        <label
																class="modal-label-style text-black">Remarks<span
																style="color: red">*</span></label>

                                                                    </div>
                                                                    <div
															class=" col-md-9">
                                                                        <textarea
																row="50" col="200"
																class="form-control input-set m-pad-0 m-top-0 textArea-style text-black"
																name="remarks" id="remarks" required></textarea>
                                                                    </div>
                                                                </div>



                                                            </div>

                                                            <div
													class="controls col-md-5">
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-7">
                                                                        <label
																class="modal-label-style text-black">Upload Image<span
																style="color: red">*</span></label>
                                                                        <input
																type="file" name="file" id="file"
																class="inputfile text-black" name="upload_image"
																id="upload_image" required />
                                                                        <label
																for="file" class="custom-label"><img
																src="assets/img/turning_image/upload.png" width="30px"
																height="30px" /></label>
                                                                    </div>
                                                                    <div
															class="col-md-3"
															>
                                                                         <img id="blah" src="#" alt="Your image goes here..." width="100px" height="100px" style=" border:3px solid #00838f;" class="img-square"/>
                                                                    </div>

                                                                </div>
                                                                <label
														class="modal-label-style" style="color: red !important">Personal Details</label>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-4">
                                                                        <label
																class="modal-label-style text-black">Father Name<span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-8">
                                                                        <input
																type="text"
																class="form-control input-set m-pad-0 text-black"
																onkeypress="return onlyAlphabets(event,this);"
																name="fname" id="fname" required />
                                                                    </div>

                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-4">
                                                                        <label
																class="modal-label-style text-black">Mother Name<span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-8">
                                                                        <input
																type="text"
																class="form-control input-set m-pad-0 text-black"
																onkeypress="return onlyAlphabets(event,this);"
																name="mname" id="mname" required />
                                                                    </div>

                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-4">
                                                                        <label
																class="modal-label-style text-black">Spouse Name<span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-8">
                                                                        <input
																type="text"
																class="form-control input-set m-pad-0 text-black"
																onkeypress="return onlyAlphabets(event,this);"
																name="sname" id="sname" required />
                                                                    </div>

                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-4" style="padding-left: 12px;">
                                                                        <label
																class="modal-label-style text-black">Gender<span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-8">
                                                                        <div
																class="checkbox">
                                                                            <div
																	class="checkbox col-md-3 pad-left-0"> 
																		<label
																		class="control control--radio modal-label-style text-black"><span
																		style="margin-left: 30px; font-size: 15px;">Male</span>
		                                                                       <input
																		type="radio" name="gender" id="gender"
																		class="input-radio-male" />
		                                                                         <div
																			class="control__indicator"></div>
	                                                                                    </label>
																</div>
                                                                            <div
																	class="checkbox col-md-3 mar-pad"> <label
																		class="control control--radio modal-label-style text-black"><span
																		style="margin-left: 30px; font-size: 15px;">Female</span>
		                                                                       <input
																		type="radio" name="gender" id="gender"
																		class="input-radio-female" />
		                                                                         <div
																			class="control__indicator"></div>
	                                                                                    </label>
																</div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-4">
                                                                        <label
																class="modal-label-style text-black">Birth Date<span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-8">
                                                                        <input
																type="Date"
																class="form-control input-set m-pad-0 text-black"
																name="birth_date" id="birth_date" required />
                                                                    </div>

                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-4">
                                                                        <label
																class="modal-label-style text-black">Marital Status<span
																style="color: red">*</span></label>

                                                                    </div>
                                                                    <div
															class="col-md-8">
                                                                        <select
																class="form-control modal_select m-pad-0 text-black"
																name="martial_status" id="martial_status" required> 
                                                        <option value="">select</option>                                   
                                    <option value="Married">Married</option>
                                    <option value=" UnMarried"> UnMarried</option>
                                     
                                     
                               
                                </select>
                                                                    </div>
                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-4">
                                                                        <label
																class="modal-label-style text-black">Religion<span
																style="color: red">*</span></label>

                                                                    </div>
                                                                    <div
															class="col-md-8">
                                                                        <select
																class="form-control modal_select m-pad-0 text-black"
																name="religion" id="religion" required> 
                                                        <option value="">select</option>                                   
                                    <option value="Sikh">Sikh</option>
                                    <option value="Hindu"> Hindu</option>
                                    <option value="Muslim">Muslim</option>
                                    <option value="Christian"> Christian</option>
                                     
                                     
                               
                                </select>
                                                                    </div>
                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-12">
                                                                        <button
																type="submit" class="btn btn-default btn-block">
																Save basic details</button>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </form>
                                                    </div>

                                                    <div
											class="tab-pane bg-form" id="contact_info">
                                                        <form
												role="form" id="personal" action=" " method=" ">
                                                            <div
													class="controls col-md-7">

                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-4">
                                                                        <label
																class="modal-label-style">Telephone<span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-8">
                                                                        <input
																type="text" class="form-control input-set m-pad-0"
																maxlength="10" onkeypress="return isNumber(event)"
																name="telephone" id="telephone" />
                                                                    </div>

                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-4">
                                                                        <label
																class="modal-label-style text-black">Mobile<span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-8">
                                                                        <input
																type="text"
																class="form-control input-set m-pad-0 text-black"
																maxlength="10" onkeypress="return isNumber(event)"
																name="mobile" id="mobile" />
                                                                    </div>

                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-4">
                                                                        <label
																class="modal-label-style text-black">City<span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-8">
                                                                        <input
																type="text"
																class="form-control input-set m-pad-0 text-black"
																name="city" id="city" />
                                                                    </div>

                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-4">
                                                                        <label
																class="modal-label-style text-black">State<span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-8">
                                                                        <select
																class="form-control modal_select m-pad-0 text-black"
																name="state" id="state"> 
                                                        <option value="">select</option>                                   
                                    <option value="Haryana">Haryana</option>
                                    <option value="Punjab"> Punjab</option>
                                    <option value="kolkata">kolkata</option>
                                    <option value="Uttar Pradesh"> Uttar Pradesh</option>
                                     
                                     
                               
                                </select>
                                                                    </div>

                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-4">
                                                                        <label
																class="modal-label-style text-black">Country<span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-8">
                                                                        <select
																class="form-control modal_select m-pad-0 text-black"
																name="country" id="country"> 
                                                        <option value="">select</option>                                   
                                    <option value="India">India</option>
                                   
                                     
                                     
                               
                                </select>
                                                                    </div>

                                                                </div>

                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-4">
                                                                        <label
																class="modal-label-style text-black">Email Address<span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-8">
                                                                        <input
																type="email"
																class="form-control input-set m-pad-0 text-black"
																name="email" id="email" />
                                                                    </div>

                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-4">
                                                                        <label
																class="modal-label-style text-black">Current Address<span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-8">
                                                                        <textarea
																row="20" col="100"
																class="form-control input-set  m-pad-0 m-top-0 text-black"
																name="temp_address" id="temp_address"></textarea>
                                                                    </div>

                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-4">
                                                                        <label
																class="modal-label-style text-black">Permanent Address<span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-8">
                                                                        <textarea
																row="20" col="100"
																class="form-control input-set  m-pad-0 m-top-0 text-black"
																name="per_address" id="per_address"></textarea>
                                                                    </div>

                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-8 col-md-offset-4">
                                                                        <button
																type="submit" class="btn btn-default btn-block" id="basic_btn">
																Save Contact details</button>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </form>
                                                    </div>

                                                    <div
											class="tab-pane bg-form" id="salary_details">
                                                        <form
												role="form" id="salary">
                                                            <div
													class="controls col-md-7">
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-4">
                                                                        <label
																class="modal-label-style text-black">Basic Salary<span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-8">
                                                                        <input
																type="text"
																class="form-control input-set m-pad-0 text-black"
																name="basic_salary" id="basic_salary" />
                                                                    </div>

                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-4">
                                                                        <label
																class="modal-label-style text-black">Gross Salary<span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-8">
                                                                        <input
																type="text"
																class="form-control input-set m-pad-0 text-black"
																name="gross_salary" id="gross_salary" />
                                                                    </div>
                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-4">
                                                                        <label
																class="modal-label-style text-black">Pay Currency<span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-8">
                                                                        <select
																class="form-control modal_select m-pad-0 text-black"
																name="pay_currency" id="pay_currency"> 
                                                        <option value="">select</option>                                   
                                    <option value="Indian">Indian</option>
                                   
                                     
                                     
                               
                                </select>
                                                                    </div>

                                                                </div>
                                                                <div
														class="form-row">
                                                                    <div
															class="col-md-4">
                                                                        <label
																class="modal-label-style text-black">Emp. Bank Account<span
																style="color: red">*</span></label>
                                                                    </div>
                                                                    <div
															class="col-md-8">
                                                                        <select
																class="form-control modal_select m-pad-0 text-black"
																name="emp_bank_acc" id="emp_bank_acc"> 
                                                        <option value="">select</option>                                   
                                    <option value="HDFC">HDFC</option>
                                   
                                     
                                     
                               
                                </select>
                                                                    </div>
                                                                    <div
															class="form-row">
                                                                        <div
																class="col-md-4">
                                                                            <label
																	class="modal-label-style text-black">Emp. Account No<span
																	style="color: red">*</span></label>
                                                                        </div>
                                                                        <div
																class="col-md-8">
                                                                            <input
																	type="text"
																	class="form-control input-set m-pad-0 m-top-7 text-black"
																	name="emp_account_num" id="emp_account_num" />
                                                                        </div>
                                                                    </div>


                                                                    <div
															class="form-row">
                                                                        <div
																class="col-md-8 col-md-offset-4">
                                                                            <button
																	type="submit"
																	class="btn btn-default btn-block">
																	Save salary details</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>



                                </div>
                                <div class="modal-footer footer">
                                    

                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- End of Edit Modal -->


                </jsp:body>
</template:form>