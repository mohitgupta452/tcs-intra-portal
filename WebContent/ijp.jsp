<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@page contentType="text/html" pageEncoding="UTF-8"%>
        <%@taglib prefix="template" tagdir="/WEB-INF/tags"%>
            <template:form>
                <jsp:attribute name="headContent">
                    <script src='assets/js/plugins/jquery/jquery.min.js'></script>
                    <script type='text/javascript' src='assets/js/plugins/jquery/jquery.validate.js'></script>
                    <script type='text/javascript' src='assets/js/plugins/bootstrap/bootstrap.min.js'></script>

                    <script type='text/javascript' src='assets/js/custom/ijp.js'></script>
                    <script type='text/javascript' src='assets/js/plugins/jquery/jquery-ui.min.js'></script>
                    <style>
                        label.error {
    color: rgb(244,67,54);
}
                        
                        input.error {
                            border: 1px solid  rgb(244,67,54);
                        }
                        
                        textarea.error {
                            border: 1px solid  rgb(244,67,54);
                        }                            #directory::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	border-radius: 10px;
	background-color: #F5F5F5;
}
.text-black{color:#000 !important}
#directory::-webkit-scrollbar
{
	width: 12px;
	background-color: #F5F5F5;
}

#directory::-webkit-scrollbar-thumb
{
	border-radius: 10px;
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
	background-color: #555;
}
    .direc_con
{
	
	
	overflow-y: scroll;
	
}.form-control,
select[multiple],
textarea {

     border: 1px solid #ccc;
    background: none !important;
    box-shadow: 1px 1px 1px #ccc;
}
.form-control:focus, input:focus, select:focus, textarea:focus {border:1px solid #ccc;
box-shadow: 1px 1px 1px #ccc;}

                  .block .header h2{color:#fff !important}      
                        .nav-tabs > li > a{text-shadow:none !important}
                        
                        label{font-weight: 500 !important}
                        textarea.form-control {
    height: 50px !important;
}
.control-group {
	display: inline-block;
	width: 200px;
	height: 210px;
	margin: 10px;
	padding: 30px;
	text-align: left;
	vertical-align: top;
	background: #fff;
	box-shadow: 0 1px 2px rgba(0,0,0,.1);
}
.control {
	font-size: 18px;
	position: relative;
	display: block;
	margin-bottom: 15px;
	padding-left: 30px;
	cursor: pointer;
}

.control input {
	position: absolute;
	z-index: -1;
	opacity: 0;
}
.control__indicator {
	position: absolute;
	top: 2px;
	left: 0;
	width: 20px;
	height: 20px;
	}

.control--radio .control__indicator {
	border-radius: 50%;
}

/* Hover and focus states */
.control:hover input ~ .control__indicator,
.control input:focus ~ .control__indicator {
	background: #ccc;
}

/* Checked state */
.control input:checked ~ .control__indicator {
	background: #2aa1c0;
}

/* Hover state whilst checked */
.control:hover input:not([disabled]):checked ~ .control__indicator,
.control input:checked:focus ~ .control__indicator {
	background: #0e647d;
}

/* Disabled state */
.control input:disabled ~ .control__indicator {
	pointer-events: none;
	opacity: .6;
	background: #e6e6e6;
}

/* Check mark */
.control__indicator:after {
	position: absolute;
	display: none;
	content: '';
}

/* Show check mark */
.control input:checked ~ .control__indicator:after {
	display: block;
}

/* Checkbox tick */
.control--checkbox .control__indicator:after {
	top: 4px;
	left: 8px;
	width: 3px;
	height: 8px;
	transform: rotate(45deg);
	border: solid #fff;
	border-width: 0 2px 2px 0;
}

/* Disabled tick colour */
.control--checkbox input:disabled ~ .control__indicator:after {
	border-color: #7b7b7b;
}

/* Radio button inner circle */
.control--radio .control__indicator:after {
	top: 7px;
	left: 7px;
	width: 6px;
	height: 6px;
	border-radius: 50%;
	background: #fff;
}

/* Disabled circle colour */
.control--radio input:disabled ~ .control__indicator:after {
	background: #7b7b7b;
}

                    </style>
                </jsp:attribute>
                <jsp:body>
                    <div class="col-md-10 layout">
                    <div class="row">
                            <div class="block block-drop-shadow">

                                <div class="header form-header ">
                                    <h2>Internal job Posting</h2>
                                </div>
                                <ul class="nav nav-tabs nav-justified">
                                    <li class="active"><a href="#forhr" class="text-black" data-toggle="tab">For HR </a></li>
                                    <li class="text-color"><a href="#foremployee" class="text-black" data-toggle="tab">For Employee/Manager</a></li>

                                </ul>
                                <div class="content tab-content bg-form ">
                                    <div class="tab-pane active " id="forhr">
                                        <form role="form" id="ijpForHr">
                                            <div class="content controls bg-form" bg-form style="margin-top:30px;">
                                                <div class="form-row">
                                                    <div class="col-md-3 text-black">Job Description<span style="color: red">*</span></div>
                                                    <div class="col-md-9">
                                                        <textarea rows="5" cols="50" class="form-control text-black" style="margin-top:0px;" id="job_description" name="job_description"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-3 text-black">Experience <span style="color: red">*</span></div>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control text-black" id="experience" name="experience" />
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-3 text-black">Salary<span style="color: red">*</span></div>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control text-black" id="salary" name="salary" />
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-3 text-black">Signature<span style="color: red">*</span></div>
                                                    <div class="col-md-9">
                                                        <textarea rows="5" cols="50" class="form-control text-black" style="margin-top:0px;" id="signature" name="signature"></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-row  m-top-20">

                                                    <div class="col-md-2 mar-top-12">
                                                        <button type="submit" class="btn btn-block btn-success">Submit</button>
                                                    </div>
                                                    <div class="col-md-2 mar-top-12">
                                                        <button type="reset" class="btn btn-block btn-danger">Cancel</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane " id="foremployee">
                                        <form role="form" id="ijpForEmployee">
                                            <div class="content controls bg-form " style="margin-top:30px;">
                                                <div class="form-row">
                                                    <div class="col-md-3 text-black ">Apply for<span style="color: red">*</span></div>
                                                    <div class="col-md-9">
                                                        <div class="col-md-4">
                                                            
                                                            			<label
																		class="control control--checkbox  text-black"><span
																		style="margin-left: 15px; font-size: 15px !important;">Self</span> 
		                                                                   <input
																		type="checkbox" class="ibutton" name="apply[]" id="apply[]" value="Self" style="margin-top: -19px;" />
		                                                                     <div
																			class="control__indicator"></div>
	                                                       </label>
                                                        </div>
                                                        <div class="col-md-4">
                                                          
                                                            		<label
																		class="control control--checkbox  text-black"><span
																		style="margin-left: 15px; font-size: 15px !important;">Collleagues</span> 
		                                                                   <input
																		type="checkbox" class="ibutton" name="apply[]" id="apply[]" value="Collleagues" style="margin-top: -19px;" />
		                                                                     <div
																			class="control__indicator"></div>
	                                                       </label>
                                                        </div>
                                                        <div class="col-md-4">
                                                          
                                                            <label
																		class="control control--checkbox  text-black"><span
																		style="margin-left: 30px; font-size: 15px !important;">others</span> 
		                                                                   <input
																		type="checkbox" class="ibutton" name="apply[]" id="apply[]" value="others" style="margin-top: -19px;" />
		                                                                     <div
																			class="control__indicator"></div>
	                                                       </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col-md-3 text-black">Current Department<span style="color: red">*</span></div>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control text-black" name="Current_Depart" id="Current_Depart" style="margin-top:10px" />
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col-md-3 text-black">Salary<span style="color: red">*</span></div>
                                                        <div class="col-md-9">
                                                            <input type="number" class="form-control text-black" name="salary" id="salary" />
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col-md-3 text-black">Signature<span style="color: red">*</span></div>
                                                        <div class="col-md-9">
                                                            <textarea rows="5" cols="50" class="form-control text-black" style="margin-top:0px;" name="signature" id="signature"></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-row m-top-20">

                                                        <div class="col-md-2 mar-top-12">
                                                            <button type="submit" class="btn btn-block btn-success">Submit</button>
                                                        </div>
                                                        <div class="col-md-2 mar-top-12">
                                                            <button type="reset" class="btn btn-block btn-danger text-color">Cancel</button>
                                                        </div>
                                                    </div>

                                                </div>

                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    
                    </div>
                  
                    <!-- mydirectory -->
                  <div class="direc_con" id="directory" style="display: none;border:1px solid #000">
                        <i class="fa fa-caret-up" aria-hidden="true" style="position: relative;left: 748px;font-size: 25px;"></i>
                        <div class="block block-transparent block-drop-shadow  hovershow" style="border-radius: 8px;">

                            <div class="container">
                                <div class="col-md-12  mydir">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <a href="#"> <img src="assets\img\turning_image\TeamC1.png" width="70px" height="70px" />
                                                <p>Team Collaboration </p>
                                        </div>
                                        </a>
                                        <a href="ijp.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\ijp1.png" width="70px" height="70px" />
                                                <p>IJP(Internal Job Posting) </p>
                                            </div>
                                        </a>
                                        <a href="Ticketing.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\helpDesk1.png" width="70px" height="70px" />
                                                <p>Help Desk Ticketing </p>
                                            </div>
                                        </a>
                                        <a href="Recognition.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\EmpRecognition1.png" width="70px" height="70px" />
                                                <p>Employee Recognition </p>
                                            </div>
                                        </a>
                                        <a href="Download_payslip.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\downloads.png" width="70px" height="70px" />
                                                <p>Download PaySlip</p>
                                            </div>
                                        </a>
                                        <a href="Payslip.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\payslip1.png" width="70px" height="70px" />
                                                <p>Generate Payslips </p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="row">

                                        <a href="Reimbursement.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\Reimbursement1.png" width="70px" height="70px" />
                                                <p>Reimbursement </p>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\teamM1.png" width="70px" height="70px" />
                                                <p>Team Management </p>
                                            </div>
                                        </a>
                                        <a href="hr.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\hr1.png" width="70px" height="70px" />
                                                <p>HR Activities </p>
                                            </div>
                                        </a>
                                        <a href="Event_Site.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\EventSite1.png" width="70px" height="70px" />
                                                <p>Event Site </p>
                                            </div>
                                        </a>
                                        <a href="Meeting.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\Meeting1.png" width="70px" height="70px" />
                                                <p>Meeting / Briefings </p>
                                            </div>
                                        </a>
                                        <a href="Ass_tracking.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\Tracking1.png" width="70px" height="70px" />
                                                <p>Assets Tracking System</p>
                                            </div>
                                        </a>

                                    </div>
                                    <div class="row">

                                        <a href="kpis.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\kpis.png" width="70px" height="70px" />
                                                <p>KPI(For Manager/ Employee)</p>
                                            </div>
                                        </a>
                                               <a href="kpi.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\kpi1.png" width="70px" height="70px" />
                                                <p>KPI(Key Performance Indicator) for HR</p>
                                            </div>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>



                </jsp:body>

            </template:form>