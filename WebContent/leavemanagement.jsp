<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="template" tagdir="/WEB-INF/tags"%>
<template:form>
	<jsp:attribute name="headContent">
          <style>
.tab-content>.active {
	display: block;
	visibility: visible;
	margin-left: 20px !important;
	margin-right: 20px !important;
	text-align: justify;
	margin-top: 0px;
	padding-top: 20px;
}

label {
	margin-bottom: 0px;
	font-size: 14px;
	font-family: roboto;
}

input[type="checkbox"] {
	width: 15px;
	margin-left: 8px !important;
	margin-top: 8px;
}

label {
	display: inline-block;
	padding-left: 0px !important;
}

input {
	display: inline-block !important;
}

.form-control[disabled] {
	background: none !important
}

.form-control, select[multiple], textarea {
	border: 1px solid #ccc;
	background: none !important;
	box-shadow: 1px 1px 1px #ccc;
}

.dataTables_wrapper .dataTables_paginate .paginate_button {
	color: #000 !important
}

.dataTables_wrapper .dataTables_paginate .paginate_button .current {
	color: #000 !important
}

#directory::-webkit-scrollbar-track {
	-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
	border-radius: 10px;
	background-color: #F5F5F5;
}

#directory::-webkit-scrollbar {
	width: 12px;
	background-color: #F5F5F5;
}

#directory::-webkit-scrollbar-thumb {
	border-radius: 10px;
	-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
	background-color: #555;
}

.direc_con {
	overflow-y: scroll;
}

.dataTables_wrapper .dataTables_paginate .paginate_button.current,
	.dataTables_wrapper .dataTables_paginate .paginate_button.current:hover
	{
	width: 20px;
	height: 20px;
	padding-top: 2px;
	padding-left: 5px;
	background: #ccc !important;
	color: #000 !important;
	border: 0px;
	font-size: 12px !important;
	font-family: tahoma !important;
}

.dataTables_wrapper .dataTables_length, .dataTables_wrapper .dataTables_filter,
	.dataTables_wrapper .dataTables_info, .dataTables_wrapper .dataTables_processing,
	.dataTables_wrapper .dataTables_paginate {
	margin-right: 20px;
	color: #fff;
	margin-top: 20px;
}

.dataTables_wrapper .dataTables_paginate .paginate_button.disabled,
	.dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover,
	.dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active
	{
	cursor: pointer !important;
	color: #000 !important;
	border: 1px solid transparent;
	background: #ccc !important;
	box-shadow: none;
	padding-left: 2px;
	padding-right: 2px;
	padding-bottom: 2px;
	padding-top: 2px;
	font-size: 11px !important;
	font-family: tahoma !important;
}

select {
	background: #E0E0E0 !important;
}

input[type="search"] {
	background: #E0E0E0 !important;
}

label {
	color: #000 !important;
}

.form-control:focus, input:focus, select:focus, textarea:focus {
	border: 1px solid #ccc;
	box-shadow: 1px 1px 1px #ccc;
}

h2 {
	font-size: 16px;
	font-style: normal;
	font-family: roboto;
	color: #000 !important;
}

th {
	font-weight: 500 !important;
}

table.dataTable.no-footer {
	border-bottom: none !important
}
</style>
          <link href="assets/css/custom/jquery.dataTables.min.css"
			rel="stylesheet" type="text/css" />
          <link
			href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
			rel="stylesheet" type="text/css" />
         <link
			href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css"
			rel="Stylesheet" type="text/css" />
<link href="assets/css/custom/sweetalert.css" rel="stylesheet"
			type="text/css">
<link href="assets/css/custom/sweetalert.min.css" rel="stylesheet"
			type="text/css">
          <script src='assets/js/plugins/jquery/jquery.min.js'></script>
          <script type='text/javascript'
			src='assets/js/plugins/jquery/jquery.validate.js'></script>
          <script type='text/javascript'
			src='assets/js/plugins/bootstrap/bootstrap.min.js'></script>
           <script type='text/javascript'
			src='assets/js/custom/jquery.dataTables.min.js'></script>
           <script type='text/javascript'
			src='assets/js/custom/sweetalert.min.js'></script>
           <script type='text/javascript'
			src='assets/js/custom/sweetalert-dev.js'></script>
           <script type='text/javascript'
			src='assets/js/custom/sweetalert-dev.min.js'></script>
          <script type='text/javascript'
			src='assets/js/custom/Leave_Management.js'></script>
          
        
          <script type='text/javascript'
			src='assets/js/plugins/jquery/jquery-ui.min.js'></script>
          <script type="text/javascript"
			src='assets/js/custom/utility.js'></script>
                     <style>
label.error {
	color: rgb(244, 67, 54);
}

input.error {
	border: 1px solid rgb(244, 67, 54);
}

textarea.error {
	border: 1px solid rgb(244, 67, 54);
}
</style>
                    <script>
                    $(document).ready(function(){
                    	$('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
                    		localStorage.setItem('activeTab', $(e.target).attr('href'));
                    	});
                    	var activeTab = localStorage.getItem('activeTab');
                    	if(activeTab){
                    		$('#leaveTab a[href="' + activeTab + '"]').tab('show');
                    	}
                    });
                   /*  $(document).ready(function () {
                    	document.querySelector('#cancel').onclick = function () {
                    		swal({
                    			  title: "Are you sure?",
                    			  text: "Your want to Cancel it!",
                    			  type: "warning",
                    			  showCancelButton: true,
                    			  confirmButtonClass: "btn-danger",
                    			  confirmButtonText: "Yes,Cancel it!",
                    			  closeOnConfirm: false
                    			},
                    			function(){
                    			  swal("Deleted!", "Your request has been successfully Cancelled", "success");
                    			
                    			});
                    	}
                    }); */
                    </script>
        </jsp:attribute>
	<jsp:body>
          <div class="col-md-10 layout">
            <div class="row">
              <ul id="leaveTab" class="nav nav-tabs nav-justified">
                <li class="active"><a href="#Applyleave"
						data-toggle="tab"> Apply Leave</a> </li>
                <li class=""><a href="#MyLeave" data-toggle="tab">My Leave</a> </li>
                <li class=""><a href="#Entitlement"
						data-toggle="tab">Entitlement</a> </li>
                <li class=""><a href="#Reports" data-toggle="tab">My Task</a> </li>
              </ul>
              <div class="tab-content block block-drop-shadow">
                <div class="tab-pane fade active in" id="Applyleave">
                  <form action="/apleave" id="applyleave">
                    <div class="header form-header header-border">
                      <h2 class="text-color">Apply Leave</h2>
                    </div>
                    <div class="content controls bg-form content-border">
                      <div class="form-row">
                      <div style="display: none">
                      <c:if var="lb"
											test="${ not empty aplb.leaveDetails}">
                       <input type="text" class="form-control"
												id="casual" name="casual"
												value="<c:out value="${aplb.leaveDetails[ 'casual']} "></c:out>" />
                       <input type="text" class="form-control" id="sick"
												name="sick"
												value="<c:out value="${aplb.leaveDetails[ 'sick']} "></c:out>" />
                       <input type="text" class="form-control" id="paid"
												name="paid"
												value="<c:out value="${aplb.leaveDetails[ 'paid']} "></c:out>" />
                      </c:if>
                      </div>
                      
                      
                        <div class="col-md-3 text-black">Leave Type <span
											style="color: red">*</span> </div>
                        <div class="col-md-3"> <select
											name="leaveType" id="leaveType"
											class="form-control text-black">
                               <option value="">Select</option>
                               <option value="casual">Casual Leave</option>
                               <option value="paid">Earned Leave</option>
                               <option value="sick">Sick Leave</option>
                           </select> </div>
                      </div>
                      <div class="form-row">
                        <div class="col-md-3 text-black">Leave Balance <span
											style="color: red">*</span> </div>
                        <div class="col-md-3"> <input type="text"
											class="form-control text-black" id="leaveBalance"
											name="leaveBalance" disabled="true"> </div>
                      </div>
                      <div class="form-row">
                        <div class="col-md-3 text-black">From Date <span
											style="color: red">*</span> </div>

                        <div class="col-md-3"> <input type="date"
											id="fromdate" name="fromdate" onchange="enddate(this.value);"
											class="input-set form-control text-black" /> </div>
                      </div>
                      <div class="form-row">
                        <div class="col-md-3 text-black">To Date <span
											style="color: red">*</span> </div>
                        <div class="col-md-3"> <input type="date"
											id="todate" name="todate"
											class="input-set form-control text-black" /> </div>
                      </div>
                      <div class="form-row">
                        <div class="col-md-3 text-black">Comment <span
											style="color: red">*</span> </div>
                        <div class="col-md-3"> <textarea
											id="leaveMessage" name="leaveMessage"
											class="form-control text-black" row="10" col="100"></textarea> </div>
                      </div>
                      <div class="form-row" style="display: none">
                        <div class="col-md-3 text-color">Service<span
											style="color: red">*</span> </div>
                        <div class="col-md-3"> <input type="text"
											id="service" name="service" value="LeaveApplication"
											class="input-set text-black" /> </div>
                      </div>
                    </div>
                    <div class="footer bg-form footer-border"> <button
									type="submit" value="apl" name="apls"
									class="btn btn-default  btn-success">Submit</button> </div>
                  </form>
                </div>
                <div class="tab-pane fade " id="MyLeave">
                  <div class="header form-header header-border">
                    <h2 class="text-color">My Leave</h2>
                  </div>
               
                  <div class=" content controls bg-form content-border">
                    <div class="form-row">
                      <div class="col-md-3 text-black">From Date<span
										style="color: red">*</span> </div>
                      <div class="col-md-3"> <input type="date"
										id="fromdatee" name="fromdatee" onchange="enddatee(this.value);"
										class="input-set form-control text-black" /> </div>
                  
                      <div class="col-md-3 text-black">To Date<span
										style="color: red">*</span> </div>
                      <div class="col-md-3"> <input type="date"
										id="todatee" name="todatee" 
										class="input-set form-control text-black" /> </div>
                    </div>
                    <div class="form-row">
                      <div class=" col-md-3 text-black"> Show leave with Status<span
										style="color: red">*</span> </div>
                      <div class=" col-md-9" style="margin-top: 10px;">
                        <div
										style="padding-left: 0px; margin-top: -4px;"
										class="checkbox col-md-1">
										<label class="control control--checkbox  text-black"><span
											style="margin-left: 30px; font-size: 15px !important;">All</span> 
		                                                                   <input
											type="checkbox" class="check-box" name="status[]"
											id="status[]" value="All" />
		                                                                     <div
												class="control__indicator"></div>
	                                                       </label> </div>
                        <div style="padding-left: 0px;"
										class="checkbox col-md-2"> 
												<label class="control control--checkbox  text-black"><span
											style="margin-left: 30px; font-size: 15px !important;">Rejected</span> 
		                                                                   <input
											type="checkbox" class="check-box" name="status[]"
											id="status[]" value="Rejected" />
		                                                                     <div
												class="control__indicator"></div>
	                                                       </label>
											
											
											 </div>
                        <div style="padding-left: 0px;"
										class="checkbox col-md-2">  
											<label class="control control--checkbox  text-black"><span
											style="margin-left: 30px; font-size: 15px !important;">Cancelled</span> 
		                                                                   <input
											type="checkbox" class="check-box" name="status[]"
											id="status[]" value="Cancelled" />
		                                                                     <div
												class="control__indicator"></div>
	                                                       </label>
											
											
											</div>
											
											
                        <div style="padding-left: 0px;"
										class="checkbox col-md-3">
												<label class="control control--checkbox  text-black"><span
											style="margin-left: 30px; font-size: 15px !important;">Pending Approval</span> 
		                                                                   <input
											type="checkbox" class="check-box" name="status[]"
											id="status[]" value="Pending Approval" />
		                                                                     <div
												class="control__indicator"></div>
	                                                       </label>
											
											
											
											 </div>
                        <div style="padding-left: 0px;"
										class="checkbox col-md-2"> 
												<label class="control control--checkbox  text-black"><span
											style="margin-left: 30px; font-size: 15px !important;">Schedule</span> 
		                                                                   <input
											type="checkbox" class="check-box" name="status[]"
											id="status[]" value="Schedule" />
		                                                                     <div
												class="control__indicator"></div>
	                                                       </label>
											
											
											
											 </div>
                        <div style="padding-left: 0px;"
										class="checkbox col-md-1"> 
											
												<label class="control control--checkbox  text-black"><span
											style="margin-left: 30px; font-size: 15px !important;">Taken</span> 
		                                                                   <input
											type="checkbox" class="check-box" name="status[]"
											id="status[]" value="Taken" />
		                                                                     <div
												class="control__indicator"></div>
	                                                       </label>
											
											
											
											</div>
                      </div>
                    </div>
                  </div>
                  <div class="footer m-bottom-109 bg-form footer-border"> <button
								class="btn btn-default  btn-success">Submit</button> </div>
                 
                  <div class="row ">
                    <div class="block dtable_leave  box">
                      <div class="content data-table-body">
                        <table cellpadding="0" cellspacing="0"
										width="100%"
										class="table table-bordered table-striped sortable dataTable"
										id="myleave">
                          <thead>
                            <tr>
                              <th class="text-black">Date</th>
                              <th class="text-black">Leave Type</th>
                              <th class="text-black">Message / Leave Days / Total </th>
                              <th class="text-black">Status</th>
                              <th class="text-black">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <c:forEach var="userReq"
												items="${aplb.appliedleaveRequest}">
                              <tr>
                                <td>
                                  <c:out
															value="${userReq['ReqDateTime']}"></c:out>
                                </td>
                                <td>
                                  <c:out
															value="${userReq['Leave Type']}"></c:out>
                                </td>
                                <td>
                                <c:choose>
                                <c:when
																test="${userReq['Leave Type']=='casual' && userReq['ActionStatus']=='Pending By RM' || userReq['Leave Type']=='casual' && userReq['ActionStatus']=='Approved By RM'}">
                                    <c:out
																	value="Message: ${userReq['Message']} /"></c:out>
                                    <c:out
																	value=" (No.of Days : ${userReq['Days']}/${aplb.leaveDetails[ 'casual']})"></c:out>
                                </c:when>
                                <c:when
																test="${userReq['Leave Type']=='paid'  && userReq['ActionStatus']=='Pending By RM' || userReq['Leave Type']=='paid' && userReq['ActionStatus']=='Approved By RM'}">
                                    <c:out
																	value="Message: ${userReq['Message']} /"></c:out>
                                    <c:out
																	value=" (No.of Days : ${userReq['Days']}/${aplb.leaveDetails[ 'paid']})"></c:out>
                                </c:when>
                                <c:when
																test="${userReq['Leave Type']=='sick'  && userReq['ActionStatus']=='Pending By RM' || userReq['Leave Type']=='sick' && userReq['ActionStatus']=='Approved By RM'}">
                                    <c:out
																	value="Message: ${userReq['Message']} /"></c:out>
                                    <c:out
																	value=" (No.of Days : ${userReq['Days']}/${aplb.leaveDetails[ 'sick']})"></c:out>
                                </c:when>
                                <c:otherwise>
                                	<c:out
																	value="Message: ${userReq['Message']} /"></c:out>
                                    <c:out
																	value=" (No.of Days : ${userReq['Days']}/36)"></c:out>
                                </c:otherwise>
                                </c:choose>
                                </td>
                                <td>
                                  <c:out
															value="${userReq['ActionStatus']}"></c:out>
                                </td>
                                <c:choose>
                                  <c:when
															test="${userReq['ActionStatus']=='Cancelled'}">
                        <td><button type="button" class="btn edit"
																	disabled="disabled">Cancelled</button></td>
                                  </c:when>
                                  <c:when
															test="${userReq['ActionStatus']=='Pending By RM' || userReq['ActionStatus']=='Approved By RM' }">
                                    <td><button type="button"
																	class="btn edit" id="cancel"
																	onclick="cancelRequest(<c:out value=" ${userReq[ 'RequestID']} "></c:out>)"
																	value="${userReq['RequestID']}">Cancel</button></td>
                                  </c:when>
                                  <c:otherwise>
                                  	 <td><button type="button"
																	class="btn edit" disabled="disabled">Cancel</button></td>
                                  </c:otherwise>
                                </c:choose>
                              </tr>
                            </c:forEach>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade " id="Entitlement"> </div>
                <div class="tab-pane fade " id="Reports">
                  <div class="row">
                    <div class="block  box">
                    
                      <div class="content data-table-body">
                      
                        <table cellpadding="0" cellspacing="0"
										width="100%"
										class="table table-bordered table-striped sortable dataTable"
										id="request_table">
                          <thead>
                            <tr>
                              <th class="text-black">Date</th>
                              <th class="text-black">Employee Name</th>
                              <th class="text-black">Leave Type</th>
                              <th class="text-black">Leave Description</th>
                              <th class="text-black">Status </th>
                              <th class="text-black">Action</th>
                            </tr>
                          </thead>
                            <tbody>
                            <c:forEach var="role"
												items="${usersession.roles}">
                                <c:if test="${role=='ReportingManager'}">
                                  <c:forEach var="userReq"
														items="${aplb.managerResponse}">
                                    <tr>
                                      <td>
                                        <c:out
																	value="${userReq['ReqDateTime']}"></c:out>
                                      </td>
                                      <td>
                                        <c:out
																	value="${userReq['RequestOwner']}"></c:out>
                                      </td>
                                      <td>
                                        <c:out
																	value="${userReq['Leave Type']}"></c:out>
                                      </td>
                                      <td>
                                       <c:if
																	test="${ not empty userReq['Message']}">
                                       <c:out
																		value="Message: ${userReq['Message']} /"></c:out>
                                        <c:out
																		value=" (No.of Days : ${userReq['Days']}/18)"></c:out>
                                      </c:if>
                                      </td>
                                      <td>
                                        <c:out
																	value="${userReq['ActionStatus']}"></c:out>
                                      </td>
                                      <c:choose>
                                        <c:when
																	test="${userReq['Action']=='PendingByRM'}">
                                          <td><button type="button"
																			class="btn edit" id="PendingByRM"
																			onclick="userAction(<c:out value=" ${userReq[ 'RequestID']} "></c:out> ,'Accept')"
																			value="${userReq['RequestID']}">Approve</button>&nbsp; <button
																			type="button" class="btn edit"
																			onclick="userAction(<c:out value=" ${userReq[ 'RequestID']} "></c:out>,'Reject')"
																			value="${userReq['RequestID']}">Reject</button></td>
                                        </c:when>
                                         <c:when
																	test="${userReq['Action']=='RejectedByRM'}">
                                          <td><button type="button"
																			class="btn edit" id="RejectedByRM"
																			disabled="disabled" onclick="return false;"
																			value="${userReq['RequestID']}">Rejected</button></td>
                                        </c:when>
                                          <c:when
																	test="${userReq['Action']=='ApprovedByRM' && userReq['ActionStatus']=='Approved By RM' }">
                                           <td><button
																			type="button" class="btn edit" id="ApprovedByRM"
																			disabled="disabled" onclick="return false;"
																			value="${userReq['RequestID']}">Approved</button></td>
                                          </c:when>
                                          <c:when
																	test="${userReq['Action']=='ApprovedByRM' && userReq['ActionStatus']=='Cancelled' }">
                                           <td><button
																			type="button" class="btn edit" id="ApprovedByRM"
																			disabled="disabled" onclick="return false;"
																			value="${userReq['RequestID']}">Cancelled</button></td>
                                          </c:when>
                                           <c:when
																	test="${userReq['ActionStatus']=='Cancelled'}">
                                          <td><button type="button"
																			class="btn edit" id="Cancelled" disabled="disabled"
																			onclick="return false;"
																			value="${userReq['RequestID']}">Approve</button>&nbsp; <button
																			type="button" class="btn edit" disabled="disabled"
																			onclick="return false;"
																			value="${userReq['RequestID']}">Reject</button></td>
                                        </c:when>
                                        <c:otherwise>
                                        <td></td>
                                        </c:otherwise>
                                      </c:choose>
                                    </tr>
                                  </c:forEach>
                                </c:if>
                                <c:if test="${role=='HRManager'}">
                                  <c:forEach var="userReq"
														items="${aplb.hrResponses}">
                                    <tr>
                                      <td>
                                        <c:out
																	value="${userReq['ReqDateTime']}"></c:out>
                                      </td>
                                      <td>
                                        <c:out
																	value="${userReq['RequestOwner']}"></c:out>
                                      </td>
                                      <td>
                                        <c:out
																	value="${userReq['Leave Type']}"></c:out>
                                      </td>
                                      <td>
                                      <c:if
																	test="${ not empty userReq['Message']}">
                                       <c:out
																		value="Message: ${userReq['Message']} /"></c:out>
                                        <c:out
																		value=" (No.of Days : ${userReq['Days']}/18)"></c:out>
                                      </c:if>
                                      </td>
                                      <td>
                                        <c:out
																	value="${userReq['ActionStatus']}"></c:out>
                                      </td>
                                      <c:choose>
                                        <c:when
																	test="${userReq['RequestStatus']=='Approved By HR' || userReq['ActionStatus']=='Cancelled'}">
                                          <td><button type="button"
																			class="btn edit" disabled="disabled"
																			onclick="return false;"
																			value="${userReq['RequestID']}">Approve</button>&nbsp; <button
																			type="button" class="btn edit" disabled="disabled"
																			onclick="return false;"
																			value="${userReq['RequestID']}">Reject</button></td>
                                        </c:when>
                                        <c:when
																	test="${userReq['Action']=='ApprovedByRM'}">
                                          <td><button type="button"
																			class="btn edit"
																			onclick="userAction(<c:out value=" ${userReq[ 'RequestID']} "></c:out> ,'Accept')"
																			value="${userReq['RequestID']}">Approve</button>&nbsp; <button
																			type="button" class="btn edit"
																			onclick="userAction(<c:out value=" ${userReq[ 'RequestID']} "></c:out>,'Reject')"
																			value="${userReq['RequestID']}">Reject</button></td>
                                        </c:when>
                                       <c:when
																	test="${userReq['Action']=='RejectedByRM'}">
                                        <td><button type="button"
																			class="btn edit" disabled="disabled"
																			onclick="return false;"
																			value="${userReq['RequestID']}">Rejected</button></td>
                                        </c:when>
                                         <c:otherwise>
                                        <td></td>
                                        </c:otherwise>
                                      </c:choose>
                                    </tr>
                                  </c:forEach>
                                </c:if>
                              </c:forEach>
                            </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="direc_con" id="directory"
				style="display: none; border: 1px solid #000; top: -21px !important; left: 370px !important;">
                        <i class="fa fa-caret-up" aria-hidden="true"
					style="position: relative; left: 748px; font-size: 25px;"></i>
                        <div
					class="block block-transparent block-drop-shadow  hovershow"
					style="border-radius: 8px;">

                            <div class="container">
                                <div class="col-md-12  mydir">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <a href="#"> <img
										src="assets\img\turning_image\TeamC1.png" width="70px"
										height="70px" />
                                                <p>Team Collaboration </p>
                                        
								
								
								</div>
                                        </a>
                                        <a href="ijp.jsp">
                                            <div class="col-md-2">
                                                <img
											src="assets\img\turning_image\ijp1.png" width="70px"
											height="70px" />
                                                <p>IJP(Internal Job Posting) </p>
                                            </div>
                                        </a>
                                        <a href="Ticketing.jsp">
                                            <div class="col-md-2">
                                                <img
											src="assets\img\turning_image\helpDesk1.png" width="70px"
											height="70px" />
                                                <p>Help Desk Ticketing </p>
                                            </div>
                                        </a>
                                        <a href="Recognition.jsp">
                                            <div class="col-md-2">
                                                <img
											src="assets\img\turning_image\EmpRecognition1.png"
											width="70px" height="70px" />
                                                <p>Employee Recognition </p>
                                            </div>
                                        </a>
                                        <a href="Download_payslip.jsp">
                                            <div class="col-md-2">
                                                <img
											src="assets\img\turning_image\downloads.png" width="70px"
											height="70px" />
                                                <p>Download PaySlip</p>
                                            </div>
                                        </a>
                                        <a href="Payslip.jsp">
                                            <div class="col-md-2">
                                                <img
											src="assets\img\turning_image\payslip1.png" width="70px"
											height="70px" />
                                                <p>Generate Payslips </p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="row">

                                        <a href="Reimbursement.jsp">
                                            <div class="col-md-2">
                                                <img
											src="assets\img\turning_image\Reimbursement1.png"
											width="70px" height="70px" />
                                                <p>Reimbursement </p>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="col-md-2">
                                                <img
											src="assets\img\turning_image\teamM1.png" width="70px"
											height="70px" />
                                                <p>Team Management </p>
                                            </div>
                                        </a>
                                        <a href="hr.jsp">
                                            <div class="col-md-2">
                                                <img
											src="assets\img\turning_image\hr1.png" width="70px"
											height="70px" />
                                                <p>HR Activities </p>
                                            </div>
                                        </a>
                                        <a href="Event_Site.jsp">
                                            <div class="col-md-2">
                                                <img
											src="assets\img\turning_image\EventSite1.png" width="70px"
											height="70px" />
                                                <p>Event Site </p>
                                            </div>
                                        </a>
                                        <a href="Meeting.jsp">
                                            <div class="col-md-2">
                                                <img
											src="assets\img\turning_image\Meeting1.png" width="70px"
											height="70px" />
                                                <p>Meeting / Briefings </p>
                                            </div>
                                        </a>
                                        <a href="Ass_tracking.jsp">
                                            <div class="col-md-2">
                                                <img
											src="assets\img\turning_image\Tracking1.png" width="70px"
											height="70px" />
                                                <p>Assets Tracking System</p>
                                            </div>
                                        </a>

                                    </div>
                                    <div class="row">

                                        <a href="kpis.jsp">
                                            <div class="col-md-2">
                                                <img
											src="assets\img\turning_image\kpis.png" width="70px"
											height="70px" />
                                                <p>KPI(For Manager/ Employee)</p>
                                            </div>
                                        </a>
                                               <a href="kpi.jsp">
                                            <div class="col-md-2">
                                                <img
											src="assets\img\turning_image\kpi1.png" width="70px"
											height="70px" />
                                                <p>KPI(Key Performance Indicator) for HR</p>
                                            </div>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

        
	
	</jsp:body>
</template:form>