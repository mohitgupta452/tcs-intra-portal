<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="template" tagdir="/WEB-INF/tags"%>
<template:page>
	<jsp:attribute name="headContent">
<style>
.tab-content>.active {
	display: block;
	visibility: visible;
	margin-left: 20px !important;
	margin-right: 20px !important;
	text-align: justify;
	margin-top: 0px;
	padding-top: 20px;
}

label {
	margin-bottom: 0px;
	font-size: 14px;
	font-family: roboto;
}

input[type="checkbox"] {
	width: 15px;
	margin-left: 8px !important;
	margin-top: 8px;
}

label {
	display: inline-block;
	padding-left: 0px !important;
}

input {
	display: inline-block !important;
}

.form-control, select[multiple], textarea {
	background: #3f5360;
	border-color: transparent;
}
</style>
        
    </jsp:attribute>
	<jsp:body> 
            <div class="col-md-10 layout">
                <div class="row">
                    <div class="block block-drop-shadow"
					style="backgroud-color: #133959">
                        <ul id="myTab"
						class="nav nav-tabs nav-justified">
                            <li class=" ">
                                <a href="#employee-data"
							data-toggle="tab" aria-expanded="true">
                                    <div class="card-container">
                                        <div class="card">
                                            <div class="front">
                                                <img
												src="assets/img/turning_image/employee_data.png"
												class="employee_data" />


                                            </div>
                                            <div class="back">Employee Data
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class=" ">
                                <a href="#dtm" data-toggle="tab"
							aria-expanded="false">
                                    <div class="card-container">
                                        <div class="card">
                                            <div class="front">
                                                <img
												src="assets/img/turning_image/dtm.png" class="employee_data" />


                                            </div>
                                            <div class="back">DTM (Task Manager)
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="active">
                                <a href="#elm" data-toggle="tab"
							aria-expanded="false">
                                    <div class="card-container">
                                        <div class="card">
                                            <div class="front">
                                                <img
												src="assets/img/turning_image/leave.png"
												class="employee_data" />

                                            </div>
                                            <div class="back">ELM (Leave Management)
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class=" ">
                                <a href="#my-directory"
							data-toggle="tab" aria-expanded="false">
                                    <div class="card-container">
                                        <div class="card">
                                            <div class="front">
                                                <img
												src="assets/img/turning_image/Directory.ico"
												class="employee_data" />


                                            </div>
                                            <div class="back">My Directory</div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">



                    <ul id="leaveTab" class="nav nav-tabs nav-justified">
                        <li class="active"><a href="#Applyleave"
						data-toggle="tab"> Apply Leave</a>
                        </li>
                        <li class=""><a href="#MyLeave"
						data-toggle="tab">My Leave</a>
                        </li>
                        <li class=""><a href="#Entitlement"
						data-toggle="tab">Entitlement</a>
                        </li>
                        <li class=""><a href="#Reports"
						data-toggle="tab">Reports</a>
                        </li>
                    </ul>
 
 
                    <div class="tab-content block block-drop-shadow">
                      <div class="tab-pane fade active in"
						id="Applyleave">
                      <form action="/apleave">
                           <div class="header">
                        <h2>Apply Leave</h2>
                    </div>
                         <div class="content controls">
                          <div class="form-row">
                            <div class="col-md-3">Leave Type <span
											style="color: red">*</span>
									</div>
                            <div class="col-md-3">
                                 <select name="leaveType" id="leaveType"
											class="form-control">
                               <option type="select">Select</option>
                               <option type="casual">Casual Leave</option>
                               <option type="paid">Paid Leave</option>
                           </select>
                            </div>
                            </div>
                        
                        <div class="form-row">
                            <div class="col-md-3">Leave Balance <span
											style="color: red">*</span>
									</div>
                            <div class="col-md-3">
                           	<input type="text" class="form-control"
											id="leaveBalance" name="leaveBalance" disabled="true">
                            </div>
                        </div>
                          <div class="form-row">
                            <div class="col-md-3">From Date <span
											style="color: red">*</span>
									</div>
                            <div class="col-md-3">
                                 <input type="date" id="fromdate"
											name="fromdate" value="fromdate" class="input-set" />

                            </div>
                        </div>
                          <div class="form-row">
                            <div class="col-md-3">To Date <span
											style="color: red">*</span>
									</div>
                            <div class="col-md-3">
                                 <input type="date" id="todate"
											name="todate" value="todate" class="input-set" />

                            </div>
                        </div>
                         <div class="form-row">
                            <div class="col-md-3">Comment <span
											style="color: red">*</span>
									</div>
                            <div class="col-md-3">
                                 <textarea id="leaveMessage"
											name="leaveMessage" class="form-control" row="10" col="100"></textarea>

                            </div>
                        </div>
                    </div>
                           <div class="footer ">
                        <button type="submit" value="apl" name="apls"
									class="btn btn-default  btn-success">Apply</button>
                    </div> 
                    </form>
                        </div>
                         
                  
                      <div class="tab-pane fade " id="MyLeave">
                         <div class="header">
                        <h2>My Leave</h2>
                    </div>
                        <div class=" content controls">
                      <div class="form-row">
                            <div class="col-md-3">From Date<span
										style="color: red">*</span>
								</div>
                            <div class="col-md-3">
                                 <input type="date" id="fromdate"
										name="fromdate" value="fromdate" class="input-set" />

                            </div>
                        </div>
                          <div class="form-row">
                            <div class="col-md-3">To Date<span
										style="color: red">*</span>
								</div>
                            <div class="col-md-3">
                                 <input type="date" id="todate"
										name="todate" value="todate" class="input-set" />

                            </div>
                        </div>
                           
                                <div class="form-row">
                                    <div class=" col-md-3">
                                       Show leave with Status<span
										style="color: red">*</span>
								</div>
                                    <div class=" col-md-9">
                                        <div style="padding-left: 0px;"
										class="checkbox col-md-1">
										<label>All</label> <input type="checkbox" name="status"
											value="All">
									</div>
                                        <div style="padding-left: 0px;"
										class="checkbox col-md-2">
										<label>Rejected</label> <input type="checkbox" name="status"
											value="Rejected">
									</div>
                                        <div style="padding-left: 0px;"
										class="checkbox col-md-2">
										<label>Cancelled</label> <input type="checkbox" name="status"
											value="Cancelled">
									</div>
                                        <div style="padding-left: 0px;"
										class="checkbox col-md-3">
										<label>Pending Approval</label> <input type="checkbox"
											name="status" value="Pending Approval">
									</div>
                                        <div style="padding-left: 0px;"
										class="checkbox col-md-2">
										<label>Schedule</label> <input type="checkbox" name="status"
											value="Schedule">
									</div>
                                        <div style="padding-left: 0px;"
										class="checkbox col-md-1">
										<label>Taken</label> <input type="checkbox" name="status"
											value="Taken">
									</div>
                           


                                    </div>
									</div>
                                    </div>
                                        <div
							class="footer   m-bottom-109">

                        <button class="btn btn-default  btn-success">Apply</button>
                    </div> 
                    
                   
                                    <div class="row ">
                                        <div class="block dtable_leave">

                                            <div class="content">
                                                <table cellpadding="0"
										cellspacing="0" width="100%"
										class="table table-bordered table-striped sortable dataTable"
										id="myleave">

                                                    <thead>
                                                        <tr>
                                                            <th>Date</th>
                                                            <th>Leave Type</th>
                                                            <th>Message / Leave Days / Total </th>
                                                            <th>Status</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                       
                                                        <c:forEach var="userReq" items="${aplb.appliedleaveRequest}">
                                                         <tr>
                                                        <td><c:out value="${userReq['ReqDateTime']}"></c:out></td>
                                                        <td><c:out value="${userReq['Leave Type']}"></c:out></td>
                                                        <td><c:out value="Message: ${userReq['Message']} /"></c:out><c:out value=" (No.of Days : ${userReq['Days']}/18)"></c:out></td>
                                                        <td><c:out value="${userReq['ActionStatus']}"></c:out></td>
                                                        <td><button type="button"class="btn edit">Cancel</button></td>
                                                        </tr>
                                                        </c:forEach>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <button type="button"
									class="btn  btn-success apply">Save</button>
                                        </div>
						</div>
                                    </div>
                                </div>
   </div>
<div class="tab-pane fade " id="Entitlement">

hgfkuvfgkfvg

</div>
<div class="tab-pane fade " id="Reports">
jbhfbvhfvbfhg
</div>
</div>
</jsp:body>
</template:page>