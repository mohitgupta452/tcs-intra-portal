<%@ tag language="java" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@attribute name="headContent" fragment="true" required="true"%>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
<title>Turningcloud Solution</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<link rel="icon" type="image/ico" href="favicon.html" />

<link href="assets/css/stylesheets.css" rel="stylesheet" type="text/css" />
<link href="assets/css/custom/custom2.css" rel="stylesheet"
	type="text/css" />
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<!-- <script type='text/javascript'src='assets/js/plugins/jquery/jquery.min.js'></script>
<script type='text/javascript' src='assets/js/custom/custom.js'></script>
<script type='text/javascript'src='assets/js/plugins/jquery/jquery-ui.min.js'></script>
<script type='text/javascript'src='assets/js/plugins/jquery/jquery-migrate.min.js'></script>
<script type='text/javascript'src='assets/js/plugins/jquery/globalize.js'></script>
<script type='text/javascript'src='assets/js/plugins/bootstrap/bootstrap.min.js'></script>
<script type='text/javascript'src='assets/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js'></script>
<script type='text/javascript'src='assets/js/plugins/uniform/jquery.uniform.min.js'></script>
<script type='text/javascript'src='assets/js/plugins/knob/jquery.knob.js'></script>
<script type='text/javascript'src='assets/js/plugins/sparkline/jquery.sparkline.min.js'></script>
<script type='text/javascript'src='assets/js/plugins/flot/jquery.flot.js'></script>
<script type='text/javascript'src='assets/js/plugins/flot/jquery.flot.resize.js'></script>
<script type="text/javascript"src='assets/js/custom/utility.js'></script> -->





<jsp:invoke fragment="headContent" />

</head>
<body class="bg-img-num1 scroll-x" data-settings="open">

	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<nav class="navbar brb" role="navigation">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target=".navbar-ex1-collapse">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-reorder"></span>
						</button>
						<a class="navbar-brand" href="index.html"
							style="margin-top: 6px; font-family: fantasy !important; font-size: x-large;"><strong>CLOUD</strong>
							PORTAL</a>
					</div>
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<ul class="nav navbar-nav">
							<li class="active"><a href="index.jsp"> <span
									class="icon-home"></span> dashboard
							</a></li>
							<li class="dropdown" style="display: none"><a href="#"
								class="dropdown-toggle" data-toggle="dropdown"><span
									class="icon-pencil"></span> forms</a>
								<ul class="dropdown-menu">
									<li><a href="form_elements.html">Form elements</a></li>
									<li><a href="form_editors.html">WYSIWYG</a></li>
									<li><a href="form_files.html">File handling</a></li>
									<li><a href="form_validation.html">Validation and
											wizard</a></li>
								</ul></li>
							<li class="dropdown" style="display: none"><a href="#"
								class="dropdown-toggle" data-toggle="dropdown"><span
									class="icon-cogs"></span> components</a>
								<ul class="dropdown-menu">
									<li><a href="component_blocks.html">Blocks and panels</a></li>
									<li><a href="component_buttons.html">Buttons</a></li>
									<li><a href="component_modals.html">Modals and popups</a></li>
									<li><a href="component_tabs.html">Tabs, accordion,
											selectable, sortable</a></li>
									<li><a href="component_progress.html">Progressbars</a></li>
									<li><a href="component_lists.html">List groups</a></li>
									<li><a href="component_messages.html">Messages</a></li>
									<li><a href="#">Tables<i
											class="icon-angle-right pull-right"></i></a>
										<ul class="dropdown-submenu">
											<li><a href="component_table_default.html">Default
													tables</a></li>
											<li><a href="component_table_sortable.html">Sortable
													tables</a></li>
										</ul></li>
									<li><a href="#">Layouts<i
											class="icon-angle-right pull-right"></i></a>
										<ul class="dropdown-submenu">
											<li><a href="component_layout_blank.html">Default
													layout(blank)</a></li>
											<li><a href="component_layout_custom.html">Custom
													navigation</a></li>
											<li><a href="component_layout_scroll.html">Content
													scroll</a></li>
											<li><a href="component_layout_fixed.html">Fixed
													content</a></li>
											<li><a href="component_layout_white.html">White
													layout</a></li>
										</ul></li>
									<li><a href="component_charts.html">Charts</a></li>
									<li><a href="component_maps.html">Maps</a></li>
									<li><a href="component_typography.html">Typography</a></li>
									<li><a href="component_gallery.html">Gallery</a></li>
									<li><a href="component_calendar.html">Calendar</a></li>
									<li><a href="component_icons.html">Icons</a></li>
									<li><a href="component_portlet.html">Portlet</a></li>
								</ul></li>
							<li style="display: none"><a href="widgets.html"><span
									class="icon-globe"></span> widgets</a></li>
							<li class="dropdown" style="display: none"><a href="#"
								class="dropdown-toggle" data-toggle="dropdown"><span
									class="icon-file-alt"></span> pages</a>
								<ul class="dropdown-menu">
									<li><a href="sample_login.html">Login</a></li>
									<li><a href="sample_registration.html">Registration</a></li>
									<li><a href="sample_profile.html">User profile</a></li>
									<li><a href="sample_profile_social.html">Social
											profile</a></li>
									<li><a href="sample_edit_profile.html">Edit profile</a></li>
									<li><a href="sample_mail.html">Mail</a></li>
									<li><a href="sample_search.html">Search</a></li>
									<li><a href="sample_invoice.html">Invoice</a></li>
									<li><a href="sample_contacts.html">Contacts</a></li>
									<li><a href="sample_tasks.html">Tasks</a></li>
									<li><a href="sample_timeline.html">Timeline</a></li>
									<li><a href="#">Email templates<i
											class="icon-angle-right pull-right"></i></a>
										<ul class="dropdown-submenu">
											<li><a href="email_sample_1.html">Sample 1</a></li>
											<li><a href="email_sample_2.html">Sample 2</a></li>
											<li><a href="email_sample_3.html">Sample 3</a></li>
											<li><a href="email_sample_4.html">Sample 4</a></li>
										</ul></li>
									<li><a href="#">Error pages<i
											class="icon-angle-right pull-right"></i></a>
										<ul class="dropdown-submenu">
											<li><a href="sample_error_403.html">403 Forbidden</a></li>
											<li><a href="sample_error_404.html">404 Not Found</a></li>
											<li><a href="sample_error_500.html">500 Internal
													Server Error</a></li>
											<li><a href="sample_error_503.html">503 Service
													Unavailable</a></li>
											<li><a href="sample_error_504.html">504 Gateway
													Timeout</a></li>
										</ul></li>
								</ul></li>
							<li style="display: none"><a
								href="http://aqvatarius.com/themes/taurus_v12/front-end/index.html"><span
									class="icon-star"></span> Front-end Template</a></li>
						</ul>
						<form class="navbar-form navbar-right" role="search">
							<div class="form-group">
								<input type="text" class="form-control" style="background-color: #fff;
    border-bottom-left-radius: 25px;
    border-bottom-right-radius: 25px;
    border-top-left-radius: 25px;
    border-top-right-radius: 25px;" placeholder="search..." />

							</div>

						</form>
						<span><i class="fa fa-search" style="font-size: 16px;margin-right: 12px;position: relative;
    left: 978px;
    top: 10px" aria-hidden="true"></i></span>
					</div>
				</nav>

			</div>
		</div>
		<div class="row">

			<div class="col-md-2" style="margin-top:-10px">

				<div class="block block-drop-shadow">
					<div class="user bg-default bg-light-rtl">
						<div class="info">
							<a href="#" style="display: none" class="informer informer-three">
								<span></span> Offline
							</a>
							<c:choose>
								<c:when test="${sessionScope.userPicture !=null }">
									<a href="#" class="informer informer-four"> <span><img
											alt="" src="assets/img/online.jpg"></span> Online
									</a>
									<img src="${sessionScope.userPicture }"
										class="img-circle img-thumbnail" alt="">
								</c:when>
								<c:otherwise>
									<a href="#" class="informer informer-four"> <span><img
											alt="" src="assets/img/online.jpg"></span> Online
									</a>
									<img src="assets/img/user.jpg" class="img-circle img-thumbnail"
										alt="">
								</c:otherwise>
							</c:choose>


							<!--  <img src="img/example/user/dmitry_b.jpg"
								class="img-circle img-thumbnail" /> -->
						</div>
					</div>
					<div class="content list-group list-group-icons">
						<a href="component_messages.html" class="list-group-item"><span
							class="icon-envelope"></span>Messages<i
							class="icon-angle-right pull-right"></i></a> <a
							href="component_charts.html" class="list-group-item"><span
							class="icon-bar-chart"></span>Statistic<i
							class="icon-angle-right pull-right"></i></a> <a
							href="sample_profile.html" class="list-group-item"><span
							class="icon-cogs"></span>Settings<i
							class="icon-angle-right pull-right"></i></a> <a href="Logout"
							class="list-group-item"><span class="icon-off"></span>Logout<i
							class="icon-angle-right pull-right"></i></a>
					</div>
				</div>
				<!--This is to be extract  -->
				<div class="block block-drop-shadow">
					<div class="head bg-dot20">
						<h2>My Status</h2>
						<div class="side pull-right">
							<ul class="buttons">
								<li><a href="#"><span class="icon-cogs"></span></a></li>
							</ul>
						</div>
						<div class="head-subtitle">Performance Ratings</div>
						<div class="head-panel nm">
							<div class="hp-info hp-simple pull-left hp-inline">
								<span class="hp-main">Weekly Performance<span
									class="icon-angle-right"></span> 89%
								</span>
								<div class="hp-sm">
									<div class="progress progress-small">
										<div class="progress-bar progress-bar-danger"
											role="progressbar" aria-valuenow="89" aria-valuemin="0"
											aria-valuemax="100" style="width: 89%"></div>
									</div>
								</div>
							</div>
							<div class="hp-info hp-simple pull-left hp-inline">
								<span class="hp-main">Last Month Peformance<span
									class="icon-angle-right"></span> 56%
								</span>
								<div class="hp-sm">
									<div class="progress progress-small">
										<div class="progress-bar progress-bar-warning"
											role="progressbar" aria-valuenow="56" aria-valuemin="0"
											aria-valuemax="100" style="width: 56%"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- <div class="block block-drop-shadow" style="margin-top:-10px">
					<div class="head bg-dot20">
						<h2>Memory</h2>
						<div class="side pull-right">
							<ul class="buttons">
								<li><a href="#"><span class="icon-cogs"></span></a></li>
							</ul>
						</div>
						<div class="head-subtitle">Kingston 2x8Gb DDR3 1866MHz</div>
						<div class="head-panel nm tac">
							<div class="sparkline">
								<span sparkType="pie" sparkWidth="100" sparkHeight="100">5079,3768,7537</span>
							</div>
						</div>
						<div class="head-panel nm">
							<div class="hp-info hp-simple pull-left hp-inline">
								<span class="hp-main"><span class="icon-circle"></span>
									Allocated 5079 Mb [ 31% ]</span>
							</div>
							<div class="hp-info hp-simple pull-left hp-inline">
								<span class="hp-main"><span class="icon-circle text-info"></span>
									In Cache 3768 Mb [ 23% ]</span>
							</div>
							<div class="hp-info hp-simple pull-left hp-inline">
								<span class="hp-main"><span
									class="icon-circle text-primary"></span> Aviable 7537 Mb [ 46%
									]</span>
							</div>
						</div>
					</div>
				</div> -->
				<div class="block block-drop-shadow" style="margin-top:-10px">
					<div class="head bg-dot20" style="height:129px">
						<h2>Volumes status</h2>
						<div class="side pull-right">
							<ul class="buttons">
								<li><a href="#"><span class="icon-cogs"></span></a></li>
							</ul>
						</div>
						<div class="head-subtitle">WD Caviar Blue 1TB</div>
						<div class="head-panel nm tac" style="line-height: 0px;">
							<div class="knob">
								<input type="text" data-fgColor="#3F97FE" data-min="0"
									data-max="1024" data-width="100" data-height="100" value="654"
									data-readOnly="true" />
							</div>
						</div>
						<div class="head-panel nm">
							<div class="hp-info hp-simple pull-left hp-inline">
								<span class="hp-main">Volume 1 [ 0.5 TB ]</span> <span
									class="hp-sm">Used: 450.0 GB [ 90% ] </span> <span
									class="hp-sm">Free: 50.0 GB [ 10% ] </span>
							</div>
							<div class="hp-info hp-simple pull-left hp-inline">
								<span class="hp-main">Volume 2 [ 0.5 TB ]</span> <span
									class="hp-sm">Used: 154.0 GB [ 30% ] </span> <span
									class="hp-sm">Free: 346.0 GB [ 70% ] </span>
							</div>
						</div>
					</div>

				</div>

			</div>
			  <div class="col-md-10 layout" style="margin-top:-10px">
                <div class="row">
                    <div class="block block-drop-shadow"
					style="background-color: #546E7A;
    box-shadow: 0px 5px 10px #ccc;">
                        <ul id="myTab"
						class="nav nav-tabs nav-justified">
                            <li class=" ">
                                <a href="employeeData.jsp"
							data-toggle="tab" aria-expanded="true">
                                    <div class="card-container">
                                        <div class="card">
                                            <div class="front">
                                                <img
												src="assets/img/turning_image/employee_data.png"
												class="employee_data" />


                                            </div>
                                            <div class="back" onclick="location.reload();location.href='employeeData.jsp'">Employee Data
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class=" ">
                                <a href="#" data-toggle="tab"
							aria-expanded="false">
                                    <div class="card-container">
                                        <div class="card">
                                            <div class="front">
                                                <img
												src="assets/img/turning_image/dtm.png" class="employee_data" />


                                            </div>
                                            <div class="back">DTM (Task Manager)
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class=" ">
                                <a href="leavemanagement.jsp" data-toggle="tab"
							aria-expanded="false">
                                    <div class="card-container">
                                        <div class="card">
                                            <div class="front">
                                                <img
												src="assets/img/turning_image/leave.png"
												class="employee_data" />

                                            </div>
                                            <div class="back" onclick="location.reload();location.href='leavemanagement.jsp'">ELM (Leave Management)
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                              <li class=" ">
                                        <a href="#my-directory" data-toggle="tab" aria-expanded="false">
                                            <div class="card-container MyDirectoryOpen">
                                                <div class="card">
                                                    <div class="front">
                                                        <img src="assets/img/turning_image/Directory.ico" class="employee_data" />


                                                    </div>
                                                    <div class="back">My Directory</div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                        </ul>
                    </div>
                </div>
                </div>
			<!-- Do Body Paster Start-->
			<jsp:doBody />
			<!-- Do Body Paster End-->
		</div>
		<div class="row">
			<div class="page-footer">
				<div class="page-footer-wrap">
					<div class="side pull-left">Copyirght &COPY; YourCompany
						2013. All rights reserved.</div>
				</div>
			</div>
		</div>
	</div>
	<!--online chat -->
	<div
		style="max-width: 307px; position: fixed; bottom: 5px; right: 0px; border: 1px solid #ccc; background-color: darkslategrey; margin-right: 10px; overflow-y: scroll">
		<div class="block block-transparent block-drop-shadow" id="chat"
			style="display: none;">
			<div class="head bg-dot20 npb">
				<h2>Friends</h2>
				<div class="pull-right">
					<ul class="buttons">
						<li><a href="#">Show all</a></li>
					</ul>
				</div>
			</div>
			<div class="content np">
				<div class="list list-contacts">
					<a href="#" class="list-item">
						<div class="list-info">
							<img src="img/example/user/dmitry_s.jpg"
								class="img-circle img-thumbnail" />
						</div>
						<div class="list-text">
							<span class="list-text-name">John Doe</span>
							<div class="list-text-info">
								<i class="icon-map-marker"></i> Kyiv, Ukraine <i
									class="icon-comments"></i> <b>5</b>/142
							</div>
						</div>



						<div class="list-status list-status-offline"></div>
					</a> <a href="#" class="list-item">
						<div class="list-info">
							<img src="img/example/user/olga_s.jpg"
								class="img-circle img-thumbnail" />
						</div>
						<div class="list-text">
							<span class="list-text-name">Angelina Jolie</span>
							<div class="list-text-info">
								<i class="icon-map-marker"></i> Kyiv, Ukraine <i
									class="icon-comments"></i> <b>2</b>/251
							</div>
						</div>
						<div class="list-status list-status-online"></div>
					</a> <a href="#" class="list-item">
						<div class="list-info">
							<img src="img/example/user/alexey_s.jpg"
								class="img-circle img-thumbnail" />
						</div>
						<div class="list-text">
							<span class="list-text-name">Brad Pitt</span>
							<div class="list-text-info">
								<i class="icon-map-marker"></i> San Francisco, SA <i
									class="icon-comments"></i> 81
							</div>
						</div>
						<div class="list-status list-status-online"></div>
					</a> <a href="#" class="list-item">
						<div class="list-info">
							<img src="img/example/user/helen_s.jpg"
								class="img-circle img-thumbnail" />
						</div>
						<div class="list-text">
							<span class="list-text-name">Keira Knightley</span>
							<div class="list-text-info">
								<i class="icon-map-marker"></i> London, Great Britan <i
									class="icon-comments"></i> 51
							</div>
						</div>
						<div class="list-status list-status-away"></div>
					</a>
				</div>
			</div>
		</div>
		<div
			style="max-width: 307px; position: fixed; bottom: 5px; right: 5px; border: 1px solid green; margin-right: 5px;border-radius: 8px;">
			<button type="submit" class="btn btn-success btn-block" id="online">online
				chat</button>
		</div>
	</div>
</body>

</html>