<%@ tag language="java" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@attribute name="headContent" fragment="true" required="true"%>
<!DOCTYPE html>
<html lang="en">
<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>      
    <title>Turningcloud Solutions</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <link rel="icon" type="image/ico" href="favicon.html"/>
    
    <link href="assets/css/stylesheets.css" rel="stylesheet" type="text/css" />        
    
    <script type='text/javascript' src='assets/js/plugins/jquery/jquery.min.js'></script>
    <script type='text/javascript' src='assets/js/plugins/jquery/jquery-ui.min.js'></script>   
    <script type='text/javascript' src='assets/js/plugins/jquery/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='assets/js/plugins/jquery/globalize.js'></script>    
    <script type='text/javascript' src='assets/js/plugins/bootstrap/bootstrap.min.js'></script>
    
    <script type='text/javascript' src='assets/js/plugins/uniform/jquery.uniform.min.js'></script>
    
    <script type='text/javascript' src='assets/js/plugins.js'></script>    
    <script type='text/javascript' src='assets/js/actions.js'></script>    
    <script type='text/javascript' src='assets/js/settings.js'></script>
     <jsp:invoke fragment="headContent" />
</head>
<body class="bg-img-num1"> 
    <form class="form-vertical login-form" action="loginServlet" method="post">
    <div class="container">
    
    <!--Do Body  -->
    <jsp:doBody />
     </div>
</form>
</body>
</html>
    