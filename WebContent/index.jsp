<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="template" tagdir="/WEB-INF/tags"%>
<template:form>
	<jsp:attribute name="headContent">
	   <link href="assets/css/custom/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/custom/employeeData.css" rel="stylesheet" type="text/css" />
                  
    <script src='assets/js/plugins/jquery/jquery.min.js'></script>
    <script type='text/javascript' src='assets/js/plugins/jquery/jquery.validate.js'></script>
    <script type='text/javascript' src='assets/js/plugins/bootstrap/bootstrap.min.js'></script>
      <script type='text/javascript' src='assets/js/custom/index.js'></script>
    <script type='text/javascript' src='assets/js/custom/utility.js'></script>
    <script type='text/javascript' src='assets/js/plugins/jquery/jquery-ui.min.js'></script>
    <script type='text/javascript' src='assets/js/custom/jquery.dataTables.min.js'></script>
    <style>
        #directory::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	border-radius: 10px;
	background-color: #F5F5F5;
}

#directory::-webkit-scrollbar
{
	width: 12px;
	background-color: #F5F5F5;
}

#directory::-webkit-scrollbar-thumb
{
	border-radius: 10px;
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
	background-color: #555;
}
    .direc_con
{
	
	
	overflow-y: scroll;
	
}

    
    </style>
    </jsp:attribute>
	<jsp:body>     
            <div class="col-md-10 layout">
                
                <!--<div class="block block-drop-shadow">
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade" id="employee-data">
                            <h4 style="color:black">Employee data</h4>
                            <p style="color:black">Portals and Web 2.0 based Applications are where 'Synergy' stops being a buzzword and becomes a tangible business benefit. Portals allow employees and customers to search and access corporate information and are a user-friendly
                                and effective means of sharing information and data. At PiTech Management, we have proven experience in mining the information from disparate systems and connecting different stakeholders in an enterprise by developing
                                tailor made portals, thereby helping the enterprise reap the maximum benefits out of their information systems</p>
                        </div>
                        <div class="tab-pane fade" id="dtm">
                            <h4 style="color:black">DTM(Task Manager)</h4>
                            <p style="color:black">We design and develop innovative Business Solutions to make our Customers stand out in the marketplace. Whether it is an Enterprise Business Portal or a Supply-Chain application, we have the domain knowledge and technical expertise
                                to deliver robust, secure & scalable applications that allow you to meet your aggressive time-to-market goals. Our Application development teams have developed and delivered value added Applications for their customers
                                and transformed their businesses.</p>
                        </div>
                        <div class="tab-pane fade" id="elm">
                            <h4 style="color:black">ELM(leave Management)</h4>
                            <p style="color:black">Enterprise applications are the âglueâ that holds an organization together. Our Application Management Services provide the scalability and technical expertise to reliably deliver and mange application availability, security
                                and performance, as an alternative to providing this support using internal resources.</p>
                        </div>
                        <div class="tab-pane fade" id="my-directory">
                            <h4 style="color:black">My Directory</h4>
                            <p style="color:black">Enterprise applications are the âglueâ that holds an organization together. Our Application Management Services provide the scalability and technical expertise to reliably deliver and mange application availability, security
                                and performance, as an alternative to providing this support using internal resources.</p>
                        </div>

                    </div>

                </div>-->
                <div class="row">
                    <div class="block" style="margin-top:-10px">
                        <div class="head head1">
                            <h2 >Create A Post</h2>
                            
                        </div>
                        <div class="content controls  bg-color-white">
                            <div class="form-row">
                                <div class="col-md-12" style="margin-top: 25px">
                                    <textarea class="form-control index-textarea"
									placeholder="Message..."></textarea>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-10">
                                    <button
									class="btn btn-primary btn-clean">
									<i class="icon-camera"></i>
								</button>
                                    <button
									class="btn btn-primary btn-clean">
									<i class="icon-link"></i>
								</button>
                                    <button
									class="btn btn-primary btn-clean">
									<i class="icon-folder-close"></i>
								</button>
                                </div>
                                <div class="col-md-2">
                                    <button
									class="btn btn-primary btn-clean btn-block">Send</button>
                                </div>
                            </div>
                        </div>
                      <div class="col-md-12">
                      <div class="row">
                      <div class="col-md-4 bg-dot20 h-175" style="margin-left:5px !important;margin-right:5px !important">
                      </div>
                       <div class="col-md-4 bg-dot20 h-175" style="margin-left:5px !important;margin-right:5px !important">
                      </div>
                       <div class="col-md-4 bg-dot20 h-175" style="margin-left:5px !important;margin-right:5px !important">
                      </div>
                      </div>
                      </div>
                      
                      
                      
                        <div class="footer bg-dot20 tac">
                            <a href="#"></a>
                        </div>
                    </div>

                </div>
            </div>
            
          
            
              <!-- mydirectory -->
                         <div class="direc_con" id="directory" style="display: none;border:1px solid #000">
                        <i class="fa fa-caret-up" aria-hidden="true" style="position: relative;left: 748px;font-size: 25px;"></i>
                        <div class="block block-transparent block-drop-shadow  hovershow" style="border-radius: 8px;">

                            <div class="container">
                                <div class="col-md-12  mydir">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <a href="#"> <img src="assets\img\turning_image\TeamC1.png" width="70px" height="70px" />
                                                <p>Team Collaboration </p>
                                        </div>
                                        </a>
                                        <a href="ijp.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\ijp1.png" width="70px" height="70px" />
                                                <p>IJP(Internal Job Posting) </p>
                                            </div>
                                        </a>
                                        <a href="Ticketing.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\helpDesk1.png" width="70px" height="70px" />
                                                <p>Help Desk Ticketing </p>
                                            </div>
                                        </a>
                                        <a href="Recognition.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\EmpRecognition1.png" width="70px" height="70px" />
                                                <p>Employee Recognition </p>
                                            </div>
                                        </a>
                                        <a href="Download_payslip.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\downloads.png" width="70px" height="70px" />
                                                <p>Download PaySlip</p>
                                            </div>
                                        </a>
                                        <a href="Payslip.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\payslip1.png" width="70px" height="70px" />
                                                <p>Generate Payslips </p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="row">

                                        <a href="Reimbursement.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\Reimbursement1.png" width="70px" height="70px" />
                                                <p>Reimbursement </p>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\teamM1.png" width="70px" height="70px" />
                                                <p>Team Management </p>
                                            </div>
                                        </a>
                                        <a href="hr.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\hr1.png" width="70px" height="70px" />
                                                <p>HR Activities </p>
                                            </div>
                                        </a>
                                        <a href="Event_Site.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\EventSite1.png" width="70px" height="70px" />
                                                <p>Event Site </p>
                                            </div>
                                        </a>
                                        <a href="Meeting.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\Meeting1.png" width="70px" height="70px" />
                                                <p>Meeting / Briefings </p>
                                            </div>
                                        </a>
                                        <a href="Ass_tracking.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\Tracking1.png" width="70px" height="70px" />
                                                <p>Assets Tracking System</p>
                                            </div>
                                        </a>

                                    </div>
                                    <div class="row">

                                        <a href="kpis.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\kpis.png" width="70px" height="70px" />
                                                <p>KPI(For Manager/ Employee)</p>
                                            </div>
                                        </a>
                                               <a href="kpi.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\kpi1.png" width="70px" height="70px" />
                                                <p>KPI(Key Performance Indicator) for HR</p>
                                            </div>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

 </jsp:body>
</template:form>