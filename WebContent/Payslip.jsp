<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="template" tagdir="/WEB-INF/tags"%>
<template:form>
	<jsp:attribute name="headContent">
          <link href="assets/css/custom/Payslip.css" rel="stylesheet">
          <script type='text/javascript'
			src='assets/js/plugins/jquery/jquery.min.js'>
											
										</script>
          <script type='text/javascript'
			src='assets/js/plugins/jquery/jquery.validate.js'>
											
										</script>
          <script type='text/javascript'
			src='assets/js/plugins/bootstrap/bootstrap.min.js'>
											
										</script>
										
						    <script type='text/javascript' src='assets/js/custom/sweetalert.min.js'></script>
           <script type='text/javascript'
			src='assets/js/custom/sweetalert-dev.js'></script>
           <script type='text/javascript'
			src='assets/js/custom/sweetalert-dev.min.js'></script>				
          <script type='text/javascript'
			src='assets/js/custom/payslip.js'>
											
										</script>
          <script type='text/javascript'
			src='assets/js/plugins/jquery/jquery-ui.min.js'>
											
										</script>
          <script type='text/javascript'>
											$(function() {
												$('#online')
														.click(
																function() {
																	if ($(
																			'#chat')
																			.css(
																					'display') == "block") {
																		$(
																				'#chat')
																				.css(
																						'display',
																						'none');
																	} else {
																		$(
																				'#chat')
																				.css(
																						'display',
																						'block');
																	}
																});
											});



											/* $(document).ready(function () {
												document.querySelector('#edetailsGen').onclick = function () {
													swal( "Your request Has been successfully Submitted!", "success")
												}
											}); */
										</script>
										

          <style>
  label.error {
    color: rgb(244,67,54);
}
                        
                        input.error {
                            border: 1px solid  rgb(244,67,54);
                        }
                        
                        textarea.error {
                            border: 1px solid  rgb(244,67,54);
                        }

.legend_border {
	border: 1px solid #999;
	    box-shadow: 1px 1px 1px #ccc;
}

.form-row:first-child {
	margin-top: 0px !important;
}
    #directory::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	border-radius: 10px;
	background-color: #F5F5F5;
}

#directory::-webkit-scrollbar
{
	width: 12px;
	  border-radius: 25px;
	background-color: #F5F5F5;
}

#directory::-webkit-scrollbar-thumb
{
	border-radius: 10px;
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
	background-color: #555;
}
    .direc_con
{
	
	
	overflow-y: scroll;
	
}
.button{    padding-left: 30px;
    padding-right: 30px;
    background: #00838F;
    color: white;}
label
{font-weight:500 !important;}
.text-black{color:#000 !important}
.text-black{color:#000 !important}

.btn:hover{background:}

</style>
        </jsp:attribute>
	<jsp:body>
          <div class="col-md-10 layout">
            <div class="row">
              <div class="block block-drop-shadow">
                     <form role="form" action="Payslip" id="Emp_info" method ="post">
                <div class="content controls legend_border bg-form">
                  <p class="payslip_para1">  Employee Search Option
                  </p>
                  <div class="form-row">
                    <div class="col-md-2 text-black">
                      Employee Code:
                    </div>
                    <div class="col-md-2">
                      <select class="form-control modal_select m-pad-0 text-black"
									name="Emp_code" id="Emp_code" required>                                    
                        <option value="">select
                        </option>
                        <c:forEach var="empCode"
										items="${eData.empCode}">
                          <option
											value="<c:out value="${empCode}"></c:out>">
                            <c:out value="${empCode}">
                            </c:out>
                          </option>
                        </c:forEach>
                      </select>
                    </div>
                    <div class="col-md-2 text-black">
                      Employee Name:
                    </div>
                    <div class="col-md-2">
                      <input type="text" class="form-control text-black"
									name="emp_Name" id="emp_Name"
									onkeypress="return onlyAlphabets(event,this);" required />
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="col-md-2 text-black">
                      Month<span style="color:red">*</span>
                    </div>
                    <div class="col-md-2">
                      <select class="form-control modal_select m-pad-0 text-black"
									name="months" id="months" required>                                    
                        <option value="">select
                        </option>
                        <option value="January">January
                        </option>
                        <option value="February">February
                        </option>
                        <option value="March">March
                        </option>
                        <option value="April">April
                        </option>
                        <option value="May">May
                        </option>
                        <option value="June">June
                        </option>
                        <option value="July">July
                        </option>
                        <option value="August">August
                        </option>
                        <option value="September">September
                        </option>
                        <option value="October">October
                        </option>
                        <option value="November">November
                        </option>
                        <option value="December">December
                        </option>
                      </select>
                    </div>
                    <div class="col-md-2 text-black">
                      Year<span style="color:red">*</span>
                    </div>
                    <div class="col-md-2">
                      <select class="form-control modal_select m-pad-0 text-black"
									name="year" id="year" required> 
                        <option value="">select
                        </option>
                        <option value="2018">2018
                        </option>
                        <option value="2017">2017
                        </option>
                        <option value="2016">2016
                        </option>
                        <option value="2015">2015
                        </option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <button type="submit" class="btn btn-primary"
									style="padding-left: 30px; padding-right: 30px; display: none"
									disabled>Calculate Again
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <ul id="leaveTab" class="nav nav-tabs nav-justified"
					style="width: 34% !important">
                <li class="active">
                  <a href="#Emp_information" data-toggle="tab">Employee Information
                  </a> 
                </li>
                <li class="">
                  <a href="#Salary_details" data-toggle="tab">Salary Details
                  </a> 
                </li>
              </ul>
              <div class="tab-content block block-drop-shadow">
                <div class="tab-pane fade active in"
						id="Emp_information">
                  <div class="content controls legend_border bg-form">
                    <p class="payslip_para1" style="width: 125px">Employee Information
                    </p>
             
                      <div class="form-row">
                        <div class="col-md-3" style="display: none">
                          <input type="text" class="form-control"
											name="edi" id="edi" />
                        </div>
                        <div class="col-md-3 text-black">
                          First Name :
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control text-black"
											name="first_Name" id="first_Name"
											onkeypress="return onlyAlphabets(event,this);" disabled="disabled" required />
                        </div>
                        <div class="col-md-3 text-black">
                          Last Name:
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control text-black"
											name="last_Name" id="last_Name"
											onkeypress="return onlyAlphabets(event,this);" disabled="disabled" required />
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col-md-3 text-black">
                          Mobile Number:
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control text-black"
											name="mobile" id="mobile" maxlength="10"
											onkeypress="return isNumber(event,this);" required />
                        </div>
                        <div class="col-md-3 text-black">
                          EmailId:
                        </div>
                        <div class="col-md-3">
                          <input type="email" class="form-control text-black"
											name="emailid" id="emailid" disabled="disabled" required />
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col-md-3 text-black">
                          Current Address:
                        </div>
                        <div class="col-md-3">
                          <textarea row="10" col="100"
											class="form-control text-black" name="current_Address"
											id="current_Address">
                          </textarea>
                        </div>
                        <div class="col-md-3 text-black">
                          Permanent Address:
                        </div>
                        <div class="col-md-3">
                          <textarea row="10" col="100"
											class="form-control text-black" name="permanent_Address"
											id="permanent_Address">
                          </textarea>
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col-md-3 text-black">
                          Zip/Postal Code:
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control text-black"
											name="Postal_code" id="Postal_code"
											onkeypress="return isNumber(event,this);" maxlength="6"
											required />
                        </div>
                        <div class="col-md-3 text-black">
                          City:
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control text-black"
											name="city" id="city" required />
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col-md-3 text-black">
                          State:
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control text-black"
											name="state" id="state" required />
                        </div>
                        <div class="col-md-3 text-black">
                          Country:
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control text-black"
											name="country" id="country" required />
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col-md-2 col-md-offset-4">
                          <button type="submit" class="btn btn-primary"
											id="edetailsSub" name="edetailsSub" value="eDetailSub"
											style="padding-left: 30px; padding-right: 30px; display: none">Save
                          </button>
                        </div>
                        <div class="col-md-2">
                          <button type="print" class="btn btn-primary"
											style="padding-left: 30px; padding-right: 30px; display: none">Print
                          </button>
                        </div>
                      </div>
               
                  </div>
                </div>
                <div class="tab-pane fade" id="Salary_details">
                  <div class="content controls legend_border bg-form">
                    <p class="payslip_para1" style="width: 105px">Salary Information
                    </p>
                    <form action="Payslip">
                      <div class="form-row l-height">
                        <div class="col-md-10">
                          <div class="legend_border"
											style="padding-bottom: 50px">
                            <p class="payslip_para1 text-color" style="width: 68px">Attendence
                            </p>
                            <div class="col-md-2 m-top-20">
	                            <div class="col-md-3" style="display: none">
			                          <input type="text" class="form-control text-black"
														name="sedi" id="sedi" />
			                        </div>
			                        <div class="col-md-3" style="display: none">
			                          <input type="text" class="form-control text-black"
														name="years" id="years" />
			                        </div>
			                        
			                        <div class="col-md-3" style="display: none">
			                          <input type="text" class="form-control text-black"
														name="month" id="month" />
			                        </div>
			                        
			                         <div class="col-md-3" style="display: none">
			                          <input type="text" class="form-control text-black"
														name="gMonthlySalary" id="gMonthlySalary" />
			                        </div>
                              <label class="text-black">
                                Working Days:
                              </label>
                              <input type="text" class="form-control text-black"
													name="workingDays" id="workingDays" />
                            </div>
                            <div class="col-md-1 m-top-20">
                              <label class="text-black">EL:
                              </label>
                              <input type="text" class="form-control text-black"
													name="earnedleave" id="el" />
                            </div>
                            <div class="col-md-1 m-top-20">
                              <label class="text-black">SL:
                              </label>
                              <input type="text" class="form-control text-black"
													name="sickleave" id="sl" />
                            </div>
                            <div class="col-md-1 m-top-20">
                              <label class="text-black">CL:
                              </label>
                              <input type="text" class="form-control text-black"
													name="casualLeave" id="cl" />
                            </div>
                            <div class="col-md-2 m-top-20">
                              <label class="text-black">Holidays:
                              </label>
                              <input type="number" class="form-control text-black"
													name="holidays" id="holidays" />
                            </div>
                            <div class="col-md-2 m-top-20">
                              <label class="text-black">Absent:
                              </label>
                              <input type="text" class="form-control text-black"
													name="absent" id="absent" />
                            </div>
                            <div class="col-md-1 m-top-20">
                              <label class="text-black">PH:
                              </label>
                              <input type="text" class="form-control text-black"
													name="paidHours" id="ph" />
                            </div>
                            <div class="col-md-2 m-top-20">
                              <label class="text-black">Months Days:
                              </label>
                              <input type="text" class="form-control text-black"
													name="monthDays" id="monthDays" />
                            </div>
                          </div>
                        </div>
                        <div class="col-md-2 ">
                          <div class="legend_border"
											style="padding-bottom: 50px">
                            <p class="payslip_para1 text-color l-height"
												style="width: 102px">PF Range Details
                            </p>
                            <div class="col-md-6 m-top-20">
                              <label class="text-black">PF:
                              </label>
                              <input type="text" class="form-control text-black"
													name="pf" id="pf" />
                            </div>
                            <div class="col-md-6 m-top-20">
                              <label class="text-black">ESI:
                              </label>
                              <input type="text" class="form-control text-black"
													name="esi" id="esi" />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-row l-height">
                        <div class="col-md-5">
                          <div class="legend_border"
											style="padding-bottom: 61px">
                            <p class="payslip_para1 text-color"
												style="width: 126px">Earning-Basic Details
                            </p>
                            <div class="col-md-6 m-top-20">
                              <label class="text-black">Daily:
                              </label>
                              <input type="text" class="form-control text-black"
													name="daily" id="daily" />
                            </div>
                            <div class="col-md-6 m-top-20">
                              <label class="text-black">Monthly:
                              </label>
                              <input type="text" class="form-control text-black"
													name="monthly" id="monthly" />
                            </div>
                            <div class="legend_border"
												style="margin-top: 72px; margin-left: 20px; margin-right: 20px; margin-bottom: 20px; padding-bottom: 6px;">
                              <p class="payslip_para1 text-color l-height"
													style="width: 70px">Allowances
                              </p>
                              <div
													style="margin-left: 20px; margin-right: 20px"
													id="AddAllowance0">
                                <div class="col-md-6">
                                  <select
															class="form-control modal_select m-pad-0 text-black"
															name="allowance0" id="allowance0" required> 
                                    <option value="">select
                                    </option>
                                    <option value="mReim">Medical Reimbursement
                                    </option>
                                    <option value="tReim">Telephone Reimbursement
                                    </option>
                                    <option value="nshiftAllow">Night Shift Allowance
                                    </option>
                                    <option value="nPerPayout">Notice Period Payout
                                    </option>
                                      <option value="bonus">Bonus
                                    </option>
                                  </select>
                                </div>
                                <div class="col-md-6">
                                  <input id="allowanceAmt0" type="text"
															class="form-control text-black"  />
                                </div>
                                <a href="javascript:void(0)" id="add"
														style="margin-left: 210px" onclick="addFunction()">+ Add Allowance
                                </a>
                              </div>
                              <div id="ExtraDiv">
                              </div>
                              <div class="col-md-2 col-md-offset-6"
													style="margin-top: 51px">
                                Total:
                              </div>
                              <div class="col-md-4 m-top-20"
													style="margin-top: 47px">
                                <input type="text" class="form-control text-black" />
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-7">
                          <div class="form-row l-height">
                            <div class="legend_border"
												style="padding-bottom: 50px">
                              <p class="payslip_para1 "
													style="width: 103px">PF/ESI Deduction
                              </p>
                              <div class="col-md-3 m-top-20">
                                <label class="text-black">PF:
                                </label>
                                <input type="text" class="form-control text-black"
														name="pfd" id="pfd" />
                              </div>
                              <div class="col-md-2 m-top-20">
                                <label class="text-black">EPF:
                                </label>
                                <input type="text" class="form-control text-black"
														name="epfd" id="epfd" />
                              </div>
                              <div class="col-md-2 m-top-20">
                                <label class="text-black">EPS:
                                </label>
                                <input type="text" class="form-control text-black"
														name="epsd" id="epsd" />
                              </div>
                              <div class="col-md-3 m-top-20">
                                <label class="text-black">ESI:
                                </label>
                                <input type="text" class="form-control text-black"
														name="esid" id="esid" />
                              </div>
                              <div class="col-md-2 m-top-20">
                                <label class="text-black">ESIC:
                                </label>
                                <input type="text" class="form-control text-black"
														name="esicd" id="esicd" />
                              </div>
                            </div>
                          </div>  
                          <div class="form-row l-height">
                            <div class="col-md-8 ">
                              <div class="legend_border"
													style="padding-bottom: 90px;">
                                <p class="payslip_para1 text-color"
														style="width: 129px">Deduction-Allowances
                                </p>
                                <div
														style="margin-left: 20px; margin-right: 20px"
														id="DedAllowance0">
                                  <div class="col-md-6">
                                    <select
																class="form-control modal_select m-pad-0 text-black"
																name="Dallowance0" id="Dallowance0" required> 
                                      <option value="">select
                                      </option>
                                      <option value="pfund">Provident Fund
                                      </option>
                                      <option value="pTax">Professional Tax
                                      </option>
                                      <option value="lRecovery">Lunch Recovery
                                      </option>
                                      <option value="tRecovery">Transport Recovery
                                      </option>
                                       <option value="iTax">Income Tax
                                      </option>
                                    </select>
                                  </div>
                                  <div class="col-md-6">
                                    <input id="DallowanceAmt0"
																type="text" class="form-control text-black" />
                                  </div>
                                  <a href="javascript:void(0)" id="addition"
															style="margin-left: 210px" onclick="addDecFunction()">+ Add Allowance
                                  </a>
                                </div>
                                <div id="ExtraDivv">
                                </div>
                                <div class="col-md-2 col-md-offset-6 text-black"
														style="margin-top: 50px">
                                  Total:
                                </div>
                                <div class="col-md-4 m-top-20"
														style="margin-top: 47px">
                                  <input type="text"
															class="form-control text-black" />
                                </div>
                              </div>
                            </div>
                            <div class="col-md-4 ">
                              <div class="form-row l-height" >
                                <div class="legend_border"
														style="padding-bottom: 10px;">
                                  <p class="payslip_para1 "
															style="width: 105px">GLWF Calculation
                                  </p>
                                  <div class="m-top-20">
                                    <label
																style="width: 90%; margin-left: 9px; margin-right: 10px;" class="text-black">GLWF:
                                    </label>
                                    <input type="text"
																class="form-control text-black" name="glwf" id="glwf"
																style="width: 90%; margin-left: 9px; margin-right: 10px;" />
                                  </div>
                                </div>
                              </div>
                              <div class="form-row l-height" style="margin-top:8px;">
                                <div class="legend_border"
														style="padding-bottom: 10px;">
                                  <p class="payslip_para1 "
															style="width: 65px">Net Salary
                                  </p>
                                  <div class="m-top-20">
                                    <label
																style="width: 90%; margin-left: 9px; margin-right: 10px;" class="text-black">Net Pay:
                                    </label>
                                    <input type="text"
																class="form-control text-black" name="netSalary" id="netSalary"
																style="width: 90%; margin-left: 9px; margin-right: 10px;" />
									 <input type="text"
																class="form-control text-black" name="netHiddenSalary" id="netHiddenSalary"
																style="display:none;width: 90%; margin-left: 9px; margin-right: 10px;" />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col-md-2 col-md-offset-5">
                          <button type="submit" class="btn button"
											id="edetailsGen" name="edetailsGen" value="ePayGen"
											>Generate
                          </button>
                        </div>
                        <div class="col-md-2">
                          <button type="print" class="btn btn-primary"
											style="padding-left: 30px; padding-right: 30px; display: none">Print
                          </button>
                        </div>
                      </div>
                    </form>

                  </div>

                </div>
              </div>
            </div>
          </div>
          </div>
        <!-- mydirectory -->
            <div class="direc_con" id="directory" style="display: none;border:1px solid #000">
                        <i class="fa fa-caret-up" aria-hidden="true" style="position: relative;left: 748px;font-size: 25px;"></i>
                        <div class="block block-transparent block-drop-shadow  hovershow" style="border-radius: 8px;">

                            <div class="container">
                                <div class="col-md-12  mydir">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <a href="#"> <img src="assets\img\turning_image\TeamC1.png" width="70px" height="70px" />
                                                <p>Team Collaboration </p>
                                        </div>
                                        </a>
                                        <a href="ijp.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\ijp1.png" width="70px" height="70px" />
                                                <p>IJP(Internal Job Posting) </p>
                                            </div>
                                        </a>
                                        <a href="Ticketing.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\helpDesk1.png" width="70px" height="70px" />
                                                <p>Help Desk Ticketing </p>
                                            </div>
                                        </a>
                                        <a href="Recognition.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\EmpRecognition1.png" width="70px" height="70px" />
                                                <p>Employee Recognition </p>
                                            </div>
                                        </a>
                                        <a href="Download_payslip.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\downloads.png" width="70px" height="70px" />
                                                <p>Download PaySlip</p>
                                            </div>
                                        </a>
                                        <a href="Payslip.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\payslip1.png" width="70px" height="70px" />
                                                <p>Generate Payslips </p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="row">

                                        <a href="Reimbursement.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\Reimbursement1.png" width="70px" height="70px" />
                                                <p>Reimbursement </p>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\teamM1.png" width="70px" height="70px" />
                                                <p>Team Management </p>
                                            </div>
                                        </a>
                                        <a href="hr.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\hr1.png" width="70px" height="70px" />
                                                <p>HR Activities </p>
                                            </div>
                                        </a>
                                        <a href="Event_Site.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\EventSite1.png" width="70px" height="70px" />
                                                <p>Event Site </p>
                                            </div>
                                        </a>
                                        <a href="Meeting.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\Meeting1.png" width="70px" height="70px" />
                                                <p>Meeting / Briefings </p>
                                            </div>
                                        </a>
                                        <a href="Ass_tracking.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\Tracking1.png" width="70px" height="70px" />
                                                <p>Assets Tracking System</p>
                                            </div>
                                        </a>

                                    </div>
                                    <div class="row">

                                        <a href="kpis.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\kpis.png" width="70px" height="70px" />
                                                <p>KPI(For Manager/ Employee)</p>
                                            </div>
                                        </a>
                                               <a href="kpi.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\kpi1.png" width="70px" height="70px" />
                                                <p>KPI(Key Performance Indicator) for HR</p>
                                            </div>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

        </jsp:body>
</template:form>