<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="template" tagdir="/WEB-INF/tags" %>



<template:login>
	<jsp:attribute name="headContent">

    </jsp:attribute>
	<jsp:body > 
	<div style="width: 100%; height: 100vh; background: #283e4a; margin: 0px; padding: 0px; margin-top: -102px;">
<div class="login-block">
            <div class="block block-transparent"
					style="background-color: #fff; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; border-top-left-radius: 10px; border-top-right-radius: 10px;margin-top:100px">
                <div class="head">
                    <div class="user">
                        <div class="info user-change">                                                                                 
                            <img
									src="assets/img/example/user/dmitry_b.jpg"
									class="img-circle img-thumbnail" />
                            <div class="user-change-button">
                                <span class="icon-off"></span>
                            </div>
                        </div>                            
                    </div>
                </div>
                <div class="content controls npt">
                    <div class="form-row user-change-row">
                        <div class="col-md-12">
                            <div class="input-group form-back">
                                <div class="input-group-addon">
                                    <span class="icon-user" style="color:white"></span>
                                </div>
                                <input type="text" name="username"
										class="form-control login-input" placeholder="Login" />
                            </div>
                        </div>
                    </div>  
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="input-group form-back">
                                <div class="input-group-addon">
                                    <span class="icon-key" style="color:white"></span>
                                </div>
                                <input type="password"
										class="form-control login-input" placeholder="Password" />
                            </div>
                        </div>
                    </div>                        
                    <div class="form-row">
                        <div class="col-md-6">
                            <a href="#" class="btn  btn-block btn-info">Register</a>                                
                        </div>
                        <div class="col-md-6">
                            <button type="submit" name="action"
									value="signIn" class="submit btn btn-success btn-block ">Log In <i
										class="icon-angle-right"></i>
                            </button>
                        </div>
                    </div>
                    <div class="form-row">                            
                        <div class="col-md-12">
                            <a href="#" class="btn btn-link btn-block">Forgot your password?</a>
                        </div>
                    </div>                         
                    <div class="form-row">
                        <div class="col-md-12 tac">
							<strong>OR USE</strong>
						</div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12">
                            <a href="${login.googleAuth}"
									class="btn btn-info btn-block"><span
									class="icon-google-plus"></span> &nbsp; Google</a>
                        </div>
                    </div>
                    <div class="form-row" style="display: none">
                        <div class="col-md-12">
                            <a href="#"
									class="btn btn-primary btn-block"><span
									class="icon-twitter"></span> &nbsp; Tweeter</a>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
        </div>
 </jsp:body>
</template:login>