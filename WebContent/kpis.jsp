<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@page contentType="text/html" pageEncoding="UTF-8"%>
        <%@taglib prefix="template" tagdir="/WEB-INF/tags"%>
            <template:form>
                <jsp:attribute name="headContent">



                    <script src='assets/js/plugins/jquery/jquery.min.js'></script>
                    <script type='text/javascript' src='assets/js/plugins/jquery/jquery.validate.js'></script>
                    <script type='text/javascript' src='assets/js/plugins/bootstrap/bootstrap.min.js'></script>

                    <script type='text/javascript' src='assets/js/custom/kpis.js'></script>
                    <script type='text/javascript' src='assets/js/plugins/jquery/jquery-ui.min.js'></script>
                    <style>
                         label.error {
    color: rgb(244,67,54);
}
                        
                        input.error {
                            border: 1px solid  rgb(244,67,54);
                        }
                        
                        textarea.error {
                            border: 1px solid  rgb(244,67,54);
                        }
                            #directory::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	border-radius: 10px;
	background-color: #F5F5F5;
}

#directory::-webkit-scrollbar
{
	width: 12px;
	background-color: #F5F5F5;
}

#directory::-webkit-scrollbar-thumb
{
	border-radius: 10px;
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
	background-color: #555;
}
    .direc_con
{
	
	
	overflow-y: scroll;
	
}
        .block .content{background-color: #fff !important;}                
                    </style>
                </jsp:attribute>
                <jsp:body>
                    <div class="col-md-10 layout">
                        <div class="row">
                            <div class="block block-drop-shadow">

                                <div class="header form-header header-border">
                                    <h2 class="text-color">Key Performance Indicator</h2>
                                </div>
                                <ul class="nav nav-tabs nav-justified">
                                    <li class="active "><a href="#foremployee" data-toggle="tab">For Employee </a></li>
                                    <li><a href="#reportingM" data-toggle="tab">For Reporting Manager</a></li>

                                </ul>
                                <div class="content tab-content ">

                                    <div class="tab-pane active " id="foremployee">
                                        <form role="form" id="kpiforEmp">
                                            <div class="content controls bg-form content-full-border" style="margin-top:30px;">
                                                <div class="form-row">
                                                    <div class="col-md-3 text-black">Key Performance(Indicators)     <span style="color: red">*</span></div>
                                                    <div class="col-md-9">
                                                        <select class="form-control text-black" name="kpi" id="kpi" required> 
                                        <option value="">Select</option>
                                
                                                            
                                </select>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-3 text-black">Min      <span style="color: red">*</span></div>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control text-black" name="min" id="min" required/>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-3 text-black">Max     <span style="color: red">*</span></div>
                                                    <div class="col-md-9">
                                                        <input type="number" class="form-control text-black" name="max" id="max" required/>
                                                    </div>
                                                </div>


                                                <div class="form-row  m-top-20">

                                                    <div class="col-md-2 mar-top-12">
                                                        <button type="submit" class="btn btn-block btn-success">Submit</button>
                                                    </div>
                                                    <div class="col-md-2 mar-top-12">
                                                        <button type="reset" class="btn btn-block btn-danger">Cancel</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </form>
                                    </div>

                                    <div class="tab-pane " id="reportingM">
                                        <form role="role" id="forManager">
                                            <div class="content controls bg-form content-full-border" style="margin-top:30px;">
                                                <div class="form-row">
                                                    <div class="col-md-3 text-black">Employee Id     <span style="color: red">*</span></div>
                                                    <div class="col-md-9">
                                                        <select class="form-control text-black" id="Emp_id" name="Emp_id" required>
                                        <option value="">Select</option>
                                
                                                            
                                </select>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-3" style="color:darkcyan">Performance Rating (Project Wise) </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-3 text-black">Project      <span style="color: red">*</span></div>
                                                    <div class="col-md-9">
                                                        <select class="form-control text-black" id="project" name="project" required>
                                        <option value="">Select</option>
                                        <option value="EaseMyGst">EaseMyGst</option>
                                        <option value="other">other</option>
                                
                                                            
                                </select>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-3 text-black">Rate     <span style="color: red">*</span></div>
                                                    <div class="col-md-9">
                                                        <select class="form-control text-black" id="rate" name="rate" required> 
                                        <option value="">Select</option>
                                        <option value="5">5</option>
                                        <option value="4">4</option>
                                <option value="3">3</option>
                                 <option value="2">2</option>
              <option value="1">1</option>
                                                            
                                </select>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-3 text-black">Comment/Remarks     <span style="color: red">*</span></div>
                                                    <div class="col-md-9">
                                                        <textarea rows="5" cols="50" class="form-control text-black" style="margin-top:0px;" id="comment" name="comment" required></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-row m-top-20">

                                                    <div class="col-md-2 mar-top-12">
                                                        <button type="submit" class="btn btn-block btn-success">Submit</button>
                                                    </div>
                                                    <div class="col-md-2 mar-top-12">
                                                        <button type="reset" class="btn btn-block btn-danger">Cancel</button>
                                                    </div>
                                                </div>

                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                 
                    </div>
               
                    <!-- mydirectory -->
                        <div class="direc_con" id="directory" style="display: none;border:1px solid #000">
                        <i class="fa fa-caret-up" aria-hidden="true" style="position: relative;left: 748px;font-size: 25px;"></i>
                        <div class="block block-transparent block-drop-shadow  hovershow" style="border-radius: 8px;">

                            <div class="container">
                                <div class="col-md-12  mydir">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <a href="#"> <img src="assets\img\turning_image\TeamC1.png" width="70px" height="70px" />
                                                <p>Team Collaboration </p>
                                        </div>
                                        </a>
                                        <a href="ijp.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\ijp1.png" width="70px" height="70px" />
                                                <p>IJP(Internal Job Posting) </p>
                                            </div>
                                        </a>
                                        <a href="Ticketing.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\helpDesk1.png" width="70px" height="70px" />
                                                <p>Help Desk Ticketing </p>
                                            </div>
                                        </a>
                                        <a href="Recognition.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\EmpRecognition1.png" width="70px" height="70px" />
                                                <p>Employee Recognition </p>
                                            </div>
                                        </a>
                                        <a href="Download_payslip.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\downloads.png" width="70px" height="70px" />
                                                <p>Download PaySlip</p>
                                            </div>
                                        </a>
                                        <a href="Payslip.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\payslip1.png" width="70px" height="70px" />
                                                <p>Generate Payslips </p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="row">

                                        <a href="Reimbursement.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\Reimbursement1.png" width="70px" height="70px" />
                                                <p>Reimbursement </p>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\teamM1.png" width="70px" height="70px" />
                                                <p>Team Management </p>
                                            </div>
                                        </a>
                                        <a href="hr.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\hr1.png" width="70px" height="70px" />
                                                <p>HR Activities </p>
                                            </div>
                                        </a>
                                        <a href="Event_Site.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\EventSite1.png" width="70px" height="70px" />
                                                <p>Event Site </p>
                                            </div>
                                        </a>
                                        <a href="Meeting.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\Meeting1.png" width="70px" height="70px" />
                                                <p>Meeting / Briefings </p>
                                            </div>
                                        </a>
                                        <a href="Ass_tracking.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\Tracking1.png" width="70px" height="70px" />
                                                <p>Assets Tracking System</p>
                                            </div>
                                        </a>

                                    </div>
                                    <div class="row">

                                        <a href="kpis.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\kpis.png" width="70px" height="70px" />
                                                <p>KPI(For Manager/ Employee)</p>
                                            </div>
                                        </a>
                                               <a href="kpi.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\kpi1.png" width="70px" height="70px" />
                                                <p>KPI(Key Performance Indicator) for HR</p>
                                            </div>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


                </jsp:body>

            </template:form>