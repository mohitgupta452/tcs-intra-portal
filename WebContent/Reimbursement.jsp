<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@page contentType="text/html" pageEncoding="UTF-8"%>
        <%@taglib prefix="template" tagdir="/WEB-INF/tags"%>
            <template:form>
                <jsp:attribute name="headContent">

                    <link href="assets/css/custom/Reimbursement.css" rel="stylesheet" type="text/css" />

                    <script src='assets/js/plugins/jquery/jquery.min.js'></script>
                    <script type='text/javascript' src='assets/js/plugins/jquery/jquery.validate.js'></script>
                    <script type='text/javascript' src='assets/js/plugins/bootstrap/bootstrap.min.js'></script>

                    <script type='text/javascript' src='assets/js/custom/Reimbursement.js'></script>
                    <script type='text/javascript' src='assets/js/plugins/jquery/jquery-ui.min.js'></script>
                    <style>
                       label.error {
    color: rgb(244,67,54);
}
                        
                        input.error {
                            border: 1px solid  rgb(244,67,54);
                        }
                        
                        textarea.error {
                            border: 1px solid  rgb(244,67,54);
                        }
                            #directory::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	border-radius: 10px;
	background-color: #F5F5F5;
}

#directory::-webkit-scrollbar
{
	width: 12px;
	background-color: #F5F5F5;
}

#directory::-webkit-scrollbar-thumb
{
	border-radius: 10px;
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
	background-color: #555;
}
    .direc_con
{
	
	
	overflow-y: scroll;
	
}
        textarea.form-control {
    height: 50px !important;
}                  
                    </style>
                </jsp:attribute>
                <jsp:body>
                    <div class="col-md-10 layout">
                        

                        <div class="row">
                            <div class="block block-drop-shadow">

                                <div class="header form-header header-border">
                                    <h2 class="text-color">Reimbursement Form</h2>
                                </div>
                                <form role="form" id="Reimbursement">
                                    <div class="content controls content-border bg-form">

                                        <div class="form-row">
                                            <div class="col-md-3 text-black">Raised By<span style="color: red">*</span></div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control text-black" name="raisedBy" id="raisedBy" required/>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-3 text-black">Category<span style="color: red">*</span></div>
                                            <div class="col-md-9">
                                                <select class="form-control text-black" name="category" id="category" required>
                                        <option value="">Select</option>
                                    <option value="Travel Expense">Travel Expense</option>
                                    <option value="Employee FNF">Employee FNF</option>
                                    <option value="Foods">Foods</option>
                                    <option value="Medical Expense">Medical Expense</option>
                                                            
                                </select>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-3 text-black">Supporting Document<span style="color: red">*</span></div>
                                            <div class="col-md-9">
                                                <input type="file" class="form-control text-black" name="supDoc" id="supDoc" required/>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-3 text-black">Remarks<span style="color: red">*</span></div>
                                            <div class="col-md-9">
                                                <textarea rows="5" cols="50" class="form-control text-black" style="margin-top:0px;" name="remarks" id="remarks" required></textarea>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-3 text-black">Date Of Request Raised<span style="color: red">*</span></div>
                                            <div class="col-md-9">
                                                <input type="date" class="form-control text-black" name="dateofRaised" id="dateofRaised" required />
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-3 text-black">Total Amount<span style="color: red">*</span></div>
                                            <div class="col-md-9">
                                                <input type="number" class="form-control text-black" name="totalAmount" id="totalAmount" required />
                                            </div>
                                        </div>


                                    </div>
                                    <div class="footer footer-border bg-form">
                                        <div class="form-row">

                                            <div class="col-md-2 mar-top-12">
                                                <button type="submit" class="btn btn-block btn-success">Submit</button>
                                            </div>
                                            <div class="col-md-2 mar-top-12">
                                                <button type="reset" class="btn btn-block btn-danger">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>



                 
                    </div>
                   
                    <!-- mydirectory -->
      <div class="direc_con" id="directory" style="display: none;border:1px solid #000">
                        <i class="fa fa-caret-up" aria-hidden="true" style="position: relative;left: 748px;font-size: 25px;"></i>
                        <div class="block block-transparent block-drop-shadow  hovershow" style="border-radius: 8px;">

                            <div class="container">
                                <div class="col-md-12  mydir">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <a href="#"> <img src="assets\img\turning_image\TeamC1.png" width="70px" height="70px" />
                                                <p>Team Collaboration </p>
                                        </div>
                                        </a>
                                        <a href="ijp.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\ijp1.png" width="70px" height="70px" />
                                                <p>IJP(Internal Job Posting) </p>
                                            </div>
                                        </a>
                                        <a href="Ticketing.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\helpDesk1.png" width="70px" height="70px" />
                                                <p>Help Desk Ticketing </p>
                                            </div>
                                        </a>
                                        <a href="Recognition.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\EmpRecognition1.png" width="70px" height="70px" />
                                                <p>Employee Recognition </p>
                                            </div>
                                        </a>
                                        <a href="Download_payslip.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\downloads.png" width="70px" height="70px" />
                                                <p>Download PaySlip</p>
                                            </div>
                                        </a>
                                        <a href="Payslip.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\payslip1.png" width="70px" height="70px" />
                                                <p>Generate Payslips </p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="row">

                                        <a href="Reimbursement.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\Reimbursement1.png" width="70px" height="70px" />
                                                <p>Reimbursement </p>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\teamM1.png" width="70px" height="70px" />
                                                <p>Team Management </p>
                                            </div>
                                        </a>
                                        <a href="hr.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\hr1.png" width="70px" height="70px" />
                                                <p>HR Activities </p>
                                            </div>
                                        </a>
                                        <a href="Event_Site.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\EventSite1.png" width="70px" height="70px" />
                                                <p>Event Site </p>
                                            </div>
                                        </a>
                                        <a href="Meeting.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\Meeting1.png" width="70px" height="70px" />
                                                <p>Meeting / Briefings </p>
                                            </div>
                                        </a>
                                        <a href="Ass_tracking.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\Tracking1.png" width="70px" height="70px" />
                                                <p>Assets Tracking System</p>
                                            </div>
                                        </a>

                                    </div>
                                    <div class="row">

                                        <a href="kpis.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\kpis.png" width="70px" height="70px" />
                                                <p>KPI(For Manager/ Employee)</p>
                                            </div>
                                        </a>
                                               <a href="kpi.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\kpi1.png" width="70px" height="70px" />
                                                <p>KPI(Key Performance Indicator) for HR</p>
                                            </div>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


                </jsp:body>

            </template:form>