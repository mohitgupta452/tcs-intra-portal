<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="template" tagdir="/WEB-INF/tags"%>
<template:form>
	<jsp:attribute name="headContent">
          <link href="assets/css/custom/Payslip.css" rel="stylesheet">
           <link href="https://cdn.datatables.net/1.10.14/css/jquery.dataTables.min.css" rel="stylesheet " type="text/css" />
          <script type='text/javascript'
			src='assets/js/plugins/jquery/jquery.min.js'>
											
										</script>
          <script type='text/javascript'
			src='assets/js/plugins/jquery/jquery.validate.js'>
											
										</script>
          <script type='text/javascript'
			src='assets/js/plugins/bootstrap/bootstrap.min.js'>
											
										</script>
          <script type='text/javascript'
			src='assets/js/custom/Download_slip.js'>
											
										</script>
          <script type='text/javascript'
			src='assets/js/plugins/jquery/jquery-ui.min.js'>
											
										</script>
										 <script type="text/javascript" src="https://cdn.datatables.net/1.10.14/js/jquery.dataTables.min.js"></script>
          <script type='text/javascript'>
											$(function() {
												$('#online')
														.click(
																function() {
																	if ($(
																			'#chat')
																			.css(
																					'display') == "block") {
																		$(
																				'#chat')
																				.css(
																						'display',
																						'none');
																	} else {
																		$(
																				'#chat')
																				.css(
																						'display',
																						'block');
																	}
																});
											});
										</script>
          <style>
label.error {
	color: #FF5722;
}

input.error {
	border: 1px solid #FF5722;
}

textarea.error {
	border: 1px solid #FF5722;
}

.legend_border {
	border: 1px solid #999;
}

.form-row:first-child {
	margin-top: 0px !important;
}
.dataTables_wrapper .dataTables_paginate .paginate_button {
  color: #fff !important
}

.dataTables_wrapper .dataTables_paginate .paginate_button .current {
  color: #fff !important
}
.dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
  
    color: #fff !important;
    
}
.dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
    color: #fff !important;
    }
    #directory::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	border-radius: 10px;
	background-color: #F5F5F5;
}

#directory::-webkit-scrollbar
{
	width: 12px;
	background-color: #F5F5F5;
}

#directory::-webkit-scrollbar-thumb
{
	border-radius: 10px;
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
	background-color: #555;
}
    .direc_con
{
	
	
	overflow-y: scroll;
	
}

.force-overflow
{
	min-height: 450px;
}
   .data-table-header{
background-color: whitesmoke !important;

opacity: .5;
}
.data-table-body{
    background-color: whitesmoke !important;
}  .dataTables_wrapper .dataTables_paginate .paginate_button.current,
   .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
       width: 20px;
       height: 20px;
       padding-top: 2px;
       padding-left: 5px;
   background: #ccc !important;
       color: #000 !important;
       border: 0px;
               font-size: 11px !important;
    font-family: tahoma !important;
   }
   
   .dataTables_wrapper .dataTables_length,
   .dataTables_wrapper .dataTables_filter,
   .dataTables_wrapper .dataTables_info,
   .dataTables_wrapper .dataTables_processing,
   .dataTables_wrapper .dataTables_paginate {
       margin-right: 20px ;
       color: #fff ;
       margin-top: 20px ;
   }
   
   .dataTables_wrapper .dataTables_paginate .paginate_button.disabled,
   .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover,
   .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
       cursor: default;
       color: #000 !important;
       border: 1px solid transparent;
       background: #ccc !important;
       box-shadow: none;
       padding-left: 2px;
       padding-right: 2px;
       padding-bottom: 2px;
       padding-top: 2px;
           font-size: 11px !important;
    font-family: tahoma !important;
   } 
   
   table.dataTable.no-footer{border-bottom:none;}
   input[type="search"]
{background: #E0E0E0 !important;}
select{background: #E0E0E0 !important;}
h2 {
    font-size: 16px;
    font-style: normal;
    font-family: roboto;
    color: #000 !important;
}
th{font-weight:500 !important}

</style>
        </jsp:attribute>
	<jsp:body>
   <div class="col-md-10 layout">
                        <div class="row">
                            <div class="block bg-datatable box" >
                             
                                <div class="content data-table-body">
                                 <h2 >Sortable table</h2>
                                    <table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped sortable dataTable"
									id="download">
                                        <thead>
                                            <tr>
                                                <th class="text-black">Employee Code</th>
                                                <th class="text-black">Emp. File no. </th>
                                                <th class="text-black">FullName</th>
                                                <th class="text-black">Emp. Status </th>
                                                <th class="text-black">Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr>
                                                <td>TCEMP1054</td>
                                                <td>1054</td>
                                                <td>Kavita Singh</td>
                                                <td>nvcxxhcj</td>
                                                <td><button type="button" class="btn edit">EDIT</button></td>
                                            </tr>
                                            <tr>
                                                <td>TCEMP1054</td>
                                                <td>1006</td>
                                                <td>Manoj Singh</td>
                                                <td>nvcxxhcj</td>
                                                <td><button type="button" class="btn edit" >EDIT</button></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                        </div>
    <!-- mydirectory -->
               <div class="direc_con" id="directory" style="display: none;border:1px solid #000">
                        <i class="fa fa-caret-up" aria-hidden="true" style="position: relative;left: 748px;font-size: 25px;"></i>
                        <div class="block block-transparent block-drop-shadow  hovershow" style="border-radius: 8px;">

                            <div class="container">
                                <div class="col-md-12  mydir">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <a href="#"> <img src="assets\img\turning_image\TeamC1.png" width="70px" height="70px" />
                                                <p>Team Collaboration </p>
                                        </div>
                                        </a>
                                        <a href="ijp.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\ijp1.png" width="70px" height="70px" />
                                                <p>IJP(Internal Job Posting) </p>
                                            </div>
                                        </a>
                                        <a href="Ticketing.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\helpDesk1.png" width="70px" height="70px" />
                                                <p>Help Desk Ticketing </p>
                                            </div>
                                        </a>
                                        <a href="Recognition.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\EmpRecognition1.png" width="70px" height="70px" />
                                                <p>Employee Recognition </p>
                                            </div>
                                        </a>
                                        <a href="Download_payslip.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\downloads.png" width="70px" height="70px" />
                                                <p>Download PaySlip</p>
                                            </div>
                                        </a>
                                        <a href="Payslip.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\payslip1.png" width="70px" height="70px" />
                                                <p>Generate Payslips </p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="row">

                                        <a href="Reimbursement.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\Reimbursement1.png" width="70px" height="70px" />
                                                <p>Reimbursement </p>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\teamM1.png" width="70px" height="70px" />
                                                <p>Team Management </p>
                                            </div>
                                        </a>
                                        <a href="hr.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\hr1.png" width="70px" height="70px" />
                                                <p>HR Activities </p>
                                            </div>
                                        </a>
                                        <a href="Event_Site.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\EventSite1.png" width="70px" height="70px" />
                                                <p>Event Site </p>
                                            </div>
                                        </a>
                                        <a href="Meeting.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\Meeting1.png" width="70px" height="70px" />
                                                <p>Meeting / Briefings </p>
                                            </div>
                                        </a>
                                        <a href="Ass_tracking.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\Tracking1.png" width="70px" height="70px" />
                                                <p>Assets Tracking System</p>
                                            </div>
                                        </a>

                                    </div>
                                    <div class="row">

                                        <a href="kpis.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\kpis.png" width="70px" height="70px" />
                                                <p>KPI(For Manager/ Employee)</p>
                                            </div>
                                        </a>
                                               <a href="kpi.jsp">
                                            <div class="col-md-2">
                                                <img src="assets\img\turning_image\kpi1.png" width="70px" height="70px" />
                                                <p>KPI(Key Performance Indicator) for HR</p>
                                            </div>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


	</jsp:body>
	</template:form>