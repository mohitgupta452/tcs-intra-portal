$(function() {
    $('#online').click(function() {
        if ($('#chat').css('display') == "block") {

            $('#chat').css('display', 'none');
        } else {
            $('#chat').css('display', 'block');
        }
    });

});
$(document).ready(function() {
        //$('#directory').hide();

        $('.MyDirectoryOpen').on('mouseover', function() { 
            $('#directory').css('display', 'block');
        });
        $('#directory').on('mouseleave', function() { 
            $('#directory').css('display', 'none');
        });
    });

$(document).ready(function(){
$('input[type="text"],input[type="email"],input[type="number"],input[type="password"],input[type="time"],textarea').on('keydown', function() {
    $(this).removeClass('error').addClass('valid');
    var name = $(this).attr('id');
    $('[for="' + name + '"]').hide();
});
    $('select,input[type="file"],input[type="date"],input[type="checkbox"],input[type="radio"],select').on('change', function() {
        $(this).removeClass('error').addClass('valid');
        var name = $(this).attr('id');
        $('[for="' + name + '"]').hide();
    });
});
$(document).ready(function() {
    $('#example').DataTable({
        "pagingType": "full_numbers"
    });
    $('#authority').DataTable({
        "pagingType": "full_numbers"
    });
    $('#project_manager').DataTable({
        "pagingType": "full_numbers"
    });
    $('[data-toggle="tooltip"]').tooltip();
});


function onlyAlphabets(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        } else if (e) {
            var charCode = e.which;
        } else { return true; }
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode==32)
            return true;
        else
            return false;
    } catch (err) {
        alert(err.Description);
    }
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }

    return true;
}



$(document).ready(function() {

    $('#details').validate({
        rules: {
            "Emp_code": {
                required: true,

                maxlength: 20
            },

            "File_num": {
                required: true,
                maxlength: 20
            },
            "Emp_name": {
                required: true,

                maxlength: 50
            },

            "title": {
                required: true,
                maxlength: 50
            },
            "type": {
                required: true
            },

            "department": {
                required: true
            },
            "designation": {
                required: true
            },
            "hire_date": {
                required: true,
                maxlength: 50
            },
            "status": {
                required: true
            },
            "discontinued": {
                required: true,
                maxlength: 50
            },
          
            "passportNum": {
                required: true,
                maxlength: 30
            },
            "remarks": {
                required: true,
                maxlength: 100
            },
            "upload_image": {
                required: true
            },
            "fname": {
                required: true,
                maxlength: 30
            },
            "mname": {
                required: true,
                maxlength: 30
            },
            "sname": {
                required: true,
                maxlength: 30
            },
            "gender": {
                required: true
            },
            "birth_date": {
                required: true
            },
            "martial_status": {
                required: true
            },
            "religion": {
                required: true
            }
        },
        messages: {
            "Emp_code": {
                required: "Enter Employee code",
                phone: "Enter Employee code"
            },
            "File_num": {
                required: "Enter File Number",
                phone: "Enter File Number"

            },
            "Emp_name": {
                required: "Enter Employee Name",
                phone: "Enter Employee Name"
            },
            "title": {
                required: "Enter Title",
                phone: "Enter Title"
            },
            "type": {
                required: "Select type"
            },
            "department": {
                required: "Select Department"
            },
            "designation": {
                required: "Select Designation"
            },
            "hire_date": {
                required: "Enter Hire Date"
            },
            "status": {
                required: "Select Status"
            },
            "discontinued": {
                required: "Enter Date"
            },
            
            "passportNum": {
                required: "Enter Passport Number"
            },
            "remarks": {
                required: "Enter Remarks",
            
            },
            "upload_image": {
                required: "Upload a Image"
            },
            "fname": {
                required: "Enter Father Name"
            },
            "mname": {
                required: "Enter Mother Name"
            },
            "sname": {
                required: "Enter Spouse Name"
            },
            "gender": {
                required: "Select Gender"
            },
            "birth_date": {
                required: "Enter Birth Date"
            },
            "martial_status": {
                required: "Select Martial Status"
            },
            "religion": {
                required: "Select Religion"
            }
        },
        submitHandler: function(form) {
        
            form.submit();
        
        }


    });

    $('#personal').validate({
        rules: {
            "telephone": {
                required: true,
               
            },
            "mobile": {
                required: true,
              
            },
            "city": {
                required: true
            },
            "state": {
                required: true
            },
            "country": {
                required: true
            },
            "email": {
                required: true,
                maxlength: 100
            },
            "temp_address": {
                required: true,
                maxlength: 200

            },
            "per_address": {
                required: true,
                maxlength: 200
            }

        },
        messages: {
            "telephone": {
                required: "Enter Telephone Number.",
                
            },
            "mobile": {
                required: "Enter 10 Digit Mobile Number.",
               

            },
            "city": {
                required: "Enter a City"
            },
            "state": {
                required: "Select a State"
            },
            "country": {
                required: "Select a Country"
            },
            "email": {
                required: "Enter Your Email Address",
                email: "Enter Valid Email Address "
            },
            "temp_address": {
                required: "Enter Your Current Address"
            },
            "per_address": {
                required: "Enter your Permanent Address"
            }
        },
        submitHandler: function(form) {
            form.submit();
        }

    });
    $('#salary').validate({
        rules: {
            "basic_salary": {
                required: true
            },
            "gross_salary": {
                required: true
            },
            "pay_currency": {
                required: true
            },
            "emp_bank_acc": {
                required: true
            },
            "emp_account_num": {
                required: true
            }
        },
        messages: {
            "basic_salary": {
                required: "Enter Your Basic Salary"
            },
            "gross_salary": {
                required: "Enter Your Gross Salary"
            },
            "pay_currency": {
                required: "Select Pay Currency"
            },
            "emp_bank_acc": {
                required: "Select Bank Account"
            },
            "emp_account_num": {
                required: "Enter Account Number"
            }
        },
        submitHandler: function(form) {
            form.submit();
        }


    });
});

$(function () {
    $("#yes").click(function () {
        if ($(this).is(":checked")) {
            $("#passportNum").removeAttr("disabled");
            $("#passportNum").focus();
        } else {
            $("#passportNum").attr("disabled", "disabled");
        }
    });
});


$(document).ready(function(){
	$("input[type='file']").change(function(e) {
	
	    for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {

	        var file = e.originalEvent.srcElement.files[i];

	        var reader = new FileReader();
	        reader.onloadend = function() {
	             $('#blah').attr('src', reader.result);
	        }
	        reader.readAsDataURL(file);        }
	});
	$(window).load(function() {
		$(".loader").fadeOut("slow");
	})

	});




