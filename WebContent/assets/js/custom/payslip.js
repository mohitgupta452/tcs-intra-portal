$(function() {
	$('#online').click(function() {
		if ($('#chat').css('display') == "block") {

			$('#chat').css('display', 'none');
		} else {
			$('#chat').css('display', 'block');
		}
	});

});
$(document).ready(function() {
	$("#Emp_code").on('change', function() {
		var empCode = this.value;
		getEmpData(empCode);
	});
	$("#months").on('change', function() {
		$("#month").val(this.value);
	});
	$("#year").on('change', function() {
		$("#years").val(this.value);
	});
});

function getEmpData(empCode) {
	var url = "/tcs-portal/rest/data/showEmpdata/" + empCode;
	$.getJSON(url, function(result) {
		var fname = result['fname'].split(" ");
		$("#emp_Name").val(result['fname']);
		$("#edi").val(result['empDataId']);
		$("#first_Name").val(fname[0]);
		$("#last_Name").val(fname[1]);
		$("#mobile").val(result['mNumber']);
		$("#emailid").val(result['emailId']);
		$("#current_Address").val(result['curAdd']);
		$("#permanent_Address").val(result['perAdd']);
		$("#Postal_code").val(result['pincode']);
		$("#city").val(result['city']);
		$("#state").val(result['state']);
		$("#country").val(result['country']);
		$("#cl").val(result['cl']);
		$("#sl").val(result['sl']);
		$("#el").val(result['el']);
		$("#ph").val("0");
		$("#absent").val("0");
		$("#holidays").val("0");
		$("#monthDays").val("0");
		$("#daily").val(result['daySalary']);
		$("#monthly").val(result['monthSalary']);
		$("#pfd").val(result['pf']);
		$("#epfd").val(result['epf']);
		$("#epsd").val(result['eps']);
		$("#esid").val(result['esi']);
		$("#esicd").val(result['esic']);
		$("#glwf").val(result['glwf']);
		$("#sedi").val(result['empDataId']);
		$("#gMonthlySalary").val(result['gMonthlySalary']);
		$("#netSalary").val(result['netPay']);
		$("#netHiddenSalary").val(result['netPay']);
	});
}
$(document).ready(
		function() {
			$("#absent").keyup(
					function() {
						if(this.value!=null&& this.value!=""){
							var abscentDay = this.value;
							var absentAmt = parseInt($("#gMonthlySalary").val())
							* parseInt(abscentDay) * 0.03333;
					var netPay =parseInt($("#netSalary").val())-absentAmt;
					$("#netSalary").val(Math.round(netPay));
						}
						else if(this.value==0){
							$("#netSalary").val($("#netHiddenSalary").val());
						}
//						else{
//							$("#netSalary").val($("#netHiddenSalary").val());
//						}
					});
			
			$("#ph").keyup(function() {
				if(this.value!=null &&this.value!="" && this.value!=0){
					var ph = this.value;
					var paidhour = parseInt($("#gMonthlySalary").val()) * .0037037 *parseInt(ph);
					var netPay =parseInt($("#netSalary").val())+paidhour;
					$("#netSalary").val(Math.round(netPay));
				}
//				else{
//					$("#netSalary").val($("#netHiddenSalary").val());
//				}
			});
		});
$(document).ready(function() {
	// $('#directory').hide();

	$('.MyDirectoryOpen').on('mouseover', function() {
		$('#directory').css('display', 'block');
	});
	$('#directory').on('mouseleave', function() {
		$('#directory').css('display', 'none');
	});
});

function onlyAlphabets(e, t) {
	try {
		if (window.event) {
			var charCode = window.event.keyCode;
		} else if (e) {
			var charCode = e.which;
		} else {
			return true;
		}
		if ((charCode > 64 && charCode < 91)
				|| (charCode > 96 && charCode < 123))
			return true;
		else
			return false;
	} catch (err) {
		alert(err.Description);
	}
}

function isNumber(evt) {
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	}

	return true;
}

$(function() {

	$(
			'input[type="text"],input[type="email"],input[type="number"],input[type="password"],input[type="time"],textarea')
			.on('keydown', function() {
				$(this).removeClass('error').addClass('valid');
				var name = $(this).attr('id');
				$('[for="' + name + '"]').hide();
			});
	$(
			'select,input[type="file"],input[type="date"],input[type="checkbox"],input[type="radio"],select')
			.on('change', function() {
				$(this).removeClass('error').addClass('valid');
				var name = $(this).attr('id');
				$('[for="' + name + '"]').hide();
			});

});

$(function() {
	$('#Emp_info').validate({
		rules : {
			"months":{
				required:true
			},
			"year":{
				required:true
			},
			"first_Name" : {
				required : true
			},
			"last_Name" : {
				required : true
			},
			"mobile" : {
				required : true
			},
			"emailid" : {
				required : true
			},
			"current_Address" : {
				required : true
			},
			"permanent_Address" : {
				required : true
			},
			"Postal_code" : {
				required : true
			},
			"city" : {
				required : true
			},
			"state" : {
				required : true
			},
			"country" : {
				required : true
			}
		},
		messages : {
			"months":{
				required:"Select Month"
			},
			"year":{
				required:"Select Year"
			},
			"first_Name" : {
				required : "Enter your First Name"
			},
			"last_Name" : {
				required : "Enter your Second Name"
			},
			"mobile" : {
				required : "Enter your Mobile Number"
			},
			"emailid" : {
				required : "Enter your Email Id"
			},
			"current_Address" : {
				required : "Enter your Current Address"
			},
			"permanent_Address" : {
				required : "Enter your Permanent Address"
			},
			"Postal_code" : {
				required : "Enter your Postal Code"
			},
			"city" : {
				required : "Enter your City"
			},
			"state" : {
				required : "Enter your State"
			},
			"country" : {
				required : "Enter your Country"
			}

		},
		submitHandler : function(form) {
			document.querySelector('#edetailsGen').onclick = function () {
				swal( "Your request Has been successfully Submitted!", "success")
			}
			form.submit();
		}

	});

});

var count = 0;
function addFunction() {
	count++;
	addDiv(count);
}

var all = "";
function addDiv(count) {

	all += ('<div style="margin-left:20px;margin-right:20px" id="AddAllowance'
			+ count
			+ '">'
			+ '<div class="col-md-5">'
			+ '<select class="form-control modal_select m-pad-0" name="allowance'
			+ count + '" id="allowance' + count + '" required>'
			+ '<option value="">select</option>'
			+ '<option value="mReim">Medical Reimbursement</option>'
			+ '<option value="tReim">Telephone Reimbursement</option>'
			+ '<option value="nshiftAllow">Night Shift Allowance</option>'
			+ '<option value="nPerPayout">Notice Period Payout</option>'
			+ '<option value="bonus">Bonus</option>'
			+ '</select>'
			+ '</div>' + '<div class="col-md-5">'
			+ '<input type="text" id="allowanceAmt' + count
			+ '" class="form-control" />' + '</div>'
			+'<div class="col-md-2">'
			+'<span class="dustbin" id="Allowancedustbin'+count+'"><i class="fa fa-trash-o " aria-hidden="true" style="color: red;font-size: 22px">'
			+'</i></span>'
			+'</div>'
			+ '<div  style="height:50px"></div>' + '</div>');

	$('#ExtraDiv').html(all);

}

var cont = 0;
function addDecFunction() {
	
	addDivv(cont);
	cont++;
}

var deduc = "";
function addDivv(cont) {

	deduc += ('<div style="margin-left:20px;margin-right:20px" id="DedAllowance'
			+ cont
			+ '">'
			+ '<div class="col-md-5">'
			+ '<select class="form-control modal_select m-pad-0" name="Dallowance'
			+ cont
			+ '" id="Dallowance'
			+ cont
			+ '" required>'
			+ '<option value="">select</option>'
			+ '<option value="pfund">Provident Fund</option>'
			+ '<option value="pTax">Professional Tax</option>'
			+ '<option value="lRecovery">Lunch Recovery</option>'
			+ '<option value="tRecovery">Transport Recovery</option>'
			+ '<option value="iTax">Income Tax</option>'
			+ '</select>'
			+ '</div>'
			+ '<div class="col-md-5">'
			+ '<input type="text" id="DallowanceAmt'
			+ cont
			+ '" class="form-control" />'
			+ '</div>'
			+'<div class="col-md-2">'
			+'<span class="dust" id="DeducAllowancedustbin'+cont+'"><i class="fa fa-trash-o " aria-hidden="true" style="color: red;font-size: 22px">'
			+'</i></span>'
			+'</div>'
			+ '<div  style="height:50px"></div>' + '</div>');
	$('#ExtraDivv').html(deduc);
}

$(document).ready(function(){
$("body").on('click','.dustbin',function(){
var id = this.id;
var c = id.replace(/\D/g,'');
//alert(c);
$('#AddAllowance'+c).remove();
all ="";
count--;
for(var j=1 ; j<=count ; j++){
	addDiv(j)
}
//alert(count);
});



$("body").on('click','.dust',function(){
var idd = this.id;
var co = idd.replace(/\D/g,'');

$('#DedAllowance'+co).remove();
deduc="";
cont--;
for(var i=1 ; i<=cont ; i++){
	addDivv(i);
}
});
});


/*$(document).ready(function () {
	document.querySelector('#edetailsGen').onclick = function () {
		swal( "Your request Has been successfully Submitted!", "success")
	}
});*/
