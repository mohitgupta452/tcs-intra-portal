$(function() {
    $('#online').click(function() {
        if ($('#chat').css('display') == "block") {

            $('#chat').css('display', 'none');
        } else {
            $('#chat').css('display', 'block');
        }
    });

});
$(document).ready(function() {
        //$('#directory').hide();

        $('.MyDirectoryOpen').on('mouseover', function() { 
            $('#directory').css('display', 'block');
        });
        $('#directory').on('mouseleave', function() { 
            $('#directory').css('display', 'none');
        });
    });


$(function(){
    $('input[type="text"],input[type="email"],input[type="number"],input[type="password"],input[type="time"],textarea').on('keydown', function() {
        //alert();
        $(this).removeClass('error').addClass('valid');
        var name = $(this).attr('id');
        $('[for="' + name + '"]').hide();
    });
    $('select,input[type="file"],input[type="date"],input[type="checkbox"],input[type="radio"],select').on('change', function() {
        $(this).removeClass('error').addClass('valid');
        var name = $(this).attr('id');
        $('[for="' + name + '"]').hide();
    });
    
});

function onlyAlphabets(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        } else if (e) {
            var charCode = e.which;
        } else { return true; }
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
            return true;
        else
            return false;
    } catch (err) {
        alert(err.Description);
    }
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }

    return true;
}
$(document).ready(function() {
    $('#ijpForHr').validate({
        rules: {

            "job_description": {
                required: true
            },
            "experience": {
                required: true
            },
            "salary": {
                required: true
            },
            "signature": {
                required: true
            },
        },
        messages: {
            "job_description": {
                required: "Enter Job Description"
            },
            "experience": {
                required: "Enter Experience"
            },
            "salary": {
                required: "Enter Salary"
            },
            "signature": {
                required: "Enter Signature"
            }




        },
        submitHandler: function(form) {
            form.submit();
        }

    });

    $('#ijpForEmployee').validate({
        rules: {
            "apply[]": {
                required: true
            },
            "Current_Depart": {
                required: true
            },
            "salary": {
                required: true
            },
            "signature": {
                required: true
            }
        },
        messages: {
            "apply[]": {
                required: "Choose Apply For.."
            },
            "Current_Depart": {
                required: "Enter Current Department"
            },
            "salary": {
                required: "Enter Salary"
            },
            "signature": {
                required: "Enter Signature"
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });




});