$(document).ready(function() {
	$("#users").select2({placeholder: "Select Users",});
    
});
$(function() {
    $('#online').click(function() {
        if ($('#chat').css('display') == "block") {

            $('#chat').css('display', 'none');
        } else {
            $('#chat').css('display', 'block');
        }
    });

});


$(document).ready(function() {
	// $('#directory').hide();

	$('.MyDirectoryOpen').on('mouseover', function() {
		$('#directory').css('display', 'block');
	});
	$('#directory').on('mouseleave', function() {
		$('#directory').css('display', 'none');
	});
});

function onlyAlphabets(e, t) {
	try {
		if (window.event) {
			var charCode = window.event.keyCode;
		} else if (e) {
			var charCode = e.which;
		} else {
			return true;
		}
		if ((charCode > 64 && charCode < 91)
				|| (charCode > 96 && charCode < 123))
			return true;
		else
			return false;
	} catch (err) {
		alert(err.Description);
	}
}

function isNumber(evt) {
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	}

	return true;
}

$(function() {

	$('input[type="text"],input[type="email"],input[type="number"],input[type="password"],input[type="time"],textarea')
			.on('keydown', function() {
				$(this).removeClass('error').addClass('valid');
				var name = $(this).attr('id');
				$('[for="' + name + '"]').hide();
			});
	$('select,input[type="file"],input[type="date"],input[type="checkbox"],input[type="radio"],select')
			.on('change', function() {
				$(this).removeClass('error').addClass('valid');
				var name = $(this).attr('id');
				$('[for="' + name + '"]').hide();
			});

});

$(document).ready(function() {

	$("#meeting").validate({
		rules : {
			"date_of_meet" : {
				required : true

			},
			"from_date" : {
				required : true
			},
			"to_date" : {
				required : true
			},
			"users" : {
				required : true
			},
			"agenda" : {
				required : true,
				maxlength : 200
			}

		},
		messages : {
			"date_of_meet" : {
				required : "Enter Date of Meeting"

			},
			"from_date" : {
				required : "Enter Time"
			},
			"to_date" : {
				required : "Enter Time"
			},
			"users" : {
				required : "Select users"
			},
			"agenda" : {
				required : "Enter Agenda"
			}
		},
		submitHandler : function(form) {
			form.submit();
		}

	});

});