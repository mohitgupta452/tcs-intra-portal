$(function() {
    $('#online').click(function() {
        if ($('#chat').css('display') == "block") {

            $('#chat').css('display', 'none');
        } else {
            $('#chat').css('display', 'block');
        }
    });

});
$(document).ready(function() {
    $("#assets").select2({
        placeholder:"select Assets"

    });
});


$('#directory').hide();
$(document).ready(function() {

    $('.MyDirectoryOpen').mouseover(function() {
        $('#directory').show();
    });
    $('#directory').mouseleave(function() {
        $('#directory').hide();
    });

    
});
$(function(){
    $('input[type="text"],input[type="email"],input[type="number"],input[type="password"],input[type="time"],textarea').on('keydown', function() {
        //alert();
        $(this).removeClass('error').addClass('valid');
        var name = $(this).attr('id');
        $('[for="' + name + '"]').hide();
    });
    $('select,input[type="file"],input[type="date"],input[type="checkbox"],input[type="radio"],select').on('change', function() {
        $(this).removeClass('error').addClass('valid');
        var name = $(this).attr('id');
        $('[for="' + name + '"]').hide();
    });

});

function onlyAlphabets(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        } else if (e) {
            var charCode = e.which;
        } else { return true; }
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
            return true;
        else
            return false;
    } catch (err) {
        alert(err.Description);
    }
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }

    return true;
}



$(document).ready(function(){
$('#Assets').validate({
rules:
{
"Emp_name":
{
required:true,
maxlength:50
},
"Emp_id":
{
required:true
},
"Ass_id":
{
required:true
},
"Ass_type":
{
required:true
},
"Assigned":
{
required:true}
},
messages:
{
"Emp_name":
{
required:"Enter Employee Name"

},
"Emp_id":
{
required:"Enter Employee Id"
},
"Ass_id":
{
required:"Enter Asset Id"
},
"Ass_type":
{
required:"Enter Asset type"
},
"Assigned":
{
required:"Enter Name who Assigned"
}
},
 submitHandler: function(form) {
            form.submit();
        }



});

});