$(function() {
    $('#online').click(function() {
        if ($('#chat').css('display') == "block") {

            $('#chat').css('display', 'none');
        } else {
            $('#chat').css('display', 'block');
        }
    });

});
$(document).ready(function() {
	// $('#directory').hide();

	$('.MyDirectoryOpen').on('mouseover', function() {

		$('#directory').css('display', 'block');
	});
	$('#directory').on('mouseleave', function() {
		$('#directory').css('display', 'none');
	});
});
$(document).ready(function() {
	$('#myleave').DataTable({
		"pagingType" : "full_numbers"
	});

	$('#request_table').DataTable({
		"pagingType" : "full_numbers"
	});
	$('[data-toggle="tooltip"]').tooltip();
});

$(document).ready(function(){
	$('input[type="text"],input[type="email"],input[type="number"],input[type="password"],input[type="time"],textarea').on('keydown', function() {
	    
		$(this).removeClass('error').addClass('valid');
	    var name = $(this).attr('id');
	    $('[for="' + name + '"]').hide();
	});
	    $('select,input[type="text"],input[type="file"],input[type="date"],input[type="checkbox"],input[type="radio"],select').on('change', function() {
	        $(this).removeClass('error').addClass('valid');
	        var name = $(this).attr('id');
	        $('[for="' + name + '"]').hide();
	    });
	});

function onlyAlphabets(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        } else if (e) {
            var charCode = e.which;
        } else { return true; }
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
            return true;
        else
            return false;
    } catch (err) {
        alert(err.Description);
    }
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }

    return true;
}




// $(function () {
//     $("#fromdate").datepicker({
//         numberOfMonths: 2,
//          format: 'Y-m-d',
//         onSelect: function (selected) {
//             var dt = new Date(selected);
//             dt.setDate(dt.getDate() + 1);
//             $("#todate").datepicker("option", "minDate", dt);
//         }
//     });
//     $("#todate").datepicker({
//         numberOfMonths: 2,
//         onSelect: function (selected) {
//             var dt = new Date(selected);
//             dt.setDate(dt.getDate() - 1);
//             $("#fromdate").datepicker("option", "maxDate", dt);
//         }
//     });
// });
// $(function () {
//     $("#fromdatee").datepicker({
//         numberOfMonths: 2,

//         onSelect: function (selected) {
//             var dt = new Date(selected);
//             dt.setDate(dt.getDate() + 1);
//             $("#todatee").datepicker("option", "minDate", dt);
//         }
//     });
//     $("#todatee").datepicker({
//         numberOfMonths: 2,
//         onSelect: function (selected) {
//             var dt = new Date(selected);
//             dt.setDate(dt.getDate() - 1);
//             $("#fromdatee").datepicker("option", "maxDate", dt);
//         }
//     });
// });
$(document).ready(function(){

	var currentTime = new Date();
	if((currentTime.getMonth() + 1)>9){
		
		var month1 =currentTime.getMonth() + 1;
	}
	else{
    var month1 = ("0"+(currentTime.getMonth() + 1)).slice(-2);
	}
    var year1 = currentTime.getFullYear();
 if(currentTime.getDate()>9){
	  var day1 = currentTime.getDate();
 }else{
    var day1 = "0"+currentTime.getDate();
 }
   // if (month1<10) month1="0"+month1; 
   // if (day1<10) day1="0"+day1; 
   var curdate = year1 + "-" + month1 + "-" + day1;
  
     $('#fromdate').attr('min', curdate);
   
});


function enddate(fromdate){


var currentTime1 = new Date(fromdate);

    var month2 = ("0"+(currentTime1.getMonth() + 1)).slice(-2);

    var year2 = currentTime1.getFullYear();
   
    var day2 = ("0"+(currentTime1.getDate())).slice(-2);

    var curdate1 = year2 + "-" + month2 + "-" + day2;

     $('#todate').attr('min', curdate1);

}
$(document).ready(function(){
	var currentTime2 = new Date();
if((currentTime2.getMonth() + 1)>9){
	var month3 =currentTime2.getMonth() + 1;
}else{
    var month3 = ("0"+(currentTime2.getMonth() + 1)).slice(-2);
}
    var year3 = currentTime2.getFullYear();
    if(currentTime2.getDate()>9){
    	 var day3 =currentTime2.getDate();
    }
    else{
    var day3 = "0"+currentTime2.getDate();
    } 
   // if (month1<10) month1="0"+month1; 
   // if (day1<10) day1="0"+day1; 
   var curdate2 = year3 + "-" + month3 + "-" + day3;
   
     $('#fromdatee').attr('min', curdate2);
});


function enddatee(fromdatee){


var currentTime3 = new Date(fromdatee);

    var month4 = ("0"+(currentTime3.getMonth() + 1)).slice(-2);
  
    var year4 = currentTime3.getFullYear();
    
    var day4 = ("0"+(currentTime3.getDate())).slice(-2);

    var curdate3 = year4 + "-" + month4 + "-" + day4;

     $('#todatee').attr('min', curdate3);

}
$(document).ready(function(){
	$('#applyleave').validate({
		rules:{
			"leaveType":{
				required:true
				
			},
			"leaveBalance":{
				required:true
			},
			"fromdate":{
				required:true
			},
			"todate":{
				required:true
			},
			"leaveMessage":
				{
				required:true
				
				},
				"service":
					{
					required:true
					}
			
		},
		messages:
			{
			"leaveType":{
				required:"Select Leave type"
				
			},
			"leaveBalance":{
				required:"Enter Leave Balance"
			},
			"fromdate":{
				required:"Enter From Date"
			},
			"todate":{
				required:"Enter To Date",
					phone:"ToDate greater than Fromdate"
			},
			"leaveMessage":
				{
				required:"Enter Message"
				
				},
				"service":
					{
					required:"Enter Service"
					}
			
			},
			  submitHandler: function(form) {
		            form.submit();
		        }
	
		
		
	});
	
	
});
$(document).ready(function(){
	$('#leave').validate({
		rules:{
			"fromdatee":{
				required:true
				
			}	,
			"todatee":{
				required:true
			},
			"status[]":{
                 required:true				
				
				
			}
		},
		messages:{
			"fromdatee":{
				required:"Enter Form Date"
				
			}	,
			"todatee":{
				required:"Enter To Date"
			},
			"status[]":{
                 required:"Choose atleast one"			
				
				
			}
		},
		submitHandler: function(form) {
            form.submit();
        }

		
		
		
		
		
		
	});
	
	
	
});
$(document).ready(function(){
	$("#leaveType").on('change',function(){
		if(this.value=="casual"){
			$("#leaveBalance").val($("#casual").val());
		}else if (this.value=="paid") {
			$("#leaveBalance").val($("#paid").val());
		}else if (this.value=="sick") {
			$("#leaveBalance").val($("#sick").val());
		}
	});
});



















