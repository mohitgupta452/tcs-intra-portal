$(function() {
    $('#online').click(function() {
        if ($('#chat').css('display') == "block") {

            $('#chat').css('display', 'none');
        } else {
            $('#chat').css('display', 'block');
        }
    });

});

$(document).ready(function() {
        //$('#directory').hide();

        $('.MyDirectoryOpen').on('mouseover', function() { 
            $('#directory').css('display', 'block');
        });
        $('#directory').on('mouseleave', function() { 
            $('#directory').css('display', 'none');
        });
    });

$(function(){

  $('input[type="text"],input[type="email"],input[type="number"],input[type="password"],input[type="time"],textarea').on('keydown', function() {
        $(this).removeClass('error').addClass('valid');
        var name = $(this).attr('id');
        $('[for="' + name + '"]').hide();
    });
    $('select,input[type="file"],input[type="date"],input[type="checkbox"],input[type="radio"],select').on('change', function() {
        $(this).removeClass('error').addClass('valid');
        var name = $(this).attr('id');
        $('[for="' + name + '"]').hide();
    });


});


$(document).ready(function() {
    $("#eventSite").validate({
        rules: {
            "eventName": {
                required: true,
                maxlength: 30
            },
            "purpose": {
                required: true,
                maxlength: 100
            },
            "date": {
                required: true,

            },
            "time": {
                required: true
            }
        },
        messages: {
            "eventName": {
                required: "Enter Event Name"

            },
            "purpose": {
                required: "Enter Purpose"
            },
            "date": {
                required: "Enter Date"
            },
            "time": {
                required: "Enter time"
            }
        },
        submitHandler: function(form) {
            form.submit();
        }

    });
});