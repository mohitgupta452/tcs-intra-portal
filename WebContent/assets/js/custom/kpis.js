$(document).ready(function() {
        //$('#directory').hide();

        $('.MyDirectoryOpen').on('mouseover', function() { 
            $('#directory').css('display', 'block');
        });
        $('#directory').on('mouseleave', function() { 
            $('#directory').css('display', 'none');
        });
    });
$(function() {
    $('#online').click(function() {
        if ($('#chat').css('display') == "block") {

            $('#chat').css('display', 'none');
        } else {
            $('#chat').css('display', 'block');
        }
    });

});
$('input[type="text"],input[type="email"],input[type="number"],input[type="password"],input[type="file"],input[type="date"],input[type="time"],input[type="checkbox"],input[type="radio"],textarea,select').on('keydown', function() {
    $(this).removeClass('error').addClass('valid');
    var name = $(this).attr('id');
    $('[for="' + name + '"]').hide();
});

function onlyAlphabets(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        } else if (e) {
            var charCode = e.which;
        } else { return true; }
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
            return true;
        else
            return false;
    } catch (err) {
        alert(err.Description);
    }
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }

    return true;
}


$(document).ready(function() {
    $('#kpiforEmp').validate({
        rules: {
            "kpi": {
                required: true
            },
            "min": {
                required: true
            },
            "max": {
                required: true
            }
        },
        messages: {
            "kpi": {
                required: "Select kpi"
            },
            "min": {
                required: "Enter Min Value"
            },
            "max": {
                required: "Enter Max Value"
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
    $('#forManager').validate({
        rules: {
            "Emp_id": {
                required: true
            },
            "project": {
                required: true
            },
            "rate": {
                required: true
            },
            "comment": {
                required: true
            }
        },
        messages: {
            "Emp_id": {
                required: "Select Employee Id"
            },
            "project": {
                required: "Select Projects"
            },
            "rate": {
                required: "Select Rating"
            },
            "comment": {
                required: "Enter Comments"
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });


});